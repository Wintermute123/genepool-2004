<?session_start();
//session_register("build");
if (!isset($chr_id))
  $chr_id=17;
$page_title="GenePOOL Select Gene for build ";
$page_title.=$build;
$page_title.=" Contig ";
$page_title.=$seq_id;
require("nav_begin.php");

if (!isset($build))
  {
    echo 'Usage: gene.php?chr_id=1&build=testgenomix&seq_id=NT_004488.7';
	exit;
  }




mysql_connect($db_host,$db_username,$db_password);
mysql_select_db($build);
if ($search_type == "GXDB")
{ echo '<table width="100%" border="0"><TR><TH>Gene ID</TH><TH>Product</TH></TR>';
 echo '<tr><td colspan ="2"><img src = "../images/760_trans_spacer.gif"></td></tr>'; 
 for ($a=1; $a <= 25; $a++)
   {
     $chromo_id = $a;
     if ($a == 23)
       $chromo_id = "X";
     elseif ($a ==24)
       $chromo_id = "Y";
     elseif ($a == 25)
       $chromo_id = "Un";
     $search_string = trim($search_string);
     $search_string = strtoupper($search_string);
     $sql = "select gene_id, product  from chr_";
     $sql .= $chromo_id."_summary where gene_id like '%$search_string%'";
     //print $sql.'<BR>';
     $result = mysql_query($sql);
     
     for ($b=0; $row=mysql_fetch_row($result); $b++)
       {
	 $new_id = $row[2];
	 if ($b % 2 == 0)
	   {
	     echo '<tr bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFE4C4\'" align=\'center\' class =\'second\'><td width="25%">';
	     echo '<a href="gene.php?gene_id='.$row[0].'&build='.$build.'&chr_id='.$chromo_id.'">'.$row[0].'</A></td><td width="75%" align="left">'.$row[1].'</TD></TR>';
	   }
	 else
	   {
	     echo '<tr  bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFFFFF\'" align=\'center\'><td width="25%">';
	     echo '<a href="gene.php?gene_id='.$row[0].'&build='.$build.'&chr_id='.$a.'">'.$row[0].'</A></td><td width="75%" align="left">'.$row[1].'</TD></TR>';
	   }
	 
	 
       }
   } // End GXDB Search
 echo '</table>';
}
elseif ($search_type == "VERSION")
{
  echo '<PRE>';
  $uc_search_string = strtoupper($search_string);
  $syscmd = "/data/GenomixDB/src/gbget/gbget_version.pl ".$uc_search_string;
  system($syscmd);
  $syscmd = "/data/GenomixDB/src/gbget/refseq_get_version.pl ".$uc_search_string;
  system($syscmd);
  echo '</PRE>';
} //End Version Search

elseif ($search_type == "KEYWORD")
{
  $sql = "Select distinct (gene_id), product, chr_id from master_products where product like '%$search_string%'";
  $result = mysql_query($sql);
  echo '<table width="100%" border="0"><TR><TH>Gene ID</TH><TH>CHR_ID</TH><TH>Product</TH></TR>';
  echo '<tr><td colspan ="3"><img src = "../images/760_trans_spacer.gif"></td></tr>';
  for ($b=0; $row=mysql_fetch_row($result); $b++)
    {
      if ($b % 2 == 0)
	{
	  echo '<tr bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFE4C4\'" align=\'center\' class =\'second\'><td width="25%">';
	  echo '<a href="gene.php?gene_id='.$row[0].'&build='.$build.'&chr_id='.$row[2].'">'.$row[0].'</A></td><td>'.$row[2].'</td><td width="75%" align="left">'.$row[1].'</TD></TR>';
	}
      else
	{
	  echo '<tr  bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFFFFF\'" align=\'center\'><td width="25%">';
	  echo '<a href="gene.php?gene_id='.$row[0].'&build='.$build.'&chr_id='.$row[2].'">'.$row[0].'</A></td><td>'.$row[2].'</td><td width="75%" align="left">'.$row[1].'</TD></TR>';
	}
      
    }
  echo '</table>';
  
} // End Keyword Search
require("nav_end.php");