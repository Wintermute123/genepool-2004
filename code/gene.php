<?session_start();
//session_register("build");
if (!isset($chr_id))
  $chr_id=17;
$page_title="GenePOOL Select Gene for build ";
$page_title.=$build;
$page_title.=" Contig ";
$page_title.=$seq_id;
require("nav_begin.php");

if (!isset($build))
  {
    echo 'Usage: gene.php?chr_id=1&build=testgenomix&seq_id=NT_004488.7';
	exit;
  }
  

mysql_connect($db_host,$db_username,$db_password);
mysql_select_db($build);

$sql = "select gene_id,gene_number,variant_number,product,seq_id from chr_".$chr_id."_summary where gene_id = '$gene_id'";

$result=mysql_query($sql);
$row = mysql_fetch_row($result) or must_die(mysql_error());

echo '<font face="arial">';
echo '<font size="+1">Gene number '.$row[1].' variant '.$row[2].' - '.$row[0].'</font><BR><b>'.$row[3].'</b><BR>';
$variant=$row[2];
$prot_length=$row[4];
$gene_number=$row[1];
$sql = "select type, gene_begin, gene_end, strand, seq_id, protein_length,mrna_length from chr_".$chr_id."_summary where gene_id = '$gene_id' ";

//echo $sql;

$result=mysql_query($sql) or must_die(mysql_error());
$row=mysql_fetch_row($result);
?>
<table width="100%" cellspacing="0" cellpadding="0">
<TR><td width="45%" valign="top">
<TABLE cellspacing="0" cellpadding="0" width="90%">
<tr>
<TH colspan=2>Gene Information</TH>
</tr>
<tr>
<TH colspan=2><?echo $gene_id?></TH>
  </tr>

    <TD class="second">Location</TD>
    <TD class="second"><?echo $row[2]."..".$row[1]?></TD></TR>
  <TR>
    <TD>Strand</TD>
    <TD><?if ($row[3] == "r") echo "Reverse"; elseif ($row[3] == "f") echo "Forward"; else echo "Unknown"; ?></TD></TR>
  <TR>
    <TD class="second">Contig</TD>
    <?$seq_id = $row[4];?>
    <TD class="second">
    <? $seq_string = '<a href = "contig.php?build='.$build.'&chr_id='.$chr_id.'&seq_id='.$row[4].'">'.$row[4].'</A>';?>
<?echo $seq_string;?></TD></TR>
    <TR>
    <TD>Chromosome</TD>
    <TD>
    <?
        echo '<a href="select_chromosome.php?build='.$build.'&chr_id='.$chr_id.'">'.$chr_id.'</a>';
        ?>
    </TD>
    </TR>
  <TR>
    <TD class="second">Var</TD>
    <TD class="second"><?echo $variant?></TD></TR>
  <TR>
    <TD >Protein Length</TD>
    <TD ><?echo $row[5]?></TD></TR>
<tr>
   <td class="second"> mRNA Length</td>
    <td class="second"><?echo $row[6]?></td></tr>
  <TR>
    <TD>Blast</TD>
    <TD><?echo build_blast_link($build,$chr_id,$row[4],$gene_id,"View")?></TD></TR>
  <TR>
    <TD class="second" >Exp</TD>
    <TD  class="second"><?echo build_exp_link($build,$chr_id,$row[4],$gene_id); ?></TD></TR>
  </td>

  <TR>
    <TD>Protein Variants</TD>
    <TD><?echo show_prot_variants($build,$chr_id,$gene_id);?></td></tr>
    </TD>
    <TR><TD class="second">mRNA Variants</TD><td  class="second"><?echo show_mrna_variants($build,$chr_id,$gene_id);?></td>
    </TD></tr>
</td>
<tr>
    <td>Genomic Sequence</td><td >
    <?echo '<a href="javascript:openwin(\'compute_genomic_sequence.php?build='.$build.'&seq_id='.$seq_id.'&chr_id='.$chr_id.'&gene_id='.$gene_id.'\')">View</A>';

 ?></td>
</td></tr>

<tr>
<td  class="second">
USPTO Formatted Protein</TD>
<td  class="second"><?echo build_prot_pat_link($build,$chr_id,$seq_id,$gene_id,"");?></td></tr>
<TR>
<TD >USPTO Formatted mRNA</td>
<td ><?echo build_mrna_pat_link($build,$chr_id,$seq_id,$gene_id,"");?></td></tr>
<TR><TD  class="second">Isoform mRNA Alignments</td>
<td class="second"><?echo '<a href="javascript:openwin(\'show_mrna_isoform.php?build='.$build.'&chr_id='.$chr_id.'&seq_id='.$seq_id.'&gene_id='.$gene_id.'&gene_number='.$gene_number.'\')">View</A>';?></td></tr>
<td >Isoform Protein Alignments</td>
<td ><?echo '<a href="javascript:openwin(\'show_protein_isoform.php?build='.$build.'&chr_id='.$chr_id.'&gene_id='.$gene_id.'&seq_id='.$seq_id.'&gene_number='.$gene_number.'\')">View</A>';?></td>



</TABLE>
</Td><TD width = "1%">&nbsp;&nbsp;</td>
<td width="45%" align="center" valign="top">
<?

$seq_id = $row[4];

//now display the exon table for this gene only
echo '<table border="0" cellspacing="0" cellpadding="0" width ="100%" >';
echo '<tr><th colspan="4" align="center">Master Exon Table</th></tr>';
echo '<tr><th>Id</th><th>Size</th><Th>Active in Variant</th><th>Contig Location</th></tr>';
//Add here
$filename = "../gpdata/builds/".$build."/symlinks/".$chr_id."/".$gene_id.".exons";

if (!$fp = fopen($filename,"r"))
  {
    echo $filename." map file not found\n";
    exit;
  }
$count =0;
$gjmcount =0;
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
    $line = chomp($line);
    $tmparray = split(" ",$line);
    
    if ($line != "")
      {$gjmcount++;
	$count++;
	if (($count % 2) != 0)
	  {
	    echo '<tr><td align="right" class="second">'.$tmparray[0].'</td>';
	    //echo '<tr><td align="right" class="second">'.$gjmcount.'</td>';
	    if ($tmparray[1] <= $tmparray[2])
	      echo '<td align="right" class="second">'.($tmparray[2]-$tmparray[1]+1).'</td>';
	    else
	      echo '<td align="right" class="second">'.($tmparray[1]-$tmparray[2]+1).'</td>'; 
	    
	    echo '<td class="second" align="center">'.$tmparray[3].'</td>';
	    echo '<td align="center" class="second">'.$tmparray[1].'..'.$tmparray[2].'</td></tr>';
	  }
	else
	  {echo '<tr><td align="right">'.$tmparray[0].'</td>';
	    //echo '<tr><td align="right">'.$gjmcount.'</td>';
	    if ($tmparray[1] <= $tmparray[2])
	      echo '<td align="right">'.($tmparray[2]-$tmparray[1]+1).'</td>';
	    else
	      echo '<td align="right">'.($tmparray[1]-$tmparray[2]+1).'</td>'; 
	    
	    echo '<td align="center">'.$tmparray[3].'</td>';
	    echo '<td align="center">'.$tmparray[1].'..'.$tmparray[2].'</td></tr>';
	  }
	
      }
  }
fclose($fp);















//stop here
echo '</table>';
echo '</td></tr><tr><TD colspan="3"><img src="../images/760_trans_spacer.gif"></td></tr></table>';
//echo '<br><br>';
echo "</font></font>";

//Ok Here is where we need to add the map file 

echo '<map name="var_'.$gene_id.'">';
$var_map = "../gpdata/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/images/var_".$gene_id.".map";
if (!$fp = fopen($var_map,"r"))
  {
    echo "map file not found\n";
    //exit;
  }
else
{	
    while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }

fclose($fp);
}
echo "</map>";

echo '<map name="evid_'.$gene_id.'">';
$evid_map = "../gpdata/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/images/evid_".$gene_id.".map";
if (!$fp = fopen($evid_map,"r"))
  {
    echo "map file not found\n";
    //exit;
  }
else
{	
    while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }

fclose($fp);
}
echo "</map>";





//Insert Variant and evidence pictures here
echo '<table border=0 cellspacing=0 cellpadding=0>';
echo '<tr><td width = 30>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td><td>';
echo '<BR><img border ="0" src="../gpdata/builds/'.$build.'/symlinks/'.$chr_id.'/images/'.$seq_id.'/images/var_'.$gene_id.'.png"  usemap="#var_'.$gene_id.'" >';
echo '</td></tr></table>';
//begin of est table



$filename = "../gpdata/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/images/evid_".$gene_id.".ps.evid.list";

#print "$filename";
$fp = fopen($filename,"r");


// Ok Lets Start Here
$count = 0;
echo '<form target=_new action="../src/make_est_alignments/est_align.pl" method="POST">';
echo '<input type="hidden" name="build" value="'.$build.'">';
echo '<input type="hidden" name="chr_id" value="'.$chr_id.'">';
echo '<input type="hidden" name="seq_id" value="'.$seq_id.'">';
echo '<input type="hidden" name="gene_id" value="'.$gene_id.'">';
echo '<input type="hidden" name="gene_num" value="'.$gene_number.'">';
echo '<input type="hidden" name="var_num" value="'.$variant.'">';

echo '<table border="0" cellpadding ="0" cellspacing="0" width="1400"><tr><td>'; //First main cell
echo '<table border="0" cellpadding ="0" cellspacing="0" width ="100%">'; //Est table
echo '<TR><td height="21" width = "10"></td></tr>';
echo '<TR><td height="21" width = "10"></td></tr>';
echo '<TR><td height="21" width = "10"></td></tr>';
while (!feof($fp))
{
  $fstring = fgets($fp,4096);
  //  echo $fstring;
  $count++;
  $fstr = str_replace("\n","",$fstring);
  if ($fstr != "")
    {
      echo '<TR><td height="20" width = "10" valign="center">';
      echo '<input type="checkbox" name="evid'.$count.'" value = "'.$fstr.'">';
	#echo '<font size ="-2">'.$fstr.'</font>';
      echo '</td></tr>';
      echo "\n";
    }
} #end while infile
echo '</table>'; // end est table
echo '</td>'; //end main cell 1
echo '<td valign = "top"><img usemap = "#evid_'.$gene_id.'" border="0" src="../gpdata/builds/'.$build.'/symlinks/'.$chr_id.'/images/'.$seq_id.'/images/evid_'.$gene_id.'.png">'; //EST PICT HERE
echo '</td></tr></table>'; // end main table

echo '<input type="submit" value="View Selected Alignments"></form>';






//// End of est table
echo '<br><br>';


echo 'mRNA Sequence<BR>';
$mrna_file = "../gpdata/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/mrna/".$gene_id.".mrna.html";
if (!$fp=fopen($mrna_file,"r"))
  {
    echo "$mrna_file file not found\n";
    exit;
  }
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }
fclose($fp);

echo '</b><br><br></TT>Protein Sequence<BR>';
$prot_file = "../gpdata/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/prot/".$gene_id.".prot.html";
if (!$fp=fopen($prot_file,"r"))
  {
    echo "$prot_file file not found\n";
    exit;
  }
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }
fclose($fp);

echo "<BR><BR>";


require("nav_end.php");

?>
