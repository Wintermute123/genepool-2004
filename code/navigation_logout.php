<?

//Check to see if the user went through the login page and if not
//make em login.  If they have then display page as normal
if (!isset($validated_user))
  header("Location: ../index.html");
if ($validated_user != "TRUE" )
    header("Location: ../index.html");
session_destroy();

global $chr_id;
if (!isset($chr_id))
  $chr_id = 17;



//Bring in the required files
require("global/global_vars.php");
require("global/global_functions.php");

// Start of HTML code
echo '<html><head>';

//Grab the stylesheet
echo '<link rel=stylesheet type="text/css" href="global/genome.css">';
//insert javascript here
echo '<title>'.$page_title."</title>";
?>
<script>

function demoversion()
{
    alert("  Selected features are not\n    available in the demo version\n\nPlease contact Genomix Corporation\n865-220-0043  sales@genomix.com");
}

function openwin(pageurl)
  {
  	var myBars = 'directories=no,location=no,menubar=no,status=yes';
	myBars += ',titlebar=no,toolbar=no';
	var myOptions = 'scrollbars=yes,width=785,height=500,resizable=yes';
	var myFeatures = myBars + ',' + myOptions;
	window.open(pageurl, 'GenePOOL', myFeatures);
  }

function selchr()
{
	var val = document.form1.chr_id.options[ document.form1.chr_id.selectedIndex ].value;
	if( val != 0 )
		document.form1.submit();
	else return false;
}

</script>
<?
echo '</head>';
?>

<body MARGINHEIGHT="0" MARGINWIDTH="2">
<table cellpadding='0' cellspacing='0' width='990' border='0' class="navbar">
<tr>
<td colspan ="3"><a href="chromosome.php"><img src="../images/small-genepoolwebpage.jpg"  alt="Genepool by Genomix Corporation, (c)Copyright 2001,2002 by Genomix Corporation.  All Rights Reserved" border='0'></a></td>
</tr>

<tr valign="center" align="center">

<td colspan="1" align="center" valign="middle">&nbsp;</td></tr>
</table><p>


