<?session_start();
//session_register("build");
if (!isset($chr_id))
  $chr_id=17;
$page_title="GenePOOL Select Gene for build ";
$page_title.=$build;
$page_title.=" Contig ";
$page_title.=$seq_id;
require("nav_begin.php");

if (!isset($build))
  {
    echo 'Usage: gene.php?chr_id=1&build=testgenomix&seq_id=NT_004488.7';
	exit;
  }
  

mysql_connect($db_host,$db_username,$db_password);
mysql_select_db($build);

$sql = "select gene_id,gene_number,variant_number,product,seq_id from chr_".$chr_id."_summary where gene_id = '$gene_id'";
//gene.php?chr_id=1&build=novel&gene_id=GXDB000200894&type=2&percent_id=70&exon_count=1&evidence_count=4&snp_count=1088
$result=mysql_query($sql);
$row = mysql_fetch_row($result) or must_die(mysql_error());

echo '<font face="arial">';
echo '<font size="+1">Gene number '.$row[1].' variant '.$row[2].' - '.$row[0].'</font><BR><b>'.$row[3].'</b><BR>';
$variant=$row[2];
$prot_length=$row[4];
$gene_number=$row[1];
//$sql = "select gene.type,gene.begin,gene.end,gene.strand,sequence.seq_id, gene.prot_length from gene, sequence where gene_id = '$gene_id' and gene.seq_id = sequence.seq_id";
$sql = "select type, gene_begin, gene_end, strand, seq_id, protein_length from chr_".$chr_id."_summary where gene_id = '$gene_id' ";

//echo $sql;

$result=mysql_query($sql) or must_die(mysql_error());
$row=mysql_fetch_row($result);
//echo 'This gene was modeled by'.$row[0].' Located at '.$row[1].'..'.$row[2].' ('.($row[2]-$row[1]).' base pairs) on the '.$row[3].' strand of contig '.$row[4];
?>
<table width="760" cellspacing="0" cellpadding="0"><TR><td width=300 valign="top">
<TABLE cellspacing="0" cellpadding="0" width="299">
  <tr>
  	<TH colspan=2>Gene Information</TH>
  </tr>
 <tr>
  	<TH colspan=2><?echo $gene_id?></TH>
  </tr>
  <TR>
    <Td width=150>Type</Td>
    <TD width=150><?
		if ($row[0] == 1)
		  echo "EXP";
		elseif ($row[0] == 2)
		  echo "Refseq";
		elseif ($row[0] == 3)
		  echo "Genbank"; ?></TD></TR>
  <TR>
    <TD class="second">Location</TD>
    <TD class="second"><?echo $row[1]."..".$row[2]?></TD></TR>
  <TR>
    <TD>Strand</TD>
    <TD><?if ($row[3] == "r") echo "Reverse"; elseif ($row[3] == "f") echo "Forward"; else echo "Unknown"; ?></TD></TR>
  <TR>
    <TD class="second">Contig</TD>
    <?$seq_id = $row[4];?>
    <TD class="second">
    <? $seq_string = '<a href = "contig.php?build='.$build.'&chr_id='.$chr_id.'&seq_id='.$row[4].'">'.$row[4].'</A>';?>
<?echo $seq_string;?></TD></TR>
  <TR>
    <TD>Var</TD>
    <TD><?echo $variant?></TD></TR>
  <TR>
    <TD class="second">Protein Length</TD>
    <TD class="second"><?echo $row[5]?></TD></TR>
  <TR>
    <TD>Blast</TD>
    <TD><?echo build_blast_link($build,$chr_id,$row[4],$gene_id,"View")?></TD></TR>
  <TR>
    <TD class="second">Exp</TD>
    <TD class="second"><?echo build_exp_link($build,$chr_id,$row[4],$gene_id); ?></TD></TR>
  </td>

  <TR>
    <TD>Protein Variants</TD>
    <TD><?echo show_prot_variants($build,$chr_id,$gene_id);?></td></tr>
    </TD>
    <TR><TD class="second">mRNA Variants</TD><td class="second"><?echo show_mrna_variants($build,$chr_id,$gene_id);?></td>
    </TD></tr>
</td>
<tr>
    <td>Genomic Sequence</td><td><?echo show_genomic_sequence($build,$chr_id,$seq_id,$gene_id); ?></td>
</td></tr>

<tr>
<td class="second">
Formatted Proteins</TD>
<td class="second"><?echo build_prot_pat_link($build,$chr_id,$seq_id,$gene_id,"");?></td></tr>
<TR>
<TD>Formatted mRNA</td>
<td><?echo build_mrna_pat_link($build,$chr_id,$seq_id,$gene_id,"");?></td></tr>
<TR><TD class="second">Isoform mRNA Alignments</td>
<td class="second"><?echo '<a href="show_mrna_isoform.php?build='.$build.'&chr_id='.$chr_id.'&seq_id='.$seq_id.'&gene_number='.$gene_number.'" target="_new">View</A>';?></td></tr>
<TR>
<TD>Est Alignments</td>
<td><?echo '<a href="../src/checkboxes/checkboxes.php?build='.$build.'&chr_id='.$chr_id.'&seq_id='.$seq_id.'&gene_id='.$gene_id.'&gene_num='.$gene_number.'&var_num='.$variant.'" target="_new">View</a>';?></td></tr>



</TABLE>
</TR>
<td width=460 align="center" valign="top">
<?

$seq_id = $row[4];
$sql="select begin,end,frame,genecoords_exon_begin, genecoords_exon_end from chr_".$chr_id."_exon where gene_id = '$gene_id' order by genecoords_exon_begin";
$result=mysql_query($sql) or must_die(mysql_error());
echo '<table border="0" cellspacing="0" cellpadding="0" width ="400" >';
echo '<tr><th colspan="5" align="center">Exon Table</th></tr>';
echo '<tr><th>Id</th><th>Size</th><th>Frame</th><TH>Gene Location</TH><th>Contig Location</th></tr>';

$exoncount=0;
$previousend=0;
$previouscontigend=0;
for ($k =0; $row=mysql_fetch_row($result); $k++)
  { $exoncount++;
    $contigbegin=$row[0];
	$contigend=$row[1];
	$geneframe=$row[2];
    $gene_left = $row[3];
    $gene_right = $row[4];
	$genelength=$contigend-$contigbegin;

	$total= $contigbegin - $previouscontigend;
	if ($k ==0)
	  $genestart=1;
	else
	  $genestart = $previousend+$total+1;
	$geneend=$genestart+$genelength;
	$previousend=$geneend;
	$previouscontigend=$contigend;
	if ($k % 2 == 0)
	  {
		echo '<tr><td align="right" width="10">'.$exoncount.'</TD><td align="center">'.$genelength.'</td><td align="center">'.$geneframe.'</td><td align="center">'.$gene_left.'..'.$gene_right.'</td>';
		echo '<td align="center">'.$contigbegin.'..'.$contigend.'</td></tr>';
	  }
	else
	  {
		echo '<tr class=\'second\'><td align="right" width="10">'.$exoncount.'</TD><td align="center">'.$genelength.'</td><td align="center">'.$geneframe.'</td><td align="center">'.$gene_left.'..'.$gene_right.'</td>';
		echo '<td align="center">'.$row[0].'..'.$row[1].'</td></tr>';
	  }
  }
echo '</table>';
echo '</td></tr></table>';



//ok import the evid_map and initalize it.
//ok import the evid_map and initalize it.
echo '<map name="evid_'.$gene_id.'">';
$evidence_map = "../builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/evid_".$gene_id.".map";
if (!$fp = fopen($evidence_map,"r"))
  {
    echo "map file not found\n";
    //exit;
  }
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
        echo "$line\n";
  }
fclose($fp);
echo "</map>";

echo '<map name="var_'.$gene_id.'">';
$evidence_map = "../builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/var_".$gene_id.".map";
if (!$fp = fopen($evidence_map,"r"))
  {
    echo "map file not found\n";
    //exit;
  }
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }
fclose($fp);
echo "</map>";


///Show evidence png
//echo '<br><br><img src="/home/ray/genepool/builds/genepooltest/symlinks/".$chr_id."/images/evidences/var_'.$gene_id.'.png" border="1"><br><br>';
echo '<table border="0" width="990"><tr><td>';
echo '<img src="../../builds/'.$build.'/symlinks/'.$chr_id.'/images/'.$seq_id.'/var_'.$gene_id.'.png" border="0" usemap="#var_'.$gene_id.'"><br>';
echo '<img src="../builds/'.$build.'/symlinks/'.$chr_id.'/images/'.$seq_id.'/evid_'.$gene_id.'.png" border="0" usemap="#evid_'.$gene_id.'">';

echo '</td></tr></table><BR>';
echo "</font></font>";
echo 'mRNA Sequence<BR>';
$mrna_file = "/home/ray/genepool/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/mrna/".$gene_id.".mrna.html";
if (!$fp=fopen($mrna_file,"r"))
  {
    echo "$mrna_file file not found\n";
    exit;
  }
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }
fclose($fp);

echo '</b><br><br>Protein Sequence<BR>';
$prot_file = "/home/ray/genepool/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/prot/".$gene_id.".prot.html";
if (!$fp=fopen($prot_file,"r"))
  {
    echo "$prot_file file not found\n";
    exit;
  }
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }
fclose($fp);

echo "<BR><BR>";


require("nav_end.php");

?>
