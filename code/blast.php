 <?session_start();
//session_register("build");
if (!isset($chr_id))
  $chr_id=1;
$page_title="GenePOOL Select Gene for build ";
$page_title.=$build;
$page_title.=" Contig ";
$page_title.=$seq_id;
require("nav_begin.php");
?>
  <FORM ACTION="../src/blast_search/blast_search.pl" METHOD="POST">
    <TABLE WIDTH="100%" CELLSPACING=0 BORDER=0 CELLPADDING=0>
    <tr>
      <td>Search Sequence</td><td><TEXTAREA  SELECTED COLS="65" ROWS="8" NAME="sequence"></textarea></td>
    </tr>
    <tr>
      <td>Program</td><td><select name = "program">
<option value="blastn">BLASTN (Nuc/Nuc)</option>
<option value="blastp">BLASTP (Prot/Prot)</option>
<option value="blastx">BLASTX (Nuc/Prot)</option>
<option value="tblastn">TBLASTN (Prot/Nuc)</option>
</select></td>
    </tr>
    <tr>
      <td>Choose database</td><td><SELECT NAME="blast_db">
                                <OPTION value="gpprot" SELECTED>Genepool proteins</option>
                                <option value="gpmrna">Genepool mRNA</option>
                              </SELECT>   </td>
    </tr>

<tr><td>Alignment View</td>
<TD><select name = "view">
<option value="0">(-m 0) pairwise</option>
<option value="1">(-m 1) Query-anchored showing identies</option>
<option value="2">(-m 2) Query-anchored no identies</option>
<option value="3">(-m 3)flat query-anchored, show identities</option>
<option value="4">(-m 4)flat query-anchored, no identities</option>
<option value="5">(-m 5)query-anchored no identities and blunt ends</option>
<option value="6">(-m 6)flat query-anchored, no identities and blunt ends</option>
<option value="7">(-m 7)XML Blast output</option>
<option value="8">(-m 8)tabular</option>
<option value="9">(-m 9)tabular with comment lines [Integer]</option>
</select></td>
</tr>
    <tr>
      <td>Matrix</td> <td><SELECT NAME="matrix">
                            <option value="BLOSUM62">BLOSUM62</option>
                            <OPTION VALUE="BLOSUM80" SELECTED>BLOSUM80</option>
                          </SELECT></td>
    </tr>
    <tr>
      <td>E-Value</td>   <td><SELECT NAME="evalue"><option value="huge">10</option>
<option value="big">0.01</option>
<option value="small">1e-05</option>
<option value="tiny">1e-20</option>
</select></select></td>
    </tr>
     <tr>
       <td>Word Size</td> <td><select name = "wsize">
<option value="0" selected>0</option>
<option value="7">7</option>
<option value="11">11</option>
<option value="15">15</option>
</select></td>
     </tr>
     <tr>
       <TD COLSPAN=2><INPUT TYPE="submit" NAME="Run Blast"><INPUT TYPE="reset"></TD>
     </tr>
 </TABLE>
  </FORM>
  <?
  require("nav_end.php");
  ?>
