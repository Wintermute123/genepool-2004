<html><head><link rel=stylesheet type="text/css" href="global/genome.css"><title>GenePOOL Select Gene for build testgp Contig </title>
<script>

function demoversion()
{
    alert("  Selected features are not\n    available in the demo version\n\nPlease contact Genomix Corporation\n865-220-0043  sales@genomix.com");
}

function openwin(pageurl)
  {
  	var myBars = 'directories=no,location=no,menubar=no,status=yes';
	myBars += ',titlebar=no,toolbar=no';
	var myOptions = 'scrollbars=yes,width=785,height=500,resizable=yes';
	var myFeatures = myBars + ',' + myOptions;
	window.open(pageurl, 'GenePOOL', myFeatures);
  }
  
function selchr()
{
	var val = document.form1.chr_id.options[ document.form1.chr_id.selectedIndex ].value;
	if( val != 0 )
		document.form1.submit();
	else return false;
}

</script>
</head>

  <TABLE WIDTH="100%" cellspacing="0" cellpadding="0" border="0">
  <TR><td colspan="3" ><img src = "../images/760_trans_spacer.gif"></td></tr>
    <TR backgound="../images/header_background.jpg">
    <td colspan="3" background="../images/header_background.jpg"><IMG SRC="../images/genepool_logo.jpg" WIDTH=667 HEIGHT=97 BORDER=0></td>
<tr valign="center" align="center">

<td colspan="1" align="center" valign="middle"><form name="form1" action="select_chromosome.php"><input type="hidden" name="build" value="testgp"><select name="chr_id"  id="chr_id" width="25" onchange="selchr()"><option value="1" >1</option>
<option value="2" >2</option>
<option value="3" >3</option>
<option value="4" >4</option>
<option value="5" >5</option>
<option value="6" >6</option>
<option value="7" >7</option>
<option value="8" >8</option>
<option value="9" >9</option>
<option value="10" >10</option>
<option value="11" >11</option>
<option value="12" >12</option>
<option value="13" >13</option>
<option value="14" >14</option>
<option value="15" >15</option>
<option value="16" >16</option>
<option value="17" selected >17</option>
<option value="18" >18</option>
<option value="19" >19</option>
<option value="20" >20</option>
<option value="21" >21</option>
<option value="22" >22</option>
<option value="X" >X</option>
<option value="Y" >Y</option>
<option value="U" >Un</option>
</select></form></td>
<td colspan="1" Align="center" valign="middle"><b>Chromosome 17 for build testgp&nbsp;</b></td>
<td colspan="1" align="center" valign="middle">
<a href="search.php?build=testgp&chr_id=17">Search Page</A></td>
    </TR>
  </TABLE>
<BR>
<font face="arial"><font size="+1">Gene number 9 variant 1 - GXDB000509909</font><BR><b> Similar to RIKEN cDNA1810073N04 gene [Homo sapiens]</b><BR><table width="100%" cellspacing="0" cellpadding="0">
<TR><td width="45%" valign="top">
<TABLE cellspacing="0" cellpadding="0" width="90%">
<tr>
<TH colspan=2>Gene Information</TH>
</tr>
<tr>
<TH colspan=2>GXDB000509909</TH>
  </tr>

    <TD class="second">Location</TD>
    <TD class="second">196241..272706</TD></TR>
  <TR>
    <TD>Strand</TD>
    <TD>Reverse</TD></TR>
  <TR>
    <TD class="second">Contig</TD>
        <TD class="second">
    <a href = "contig.php?build=testgp&chr_id=17&seq_id=NT_010661.7">NT_010661.7</A></TD></TR>
    <TR>
    <TD>Chromosome</TD>
    <TD>
    <a href="select_chromosome.php?build=testgp&chr_id=17">17</a>    </TD>
    </TR>
  <TR>
    <TD class="second">Var</TD>
    <TD class="second">1</TD></TR>
  <TR>
    <TD >Protein Length</TD>
    <TD >1093</TD></TR>
  <TR>
    <TD class="second">Blast</TD>
    <TD class="second"><a href="javascript:openwin('show_blast_report.php?filename=../gpdata/builds/testgp/symlinks/17/blasted/out_GXDB000509909-seq=NT_010661.7')">View</a></TD></TR>
  <TR>
    <TD >Exp</TD>
    <TD ><a href="javascript:openwin('show_exp_report.php?filename=../gpdata/builds/testgp/symlinks/17/NT_010661.7.out')">View</a></TD></TR>
  </td>

  <TR>
    <TD class="second">Protein Variants</TD>
    <TD class="second"><a href="javascript:openwin('show_prot_variants.php?build=testgp&chr_id=17&gene_id=GXDB000509909')">View</a></td></tr>
    </TD>
    <TR><TD>mRNA Variants</TD><td ><a href="javascript:openwin('show_mrna_variants.php?build=testgp&chr_id=17&gene_id=GXDB000509909')">View</a></td>
    </TD></tr>
</td>
<tr>
    <td class="second">Genomic Sequence</td><td class="second"><a href="javascript:openwin('show_genomic_sequence.php?filename=../gpdata/builds/testgp/symlinks/17/images/NT_010661.7/genomic/GXDB000509909.genomic.html')">View</a></td>
</td></tr>

<tr>
<td >
Formatted Proteins</TD>
<td ><a href="javascript:openwin('show_patent_prot.php?filename=../gpdata/builds/testgp/symlinks/17/images/NT_010661.7/patent/GXDB000509909.patprot')">View</a></td></tr>
<TR>
<TD class="second">Formatted mRNA</td>
<td class="second"><a href="javascript:openwin('show_patent_mrna.php?filename=../gpdata/builds/testgp/symlinks/17/images/NT_010661.7/patent/GXDB000509909.patmrna')">View</a></td></tr>
<TR><TD >Isoform mRNA Alignments</td>
<td><a href="show_mrna_isoform.php?build=testgp&chr_id=17&seq_id=NT_010661.7&gene_number=9" target="_new">View</A></td></tr>



</TABLE>
</Td><TD width = "1%">&nbsp;&nbsp;</td>
<td width="45%" align="center" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width ="100%" ><tr><th colspan="4" align="center">Master Exon Table</th></tr><tr><th>Id</th><th>Size</th><Th>Active in Variant</th><th>Contig Location</th></tr><tr><td align="right" class="second">1</td><td align="right" class="second">1669</td><td class="second" align="center">Yes</td><td align="center" class="second">197910..196241</td></tr><tr><td align="right">2</td><td align="right">166</td><td align="center">No</td><td align="center">198288..198122</td></tr><tr><td align="right" class="second">3</td><td align="right" class="second">176</td><td class="second" align="center">Yes</td><td align="center" class="second">198298..198122</td></tr><tr><td align="right">4</td><td align="right">23</td><td align="center">No</td><td align="center">227438..227415</td></tr><tr><td align="right" class="second">5</td><td align="right" class="second">80</td><td class="second" align="center">No</td><td align="center" class="second">228918..228838</td></tr><tr><td align="right">6</td><td align="right">119</td><td align="center">Yes</td><td align="center">228957..228838</td></tr><tr><td align="right" class="second">7</td><td align="right" class="second">222</td><td class="second" align="center">No</td><td align="center" class="second">230028..229806</td></tr><tr><td align="right">8</td><td align="right">488</td><td align="center">Yes</td><td align="center">230028..229540</td></tr><tr><td align="right" class="second">9</td><td align="right" class="second">167</td><td class="second" align="center">Yes</td><td align="center" class="second">230585..230418</td></tr><tr><td align="right">10</td><td align="right">156</td><td align="center">Yes</td><td align="center">237742..237586</td></tr><tr><td align="right" class="second">11</td><td align="right" class="second">106</td><td class="second" align="center">Yes</td><td align="center" class="second">248380..248274</td></tr><tr><td align="right">12</td><td align="right">111</td><td align="center">Yes</td><td align="center">249986..249875</td></tr><tr><td align="right" class="second">13</td><td align="right" class="second">182</td><td class="second" align="center">Yes</td><td align="center" class="second">253513..253331</td></tr><tr><td align="right">14</td><td align="right">102</td><td align="center">Yes</td><td align="center">254495..254393</td></tr><tr><td align="right" class="second">15</td><td align="right" class="second">124</td><td class="second" align="center">Yes</td><td align="center" class="second">258101..257977</td></tr><tr><td align="right">16</td><td align="right">143</td><td align="center">Yes</td><td align="center">259706..259563</td></tr><tr><td align="right" class="second">17</td><td align="right" class="second">93</td><td class="second" align="center">Yes</td><td align="center" class="second">260877..260784</td></tr><tr><td align="right">18</td><td align="right">45</td><td align="center">Yes</td><td align="center">262276..262231</td></tr><tr><td align="right" class="second">19</td><td align="right" class="second">39</td><td class="second" align="center">Yes</td><td align="center" class="second">267194..267155</td></tr><tr><td align="right">20</td><td align="right">118</td><td align="center">No</td><td align="center">267194..267076</td></tr><tr><td align="right" class="second">21</td><td align="right" class="second">495</td><td class="second" align="center">Yes</td><td align="center" class="second">272706..272211</td></tr></table></td></tr><tr><TD colspan="3"><img src="../images/760_trans_spacer.gif"></td></tr></table></font></font><map name="var_GXDB000509909"><area shape="rect" coords="1,3,990,23" href="./gene.php?build=testgp&chr_id=17&gene_id=GXDB000509909">
<area shape="rect" coords="1,23,990,43" href="./gene.php?build=testgp&chr_id=17&gene_id=GXDB000509910">
<area shape="rect" coords="1,43,990,63" href="./gene.php?build=testgp&chr_id=17&gene_id=GXDB000509911">
<area shape="rect" coords="1,63,990,83" href="./gene.php?build=testgp&chr_id=17&gene_id=GXDB000509912">
<area shape="rect" coords="1,83,990,103" href="./gene.php?build=testgp&chr_id=17&gene_id=GXDB000509913">
<area shape="rect" coords="1,103,990,123" href="./gene.php?build=testgp&chr_id=17&gene_id=GXDB000509914">
<area shape="rect" coords="1,123,990,143" href="./gene.php?build=testgp&chr_id=17&gene_id=GXDB000509915">
<area shape="rect" coords="1,163,990,183" href="javascript:openwin('./show_ncbi.php?reference=XM_085558.1')">
<area shape="rect" coords="1,183,990,203" href="javascript:openwin('./show_ncbi.php?reference=XM_113895.1')">
<area shape="rect" coords="1,203,990,223" href="javascript:openwin('./show_ncbi.php?reference=XM_118935.1')">
</map><map name="evid_GXDB000509909"><area shape="rect" coords="1,63,990,83" href="javascript:openwin('./show_ncbi.php?reference=AW732596.1')">
<area shape="rect" coords="1,83,990,103" href="javascript:openwin('./show_ncbi.php?reference=BAB25384.1_gp')">
<area shape="rect" coords="1,103,990,123" href="javascript:openwin('./show_ncbi.php?reference=AAH14642.1_gp')">
<area shape="rect" coords="1,123,990,143" href="javascript:openwin('./show_ncbi.php?reference=AAH03308.1_gp')">
<area shape="rect" coords="1,143,990,163" href="javascript:openwin('./show_ncbi.php?reference=AW206173.1')">
<area shape="rect" coords="1,163,990,183" href="javascript:openwin('./show_ncbi.php?reference=AA477703.1')">
<area shape="rect" coords="1,183,990,203" href="javascript:openwin('./show_ncbi.php?reference=BF806183.1')">
<area shape="rect" coords="1,203,990,223" href="javascript:openwin('./show_ncbi.php?reference=R50242.1')">
<area shape="rect" coords="1,223,990,243" href="javascript:openwin('./show_ncbi.php?reference=BM465668.1')">
<area shape="rect" coords="1,243,990,263" href="javascript:openwin('./show_ncbi.php?reference=BB656258.1')">
<area shape="rect" coords="1,263,990,283" href="javascript:openwin('./show_ncbi.php?reference=R46419.1')">
<area shape="rect" coords="1,283,990,303" href="javascript:openwin('./show_ncbi.php?reference=BF811192.1')">
<area shape="rect" coords="1,303,990,323" href="javascript:openwin('./show_ncbi.php?reference=BM946878.1')">
<area shape="rect" coords="1,323,990,343" href="javascript:openwin('./show_ncbi.php?reference=BQ086785.1')">
<area shape="rect" coords="1,343,990,363" href="javascript:openwin('./show_ncbi.php?reference=BF236105.1')">
<area shape="rect" coords="1,363,990,383" href="javascript:openwin('./show_ncbi.php?reference=BM950209.1')">
<area shape="rect" coords="1,383,990,403" href="javascript:openwin('./show_ncbi.php?reference=AK007977.1')">
<area shape="rect" coords="1,403,990,423" href="javascript:openwin('./show_ncbi.php?reference=NM_024249.1')">
<area shape="rect" coords="1,423,990,443" href="javascript:openwin('./show_ncbi.php?reference=BI656563.1')">
<area shape="rect" coords="1,443,990,463" href="javascript:openwin('./show_ncbi.php?reference=BI645899.1')">
<area shape="rect" coords="1,463,990,483" href="javascript:openwin('./show_ncbi.php?reference=BF535806.1')">
<area shape="rect" coords="1,483,990,503" href="javascript:openwin('./show_ncbi.php?reference=BI658310.1')">
<area shape="rect" coords="1,503,990,523" href="javascript:openwin('./show_ncbi.php?reference=BM994356.1')">
<area shape="rect" coords="1,523,990,543" href="javascript:openwin('./show_ncbi.php?reference=BI684440.1')">
<area shape="rect" coords="1,543,990,563" href="javascript:openwin('./show_ncbi.php?reference=BB652145.1')">
<area shape="rect" coords="1,563,990,583" href="javascript:openwin('./show_ncbi.php?reference=BI156285.1')">
<area shape="rect" coords="1,583,990,603" href="javascript:openwin('./show_ncbi.php?reference=BG575118.1')">
<area shape="rect" coords="1,603,990,623" href="javascript:openwin('./show_ncbi.php?reference=M78949.1')">
<area shape="rect" coords="1,623,990,643" href="javascript:openwin('./show_ncbi.php?reference=AI157644.1')">
<area shape="rect" coords="1,643,990,663" href="javascript:openwin('./show_ncbi.php?reference=BG469291.1')">
<area shape="rect" coords="1,663,990,683" href="javascript:openwin('./show_ncbi.php?reference=BG469529.1')">
<area shape="rect" coords="1,683,990,703" href="javascript:openwin('./show_ncbi.php?reference=BE541641.1')">
<area shape="rect" coords="1,703,990,723" href="javascript:openwin('./show_ncbi.php?reference=BG327743.1')">
<area shape="rect" coords="1,723,990,743" href="javascript:openwin('./show_ncbi.php?reference=AI182746.1')">
<area shape="rect" coords="1,743,990,763" href="javascript:openwin('./show_ncbi.php?reference=BI454279.1')">
<area shape="rect" coords="1,763,990,783" href="javascript:openwin('./show_ncbi.php?reference=BF454750.1')">
<area shape="rect" coords="1,783,990,803" href="javascript:openwin('./show_ncbi.php?reference=BF791473.1')">
<area shape="rect" coords="1,803,990,823" href="javascript:openwin('./show_ncbi.php?reference=BM546467.1')">
<area shape="rect" coords="1,823,990,843" href="javascript:openwin('./show_ncbi.php?reference=BM312729.1')">
<area shape="rect" coords="1,843,990,863" href="javascript:openwin('./show_ncbi.php?reference=BI819701.1')">
<area shape="rect" coords="1,863,990,883" href="javascript:openwin('./show_ncbi.php?reference=BE375936.1')">
<area shape="rect" coords="1,883,990,903" href="javascript:openwin('./show_ncbi.php?reference=AW071908.1')">
<area shape="rect" coords="1,903,990,923" href="javascript:openwin('./show_ncbi.php?reference=BF355833.1')">
<area shape="rect" coords="1,923,990,943" href="javascript:openwin('./show_ncbi.php?reference=BG984796.1')">
<area shape="rect" coords="1,943,990,963" href="javascript:openwin('./show_ncbi.php?reference=BI055877.1')">
<area shape="rect" coords="1,963,990,983" href="javascript:openwin('./show_ncbi.php?reference=BG421914.1')">
<area shape="rect" coords="1,983,990,1003" href="javascript:openwin('./show_ncbi.php?reference=AAH05641.1_gp')">
<area shape="rect" coords="1,1003,990,1023" href="javascript:openwin('./show_ncbi.php?reference=BG746680.1')">
<area shape="rect" coords="1,1023,990,1043" href="javascript:openwin('./show_ncbi.php?reference=BG823175.1')">
<area shape="rect" coords="1,1043,990,1063" href="javascript:openwin('./show_ncbi.php?reference=BF568176.1')">
<area shape="rect" coords="1,1063,990,1083" href="javascript:openwin('./show_ncbi.php?reference=BM042226.1')">
<area shape="rect" coords="1,1083,990,1103" href="javascript:openwin('./show_ncbi.php?reference=BI907065.1')">
<area shape="rect" coords="1,1103,990,1123" href="javascript:openwin('./show_ncbi.php?reference=BM544970.1')">
<area shape="rect" coords="1,1123,990,1143" href="javascript:openwin('./show_ncbi.php?reference=BM674474.1')">
<area shape="rect" coords="1,1143,990,1163" href="javascript:openwin('./show_ncbi.php?reference=BQ000855.1')">
<area shape="rect" coords="1,1163,990,1183" href="javascript:openwin('./show_ncbi.php?reference=AAF48000.2_gp')">
<area shape="rect" coords="1,1183,990,1203" href="javascript:openwin('./show_ncbi.php?reference=AAK58050.1_gp')">
<area shape="rect" coords="1,1203,990,1223" href="javascript:openwin('./show_ncbi.php?reference=A23787.1_prt')">
<area shape="rect" coords="1,1223,990,1243" href="javascript:openwin('./show_ncbi.php?reference=BE273548.1')">
<area shape="rect" coords="1,1243,990,1263" href="javascript:openwin('./show_ncbi.php?reference=BE870677.1')">
<area shape="rect" coords="1,1263,990,1283" href="javascript:openwin('./show_ncbi.php?reference=AAC27038.1_gp')">
<area shape="rect" coords="1,1283,990,1303" href="javascript:openwin('./show_ncbi.php?reference=AAC27037.1_gp')">
<area shape="rect" coords="1,1303,990,1323" href="javascript:openwin('./show_ncbi.php?reference=BM546967.1')">
<area shape="rect" coords="1,1323,990,1343" href="javascript:openwin('./show_ncbi.php?reference=BG748703.1')">
<area shape="rect" coords="1,1343,990,1363" href="javascript:openwin('./show_ncbi.php?reference=BI161479.1')">
<area shape="rect" coords="1,1363,990,1383" href="javascript:openwin('./show_ncbi.php?reference=BF663461.1')">
</map><table border=0 cellspacing=0 cellpadding=0><tr><td width = 30>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td><td><BR><img border ="0" src="../gpdata/builds/testgp/symlinks/17/images/NT_010661.7/images/var_GXDB000509909.png"  usemap="#var_GXDB000509909" ></td></tr></table><form action="../src/make_est_alignments/est_align.pl" method="POST"><input type="hidden" name="build" value="testgp"><input type="hidden" name="chr_id" value="17"><input type="hidden" name="seq_id" value="NT_010661.7"><input type="hidden" name="gene_id" value="GXDB000509909"><input type="hidden" name="gene_num" value="9"><input type="hidden" name="var_num" value="1"><table border="0" cellpadding ="0" cellspacing="0" width="1400"><tr><td><table border="0" cellpadding ="0" cellspacing="0" width ="100%"><TR><td height="21" width = "10">&nbsp; &nbsp;</td></tr><TR><td height="21" width = "10">&nbsp; &nbsp;</td></tr><TR><td height="21" width = "10">&nbsp; &nbsp;</td></tr><TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid1" value = "AW732596.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid2" value = "BAB25384.1_gp"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid3" value = "AAH14642.1_gp"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid4" value = "AAH03308.1_gp"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid5" value = "AW206173.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid6" value = "AA477703.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid7" value = "BF806183.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid8" value = "R50242.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid9" value = "BM465668.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid10" value = "BB656258.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid11" value = "R46419.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid12" value = "BF811192.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid13" value = "BM946878.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid14" value = "BQ086785.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid15" value = "BF236105.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid16" value = "BM950209.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid17" value = "AK007977.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid18" value = "NM_024249.1_ref"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid19" value = "BI656563.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid20" value = "BI645899.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid21" value = "BF535806.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid22" value = "BI658310.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid23" value = "BM994356.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid24" value = "BI684440.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid25" value = "BB652145.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid26" value = "BI156285.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid27" value = "BG575118.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid28" value = "M78949.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid29" value = "AI157644.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid30" value = "BG469291.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid31" value = "BG469529.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid32" value = "BE541641.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid33" value = "BG327743.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid34" value = "AI182746.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid35" value = "BI454279.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid36" value = "BF454750.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid37" value = "BF791473.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid38" value = "BM546467.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid39" value = "BM312729.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid40" value = "BI819701.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid41" value = "BE375936.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid42" value = "AW071908.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid43" value = "BF355833.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid44" value = "BG984796.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid45" value = "BI055877.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid46" value = "BG421914.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid47" value = "AAH05641.1_gp"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid48" value = "BG746680.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid49" value = "BG823175.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid50" value = "BF568176.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid51" value = "BM042226.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid52" value = "BI907065.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid53" value = "BM544970.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid54" value = "BM674474.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid55" value = "BQ000855.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid56" value = "AAF48000.2_gp"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid57" value = "AAK58050.1_gp"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid58" value = "A23787.1_prt_pat"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid59" value = "BE273548.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid60" value = "BE870677.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid61" value = "AAC27038.1_gp"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid62" value = "AAC27037.1_gp"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid63" value = "BM546967.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid64" value = "BG748703.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid65" value = "BI161479.1_gb"></td></tr>
<TR><td height="20" width = "10" valign="center"><input type="checkbox" name="evid66" value = "BF663461.1_gb"></td></tr>
</table></td><td valign = "top"><img usemap = "#evid_GXDB000509909" border="0" src="../gpdata/builds/testgp/symlinks/17/images/NT_010661.7/images/evid_GXDB000509909.png"></td></tr></table><input type="submit" value="View Selected Alignments"></form><br><br>mRNA Sequence<BR><tt><font style="Courier"   color="#000000"> <span class="exonedge2">CGTGGAAGGCGGAAGTGTGAGCGCCGCGGCGGCAGCTGAGTTGGGCTGAGGTGTCCCTAG</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge2">CTGGCTCTGCGGCTCTTCCGGGTCTGGGCTCGGAGATTCACAGGCGGCCCGCGAGGCCGA</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge2">GCGAGGGACGCATGGCCCTGAGGCGGCCGCAGGGCTTGGCGGGGTCCGGAGGTTGACCTC</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge2">GCCCCCGCAGCCGGCCTTCGAGGCTGCCTCCTCCAGGCAGCCTCTGGGGCCCGCGCCCGC</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge2">GCCTGCTCAGGCTCCCGTGTTCAGGCTGCCCATCCCCTCCCCACCGGCGTCCCGGACGTT</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge2">GGGACCTGTGACCGTGGCCTCGGGCTGGGCTTCCAAAGCCGGCCGCAGCCCGGCGACCCC</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge2">CNGAGGCCTCTCGCCCCGGGCCCCTAGACCTCTCACT</span><b><span class="exonedge2">ATGACCGCGGCCGCCGCCTCCAA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CTGGGGGCTGATCACGAACATCGTGAACAGCATCGTAGGGGTCAGTGTCCTCACCATGCC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CTTCTGCTTCAAACAG</span><span class="exonedge3">TGCGGCATCGTCCTGGGGGCGCTGCTCTTGGTCTTCTGCT</span><span class="exonedge2">CATT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CCACGCCTACGGGAAGGCAGGCAAGATGCTGGTGGAGACCAG</span><span class="exonedge3">CATGATCGGGCTGATGCT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GGGCACCTGCATCGCCTTCTACGTCGTGATCGGCGACTTGGGGTCCAACTTCTTTGCCCG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GCTGTTCGGGTTTCAG</span><span class="exonedge2">GTGGGCGGCACCTTCCGCATGTTCCTGCTGTTCGCCGTGTCGCT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">GTGCATCGTGCTCCCGCTCAGCCTGCAGCGGAACATGATGGCCTCCATCCAGTCCTTCAG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CGCCATGGCCCTCCTCTTCTACACCGTGTTCATGTTCGTG</span><span class="exonedge3">ATCGTGCTCTCCTCTCTCAA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GCACGGCCTCTTCAGTGGGCAGTGGCTGCGGCGGGTCAGCTACGTCCGCTGGGAGGGCGT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">CTTCCGCTGCATCCCCATCTTCGGCATGTCCTTCGCCTGCCAGTC</span><span class="exonedge2">CCAGGTGCTGCCCAC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CTACGACAGCCTGGATGAGCCGTCAGTGAAAACCATGAGCTCCATATTTGCTTCCTCCCT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">TAATGTGGTCACCACCTTCTACGTCATG</span><span class="exonedge3">GTGGGGTTTTTCGGCTACGTCAGCTTCACCGA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GGCCACGGCCGGCAACGTGCTCATGCACTTTCCCTCCAACCTGGTGACGGAGATGCTCCG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">TGTGGGCTTCATGATGTCAGTGGCTGTGGGCTTCCCCATGATGATCCTGCCATGCAGGCA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GGCCCTGAGCACGCTGCTGTGTGAGCAGCAG</span><span class="exonedge2">CAAAAAGATGGCACCTTTGCAGCAGGGGG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CTACATGCCCCCTCTCCGGTTTAAAGCACTTACCCTCTCTGTGGTGTTTGGAACCATGGT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">TGGTGGCATCCTTATCCCCAACG</span><span class="exonedge3">TGGAGACCATCCTGGGCCTCAC</span></b><b></font><span class="exonedge3"><U><font color="blue">A</u></font></span><span class="exonedge3"><font color="#000000" >GGAGCGACCATGGG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;(a,g)<BR>
<b></span><span class="exonedge3">AAGCCTCATCTGCTTCATCTGCCCGGCGCTGATCTACAAGAAAATCCACAAGAACGCACT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">TTCCTCCCAG</span><span class="exonedge2">GTGGTGCTGTGGGTCGGCCTGGGCGTCCTGGTGGTGAGCACTGTCACCAC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">ACTGTCTGTGAGCGAGGAGGTCCCCGAGGACTTGGCAGAGGAAGCCCCTGGCGGCCGGCT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">TGGAGAGGCCGAGGGTTTGATGAAGGTGGAGGCAGCGCGGCTCTCAG</span><span class="exonedge3">CCCAGGATCCGGT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">TGTGGCCGTGGCTGAGGATGGCCGGGAGAAGCCGAAGCTGCCGAAGGAGAGAGAGGAGCT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GGAGCAGGCCCAGATCAAGGGGCCCGTGGATGTGCCTGGACGGGAAGATGGCAAGGAGGC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">ACCGGAGGAGGCACAGCTCGATCGCCCTGGGCAAG</span><span class="exonedge2">GGATTGCTGTGCCTGTGGGCGAGGC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CCACCGCCACGAGCCTCCTGTTCCTCACGACAAGGTGGTGGTAGATGAAGGCCAAGACCG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">AGAGGTGCCAGAAGAGAACAAACCTCCATCCAGACACGCGGGCGGAAAGGCTCCAGGGGT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CCAGGGCCAGATGGCGCCGCCTCTGCCCGACTCAGAAAGAGAGAAACAAGAGCCGGAGCA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">GGGAGAGGTTGGGAAGAGGCCTGGACAGGCCCAGGCCTTGGAGGAGGCGGGTGATCTTCC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">TGAAGATCCCCAGAAAGTTCCAGAAGCAGATGGTCAGCCAGCTGTCCAGCCTGCAAAGGA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">GGACCTGGGGCCAGGAGACAGGGGCCTGCATCCTCGGCCCCAGGCAGTGCTGTCTGAGCA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">GCAGAACGGCCTGGCGGTGGGTGGAGGGGAAAAGGCCAAGGGGGGACCGCCGCCAGGCAA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CGCCGCCGGGGACACAGGGCAGCCCGCAGAGGACAGCGACCACG</span><span class="exonedge3">GTGGGAAGCCTCCCCT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">CCCAGCGGAGAAGCCGGCTCCAGGGCCTGGGCTGCCGCCCGAGCCTCGCGAGCAGAGGGA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">CGTGGAGCGAGCGGGTGGAAACCAGGCGGCCAGCCAGCTGGAGG</span><span class="exonedge2">AAGCTGGCAGGGCGGA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">GATGCTGGACCACGCCGTCCTGCTTCAGGTGATCAAAGAACAGCAGGTGCAGCAAAAGCG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CTTGCTGGACCAGCAGGAGAAGCTGCTGGCGGTGATCGAGGAGCAGCACAAGGAGATCCA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge2">CCAGCAGAGGCAGGAGGACGAGGAGGATAAACCCAGGCAGG</span><span class="exonedge3">TGGAGGTGCATCAAGAGCC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">CGGGGCAGCGGTGCCCAGAGGCCAGGAGGCCCCTGAAGGCAAGGCCAGGGAGACGGTGGA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GAATCTGCCTCCCCTGCCTTTGGACCCTGTCCTCAGAGCTCCTGGGGGCCGCCCTGCTCC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">ATCCCAGGACCTTAACCAGCGCTCCCTGGAGCACTCTGAGGGGCCTGTGGGCAGAGACCC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">TGCTGGCCCTCCTGACGGCGGCCCTGACACAGAGCCTCGGGCAGCCCAGGCCAAGCTGAG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">AGATGGCCAGAAGGATGCCGCCCCCAGGGCAGCTGGCACTGTGAAGGAGCTCCCCAAGGG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">CCCGGAGCAGGTGCCCGTGCCAGACCCCGCCAGGGAAGCCGGGGGCCCAGAGGAGCGCCT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">CGCAGAGGAATTCCCTGGGCAAAGTCAGGACGTTACTGGCGGTTCCCAAGACAGGAAAAA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">ACCTGGGAAGGAGGTGGCAGCCACTGGCACCAGCATTCTGAAGGAAGCCAACTGGCTCGT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GGCAGGGCCAGGAGCAGAGACGGGGGACCCTCGCATGAAGCCCAAGCAAGTGAGCCGAGA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">CCTGGGCCTTGCAGCGGACCTGCCCGGTGGGGCGGAAGGAGCAGCTGCACAGCCCCAGGC</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">TGTGTTACGCCAGCCGGAACTGCGGGTCATCTCTGATGGCGAGCAGGGTGGACAGCAGGG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">CCACCGGCTGGACCATGGCGGTCACCTGGAGATGAGAAAGGCCCGCGGGGGGGACCATGT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GCCTGTGTCCCACGAGCAGCCGAGAGGCGGGGAGGACGCTGCTGTCCAGGAGCCCAGGCA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GAGGCCAGAGCCAGAGCTGGGGCTCAAACGAGCTGTCCCGGGGGGCCAGAGGCCGGACAA</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">TGCCAAGCCCAACCGGGACCTGAAACTGCAGGCTGGCTCCGACCTCCGGAGGCGACGGCG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GGACCTTGGCCCTCATGCAGAGGGTCAGCTGGCCCCGAGGGATGGGGTCATCATTGGCCT</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">TAACCCCCTGCCTGATGTCCAGGTGAACGACCTCCGTGGCGCCCTGGATGCCCAGCTCCG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">CCAGGCTGCGGGGGGAGCTCTGCAGGTGGTCCACAGCCGGCAGCTTAGACAGGCGCCTGG</span></span></span></span></b>&nbsp;&nbsp;&nbsp;<BR>
<b></span><span class="exonedge3">GCCTCCAGAGGAGTCCTAG</span></b><span class="exonedge3">CACCTGCTGGCCATGAGGGCCACGCCAGCCACTGCCCTCCT</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge3">CGGCCAGCAGCAGGTCTGTCTCAGCCGCATCCCAGCCAAACTCTGGAGGTCACACTCGCC</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge3">TCTCCCCAGGGTTTCATGTCTGAGGCCCTCACCAAGTGTGAGTGACAGTATAAAAGATTC</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge3">ACTGTGGCATCGTTTCCAGAATGTTCTTGCTGTCGTTCTGTTGCAGCTCTTAGTCTGAGG</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge3">TCCTCTGACCTCTAGACTCTGAGCTCACTCCAGCCTGTGAGGAGAAACGGCCTCCGCTGC</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge3">GAGCTGGCTGGTGCACTCCCAGGCTCAGGCTGGGGAGCTGCTGCGTCTGTGGTCAGGCCT</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge3">CCTGCTCCTGCCAGGGAGCACGCGTGGTCTTCGGGTTGAGCTCGGCCGTGCGTGGAGGTG</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge3">CGCATGGCTGCTCATGGTCCCAACACAGGCTACTGTGAGAGCCAGCATCCAACCCCACGC</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge3">TTGCAGTGACTCAGAATGATAATTATTATGACTGTTTATCGATGCTTCCCACAGTGTGGT</span></span></span></span>&nbsp;&nbsp;&nbsp;<BR>
</span><span class="exonedge3">AGAAAGTCTTGAATAAACACTTTTGCCTTCA</span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<BR>
</b><br><br>Protein Sequence<BR> <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><HTML><HEAD><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link rel=stylesheet type="text/css" href="../../../../../code/global/genome.css"></HEAD><BODY background="#FF9966"><tt><font  color="#000000"><span class="exonedge2">MTAAAASNWGLITNIVNSIVGVSVLTMPFCFKQ</span><span class="exonedge3">CGIVLGALLLVFC</span></span></span><span class="exonedge4">S</span></span></span><span class="exonedge2">FHAYGKAGKMLVE</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge2">T</span></span></span><span class="exonedge4">S</span></span></span><span class="exonedge3">MIGLMLGTCIAFYVVIGDLGSNFFARLFGFQ</span><span class="exonedge2">VGGTFRMFLLFAVSLCIVLPLSLQRNM</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge2">MASIQSFSAMALLFYTVFMFV</span><span class="exonedge3">IVLSSLKHGLFSGQWLRRVSYVRWEGVFRCIPIFGMSFA</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge3">CQ</span></span></span><span class="exonedge4">S</span></span></span><span class="exonedge2">QVLPTYDSLDEPSVKTMSSIFASSLNVVTTFYVM</span><span class="exonedge3">VGFFGYVSFTEATAGNVLMHFPS</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge3">NLVTEMLRVGFMMSVAVGFPMMILPCRQALSTLLCEQQ</span><span class="exonedge2">QKDGTFAAGGYMPPLRFKALTL</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge2">SVVFGTMVGGILIPN</span></span></span><span class="exonedge4">V</span></span></span><span class="exonedge3">ETILGLTGATMGSLICFICPALIYKKIHKNALSSQ</span><span class="exonedge2">VVLWVGLGV</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge2">LVVSTVTTLSVSEEVPEDLAEEAPGGRLGEAEGLMKVEAARLS</span></span></span><span class="exonedge4">A</span></span></span><span class="exonedge3">QDPVVAVAEDGREKPK</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge3">LPKEREELEQAQIKGPVDVPGREDGKEAPEEAQLDRPGQ</span></span></span><span class="exonedge4">G</span></span></span><span class="exonedge2">IAVPVGEAHRHEPPVPHDKV</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge2">VVDEGQDREVPEENKPPSRHAGGKAPGVQGQMAPPLPDSEREKQEPEQGEVGKRPGQAQA</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge2">LEEAGDLPEDPQKVPEADGQPAVQPAKEDLGPGDRGLHPRPQAVLSEQQNGLAVGGGEKA</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge2">KGGPPPGNAAGDTGQPAEDSDH</span></span></span><span class="exonedge4">G</span></span></span><span class="exonedge3">GKPPLPAEKPAPGPGLPPEPREQRDVERAGGNQAASQ</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge3">LE</span></span></span><span class="exonedge4">E</span></span></span><span class="exonedge2">AGRAEMLDHAVLLQVIKEQQVQQKRLLDQQEKLLAVIEEQHKEIHQQRQEDEEDKPR</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge2">Q</span></span></span><span class="exonedge4">V</span></span></span><span class="exonedge3">EVHQEPGAAVPRGQEAPEGKARETVENLPPLPLDPVLRAPGGRPAPSQDLNQRSLEHS</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge3">EGPVGRDPAGPPDGGPDTEPRAAQ<font color="blue">A</font>KLRDGQKDAAPRAAGTVKELPKGPEQVPVPDPARE</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;(A,G) <BR><span class="exonedge3">AGGPEERLAEEFPGQSQDVTGGSQDRKKPGKEVAATGTSILKEANWLVAGPGAETGDPRM</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge3">KPKQVSRDLGLAADLPGGAEGAAAQPQAVLRQPELRVISDGEQGGQQGHRLDHGGHLEMR</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge3">KARGGDHVPVSHEQPRGGEDAAVQEPRQRPEPELGLKRAVPGGQRPDNAKPNRDLKLQAG</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge3">SDLRRRRRDLGPHAEGQLAPRDGVIIGLNPLPDVQVNDLRGALDAQLRQAAGGALQVVHS</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;<BR><span class="exonedge3">RQLRQAPGPPEES<BR><BR></font></font>
<table width="100%" cellspacing="0" cellpadding="2" border="0">

<tr>
    <th><a href="begin.php"><font size="-1" color="FFFFFF">Change Build</font></a></th>
    <th><font size="-1">&copy;2001, 2002 by Genomix Corporation. All Rights Reserved.</font></th>
	<th><a href="logout.php"><font size="-1" color="FFFFFF">Logout</a></font></th>
</tr>
<tr><td colspan="3"><img src = "../images/760_trans_spacer.gif">
</td></tr>
</table>



