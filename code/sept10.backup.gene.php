<?session_start();
//session_register("build");
if (!isset($chr_id))
  $chr_id=17;
$page_title="GenePOOL Select Gene for build ";
$page_title.=$build;
$page_title.=" Contig ";
$page_title.=$seq_id;
require("nav_begin.php");

if (!isset($build))
  {
    echo 'Usage: gene.php?chr_id=1&build=testgenomix&seq_id=NT_004488.7';
	exit;
  }
  

mysql_connect($db_host,$db_username,$db_password);
mysql_select_db($build);

$sql = "select gene_id,gene_number,variant_number,product,seq_id from chr_".$chr_id."_summary where gene_id = '$gene_id'";
$result=mysql_query($sql);
$row = mysql_fetch_row($result) or must_die(mysql_error());

echo '<font face="arial">';
echo '<font size="+1">Gene number '.$row[1].' variant '.$row[2].' - '.$row[0].'</font><BR><b>'.$row[3].'</b><BR>';
$variant=$row[2];
$prot_length=$row[4];
$gene_number=$row[1];
$sql = "select type, gene_begin, gene_end, strand, seq_id, protein_length from chr_".$chr_id."_summary where gene_id = '$gene_id' ";

//echo $sql;

$result=mysql_query($sql) or must_die(mysql_error());
$row=mysql_fetch_row($result);
?>
<table width="100%" cellspacing="0" cellpadding="0">
<TR><td width="45%" valign="top">
<TABLE cellspacing="0" cellpadding="0" width="90%">
<tr>
<TH colspan=2>Gene Information</TH>
</tr>
<tr>
<TH colspan=2><?echo $gene_id?></TH>
  </tr>
  
    <TD class="second">Location</TD>
    <TD class="second"><?echo $row[1]."..".$row[2]?></TD></TR>
  <TR>
    <TD>Strand</TD>
    <TD><?if ($row[3] == "r") echo "Reverse"; elseif ($row[3] == "f") echo "Forward"; else echo "Unknown"; ?></TD></TR>
  <TR>
    <TD class="second">Contig</TD>
    <?$seq_id = $row[4];?>
    <TD class="second">
    <? $seq_string = '<a href = "contig.php?build='.$build.'&chr_id='.$chr_id.'&seq_id='.$row[4].'">'.$row[4].'</A>';?>
<?echo $seq_string;?></TD></TR>
  <TR>
    <TD>Var</TD>
    <TD><?echo $variant?></TD></TR>
  <TR>
    <TD class="second">Protein Length</TD>
    <TD class="second"><?echo $row[5]?></TD></TR>
  <TR>
    <TD>Blast</TD>
    <TD><?echo build_blast_link($build,$chr_id,$row[4],$gene_id,"View")?></TD></TR>
  <TR>
    <TD class="second">Exp</TD>
    <TD class="second"><?echo build_exp_link($build,$chr_id,$row[4],$gene_id); ?></TD></TR>
  </td>

  <TR>
    <TD>Protein Variants</TD>
    <TD><?echo show_prot_variants($build,$chr_id,$gene_id);?></td></tr>
    </TD>
    <TR><TD class="second">mRNA Variants</TD><td class="second"><?echo show_mrna_variants($build,$chr_id,$gene_id);?></td>
    </TD></tr>
</td>
<tr>
    <td>Genomic Sequence</td><td><?echo show_genomic_sequence($build,$chr_id,$seq_id,$gene_id); ?></td>
</td></tr>

<tr>
<td class="second">
Formatted Proteins</TD>
<td class="second"><?echo build_prot_pat_link($build,$chr_id,$seq_id,$gene_id,"");?></td></tr>
<TR>
<TD>Formatted mRNA</td>
<td><?echo build_mrna_pat_link($build,$chr_id,$seq_id,$gene_id,"");?></td></tr>
<TR><TD class="second">Isoform mRNA Alignments</td>
<td class="second"><?echo '<a href="show_mrna_isoform.php?build='.$build.'&chr_id='.$chr_id.'&seq_id='.$seq_id.'&gene_number='.$gene_number.'" target="_new">View</A>';?></td></tr>
<TR>
<TD>Est Alignments</td>
<td><?echo '<a href="../src/checkboxes/checkboxes.php?build='.$build.'&chr_id='.$chr_id.'&seq_id='.$seq_id.'&gene_id='.$gene_id.'&gene_num='.$gene_number.'&var_num='.$variant.'" target="_new">View</a>';?></td></tr>



</TABLE>
</Td><TD width = "1%">&nbsp;&nbsp;</td>
<td width="45%" align="center" valign="top">
<?

$seq_id = $row[4];

//now display the exon table for this gene only
echo '<table border="0" cellspacing="0" cellpadding="0" width ="100%" >';
echo '<tr><th colspan="4" align="center">Master Exon Table</th></tr>';
echo '<tr><th>Id</th><th>Size</th><Th>Active in Variant</th><th>Contig Location</th></tr>';
//Add here
$filename = "../gpdata/builds/".$build."/symlinks/".$chr_id."/".$gene_id.".exons";

if (!$fp = fopen($filename,"r"))
  {
    echo $filename." map file not found\n";
    exit;
  }
$count =0;
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
    $line = chomp($line);
    $tmparray = split(" ",$line);
    if ($line != "")
      {
	$count++;
	if (($count % 2) != 0)
	  {
	    echo '<tr><td align="right" class="second">'.$tmparray[0].'</td>';
	    if ($tmparray[1] <= $tmparray[2])
	      echo '<td align="right" class="second">'.($tmparray[2]-$tmparray[1]).'</td>';
	    else
	      echo '<td align="right" class="second">'.($tmparray[1]-$tmparray[2]).'</td>'; 
	    
	    echo '<td class="second" align="center">'.$tmparray[3].'</td>';
	    echo '<td align="center" class="second">'.$tmparray[1].'..'.$tmparray[2].'</td></tr>';
	  }
	else
	  {
	    echo '<tr><td align="right">'.$tmparray[0].'</td>';
	    if ($tmparray[1] <= $tmparray[2])
	      echo '<td align="right">'.($tmparray[2]-$tmparray[1]).'</td>';
	    else
	      echo '<td align="right">'.($tmparray[1]-$tmparray[2]).'</td>'; 
	    
	    echo '<td align="center">'.$tmparray[3].'</td>';
	    echo '<td align="center">'.$tmparray[1].'..'.$tmparray[2].'</td></tr>';
	  }
	
      }
  }
fclose($fp);















//stop here
echo '</table>';
echo '</td></tr><tr><TD colspan="3"><img src="../images/760_trans_spacer.gif"></td></tr></table>';
echo '<br><br>';
echo "</font></font>";

//Ok Here is where we need to add the map file 

echo '<map name="var_'.$gene_id.'">';
$var_map = "../gpdata/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/images/var_".$gene_id.".map";
if (!$fp = fopen($var_map,"r"))
  {
    echo "map file not found\n";
    //exit;
  }
else
{	
    while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }

fclose($fp);
}
echo "</map>";

echo '<map name="evid_'.$gene_id.'">';
$evid_map = "../gpdata/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/images/evid_".$gene_id.".map";
if (!$fp = fopen($evid_map,"r"))
  {
    echo "map file not found\n";
    //exit;
  }
else
{	
    while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }

fclose($fp);
}
echo "</map>";





//Insert Variant and evidence pictures here
echo '<img border ="0" src="../gpdata/builds/'.$build.'/symlinks/'.$chr_id.'/images/'.$seq_id.'/images/var_'.$gene_id.'.png"  usemap="#var_'.$gene_id.'" >';
echo '<img border="0" src="../gpdata/builds/'.$build.'/symlinks/'.$chr_id.'/images/'.$seq_id.'/images/evid_'.$gene_id.'.png"  usemap="#evid_'.$gene_id.'">';
echo '<br><br>';


echo 'mRNA Sequence<BR>';
$mrna_file = "../gpdata/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/mrna/".$gene_id.".mrna.html";
if (!$fp=fopen($mrna_file,"r"))
  {
    echo "$mrna_file file not found\n";
    exit;
  }
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }
fclose($fp);

echo '</b><br><br>Protein Sequence<BR>';
$prot_file = "../gpdata/builds/".$build."/symlinks/".$chr_id."/images/".$seq_id."/prot/".$gene_id.".prot.html";
if (!$fp=fopen($prot_file,"r"))
  {
    echo "$prot_file file not found\n";
    exit;
  }
while(!feof($fp))
  {
    $line = fgets($fp, 4096);
	echo "$line";
  }
fclose($fp);

echo "<BR><BR>";


require("nav_end.php");

?>
