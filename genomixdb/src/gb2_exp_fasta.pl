#!/usr/bin/perl

#check for environment variable
#$genomixdb=$ENV{GENOMIXDB};
#if(!$genomixdb) {die "GENOMIXDB environment variable not set\n"; }

if ($#ARGV != 1)
{
print "usage: gb2fsa dbname inputfile > outputfile\n";
exit 0;
}

if (open(INFILE,$ARGV[1]))
{}
else
{
print "Couldn't open $ARGV[1]\n";
exit 0;
}

$dbname = $ARGV[0];
# print "database name: $dbname\n";

@seqtoprint="";
$seqarray ="";

while ($line=<INFILE>)
{

if ($line =~ /^LOCUS/)
{
# do nothing for now
}


#############################################################

if ($line =~ /^  ORGANISM/)
{
#print $line;
@orgarray = split("ORGANISM\s+",$line);
$organism = $orgarray[1];

$organism = $line;
$organism =~ s/^  ORGANISM  //;


$organism =~ s/\n//;

$organism =~ s/\n//;
$organism =~ s/^  //;
$organism =~ s/[.,\-\(\)\/\\]//;

$orgsize= @orgarray;
# for ($zz=0; $zz<=$orgsize; $zz++)
# {print "$zz:$orgarray[$zz]\n"; }
# print "ORGANISM:$organism\n";
}

if ($organism eq "Homo sapiens") {$orgname = "human" ;}
elsif ($organism eq "Mus musculus") {$orgname = "mouse" ;}
elsif ($organism eq "Rattus norvegicus") {$orgname = "rat" ;}
else {$orgname = $organism ;}

#############################################################

if ($line =~/^DEFINITION/)
{
# print "there\n";
chomp($line);

$definition = $line;
while ($line=<INFILE>)
{
# print $line;
if ($line=~/^ACCESSION/)
{
last;
}
else
{
chomp($line);
$line=~ s/^ //;
$line=~ s/ //;
$line =~ s/     //g;
$line =~ s/[-.,'\\\/]//g;
$line =~ s/\t//g;
$definition .= $line;
$definition =~ s/\'//g;
}
}
}

#############################################################

if ($line =~ /^VERSION/)
{
#print "$version\n";
$version = $line;
}

#############################################################

if ($line =~ /\/tissue_type=/)
{
$tissue = $line;
chomp($tissue);
# chop($tissue);
$tissue = substr($tissue, 35, -1);
# print "$tissue\n";
}

if($tissue eq "")
{
$tissue = "no_tissue_information";
}


#############################################################

if ($line =~ /^ORIGIN/)
{
process_output();
#read first line of sequence
while ($line = <INFILE>)
{
#check for end of sequence entry (//)
if ($line =~ /\/\//)
{ #print "Found end of seq\n";

last; }
else
{ #print $line;
@tmparray = split(" ",$line);
for ($a=1; $a <= 6; $a++)
{
$seqarray .= $tmparray[$a];
#print " $a : $tmparray[$a]\n";
} #end for
#push seq unto @seqtoprint
$big=uc($seqarray); #turn text to upper case
print "$big\n";
$seqarray="";
} #end else
} #end while
} #end if
#print "$seqarray\n";
# exit 0;
} #end while

sub process_output()
{
$definition =~ s/DEFINITION  //;
#print "$definition\n";
@versionarray = split(" ",$version);
#print "$versionarray[1]\n";
$orgname =~ s/ /_/g;
print ">$orgname|$versionarray[1]_gb|$dbname|$tissue|$definition\n";
}







