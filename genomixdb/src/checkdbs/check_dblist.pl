#!/usr/bin/perl

#this program checks the dblist for errors
$error_count = 0;
$file_count = 0;

open(INFILE,"<dblist");
while ($line = <INFILE>)
  {
    $file_count += 5;
    chomp($line);
    if (-e $line)
      {
         print "$line correct\n";
      }
    else
      {
        print "$line WRONG\n";
        $error_count++;
      }
    #check for GXP Files now
    $line1 = $line.".gxp";
    if (-e $line1)
      {
         print "$line1 correct\n";
      }
    else
      {
        print "$line1 WRONG\n";
        $error_count++;
      }
    #check for nhr files
    $line1 = $line.".nhr";
    if (-e $line1)
      {
         print "$line1 correct\n";
      }
    else
      {
        print "$line1 WRONG\n";
        $error_count++;
      }
    #check for nin files
    $line1 = $line.".nin";
    if (-e $line1)
      {
         print "$line1 correct\n";
      }
    else
      {
        print "$line1 WRONG\n";
        $error_count++;
      }
    #check for nsq
    $line1 = $line.".nsq";
    if (-e $line1)
      {
         print "$line1 correct\n";
      }
    else
      {
        print "$line1 WRONG\n";
        $error_count++;
      }






  }

print "there were $error_count errors out of $file_count files scans\n";