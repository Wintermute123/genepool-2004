#!/usr/bin/perl

#this program checks the prodblist for errors
$error_count = 0;
$file_count = 0;

open(INFILE,"<prodblist");
while ($line = <INFILE>)
  {
    $file_count += 5;
    chomp($line);
    if (-e $line)
      {
         print "$line correct\n";
      }
    else
      {
        print "$line WRONG\n";
        $error_count++;
      }
    #check for GXP Files now
    $line1 = $line.".gxp";
    if (-e $line1)
      {
         print "$line1 correct\n";
      }
    else
      {
        print "$line1 WRONG\n";
        $error_count++;
      }
    #check for phr files
    $line1 = $line.".phr";
    if (-e $line1)
      {
         print "$line1 correct\n";
      }
    else
      {
        print "$line1 WRONG\n";
        $error_count++;
      }
    #check for pin files
    $line1 = $line.".pin";
    if (-e $line1)
      {
         print "$line1 correct\n";
      }
    else
      {
        print "$line1 WRONG\n";
        $error_count++;
      }
    #check for psq
    $line1 = $line.".psq";
    if (-e $line1)
      {
         print "$line1 correct\n";
      }
    else
      {
        print "$line1 WRONG\n";
        $error_count++;
      }






  }

print "there were $error_count errors out of $file_count files scans\n";