#!/bin/perl
# =====================================================
# Script to read and parse a gbk flat file. The fields 
#  pertaining to protein product information are output
#  to build genpept format files (info, gbk, fasta,
#  seqindex, seqinfo and gbkseqinfo.
# Bob Stephens ABCC 02/02.
# =====================================================

# command line checking...
if ($ARGV[2] eq "") {print "USAGE: GBGenpept.pl <filename> <fastadir> <mysqldir>\n"; exit 1;}

# open input/output files...
if (! open(IN,"$ARGV[0]")) {print "File $ARGV[0] Not Found.\n"; exit 1;}
$file=$ARGV[0];
$a=rindex($file,".");
if ($a>-1) {$file=substr($file,0,$a);}
$a=rindex($file,'/');
if ($a>-1) {$file=substr($file,$a+1);}
if (! open(SEQINFO,"> $ARGV[2]/$file.seqinfo")) {
    print "File $file.seqinfo Could NOT be Opened.\n";
    exit 1;
}
if (! open(GBKSEQINFO,"> $ARGV[2]/$file.gbkseqinfo")) {
    print "File $file.gbkseqinfo Could NOT be Opened.\n";
    exit 1;
}
if (! open(FASTA,"> $ARGV[1]/$file.tfa")) {
    print "File $file.tfa Could NOT be Opened.\n";
    exit 1;
}
if (! open(GBK,"> $ARGV[1]/$file.gbk")) {
    print "File $file.gbk Could NOT be Opened.\n";
    exit 1;
}
if (! open(INFO,"> $ARGV[2]/$file.info")) {
    print "File $file.info Could NOT be Opened.\n";
    exit 1;
}
if (! open(SEQINDEX,"> $ARGV[2]/$file.seqindex")) {
    print "File $file.seqindex Could NOT be Opened.\n";
    exit 1;
}
$count1=0;
$count2=0;
print "GBGenpept for $ARGV[0].\n";

while ($line=<IN>) {
    if ($line=~/^LOCUS/) {
	&clear_fields_all();
	$count3=0;
        $count1++;
  	$temp=substr($line,12,16);
	$temp=~/(\S+)/;
	$locus=$1;
	$temp=substr($line,64,3);
	$temp=~/(\S+)/;
	$div=$1;
	$fulldate=substr($line,68,11);
	$fulldate=~/(\d+)-(\S+)-(\d+)/;
	$date1=$1;
	$date2=$2;
	$date3=$3;
	&mydate();
	next;
    }
if ($line=~/ CDS /) {
        $count3++;
        $comment="COMMENT     CDS ";
        $line=~/CDS\s+(\S+)/;
        $comment.="$1\n";
	$cds=$1;
        if ($line=~/\(/ && $line!~/\)/) {
          while () {
            $line=<IN>;
            $comment.=substr($line,9);
            if($line=~/\)$/) {last;}
          }
        }
        $geneflag=0;  # read note lines as cdsnote
	next;
    }
    if ($line=~/^ACCESSION\s+(\S+)/) {
	$acc=$1;
	next;
    }
    if ($line=~/^VERSION\s+(\S+)\.(\d+)\s+GI:(\d+)/) {
	$seqgi=$3;
	next;
    }    
    if ($line=~/DEFINITION\s+([\S ]+)/) {
        $rawdef="";
	while() {
            $rawdef.=$line;
	    $definition.=$1;
	    if ($line=~/\.$/) {last;}
	    $line=<IN>;
	    $line=~/^\s+([\S ]+)/;
	}
	next;
    }
    if ($line=~/ORGANISM\s+([\S ]+)/) {
	$organism=$1;
	while() {
            $raworg.=$line;
	    if ($line=~/\.$/) {last;}
	    $line=<IN>;
	    $line=~/^\s+([\S ]+)/;
	}
	next;
    }
    if ($line=~/codon_start=(\d+)/) {
       $phase=$1;
       next;
    }
    if ($line=~/^     gene    /) {
       $geneflag=1;  # read note lines as genenotes...
       next;
    }

    if ($line=~/KEYWORDS\s+([\S ]+)\./) {
        $keyword=$1;
	next;
    }

    if ($line=~/db_xref=/) {
        if ($line=~/db_xref="taxon:(\d+)"/) {
          $taxon="$1";
        }
        elsif ($line=~/db_xref="GI:(\d+)"/) {
          $comment.=substr($line,9);
          $gi="$1";
        }
        elsif($line=~/db_xref="CDD:pfam(\d+)"/) {
          $pfam.="$1,";
        }
        else {
          $comment.=substr($line,9);
        }
        next;
    }

    if ($line=~/gene=([\s\S]+)/) {
        $test=$1;
        chomp($test);
        $test=~tr/\[\]\{\}\/\+\(\)/ /;
        $gene=$test;
           if ($comment!~m/$test/) {$comment.=substr($line,9);}
        next;
    }
    if ($line=~/protein_id=([\s\S]+)/) {
        $comment.=substr($line,9);
        next;
    }
    if ($line=~/function=([\s\S]+)/) {
        $comment.=substr($line,9);
        next;
    }
    if ($line=~/transl_table=(\d+)/) {
        $comment.=substr($line,9);
        next;
    }
    if ($line=~/product=\"([\S\s]+)\"/) {
        $comment.=substr($line,9);
        $product="$1";
	next;
    }
    if ($line=~/translation=\"/) {
	$seq=$line;
	while($line!~/\"$/){
	    $line =<IN>;
	    $seq.=$line;
	}
	$seq=~/\"([\S\s]+)\"/;
        $s=$1;
        $s=~s/\s//g;
        $seq=$s;
	$len=length($seq);
	($pi,$weight)=split(/\t/,&getpIMw());
        &printOut();
    }
    if ($line=~/note=\"/) {
        if ($geneflag == 2) {next;}
        elsif($geneflag == 1) {
          $cdsnote=substr($line,9);
	  while($line!~/\"$/){
	    $line =<IN>;
	    $cdsnote.=substr($line,9);
	  }
        }
        elsif($geneflag == 0) {
          $genenote=substr($line,9);
	  while($line!~/\"$/){
	    $line =<IN>;
	    $genenote.=substr($line,9);
	  }
        }
    }
}
if ($seq) {
    &printOut();
}

close(IN);
close(INFO);
close(SEQINFO);
close(GBKSEQINFO);
close(FASTA);
close(GBK);

print "Parsed $count1 locus records.\n";
print "Total of $count2 translations found\n";
exit 0;

sub clear_fields_all() {
    $div="";
    $date="";
    $definition="";
    $seqgi=0;
    $keyword="";
    $organism="";
    $taxon=0;
    $product="";
    $gi=0;
    $len=0;
    $pi=0;
    $weight=0;
    $seq="";
    $rawdef="";
    $raworg="";
    $comment="";
    $phase="";
    $pfam="";
    $gene="";
    return;
}
sub clear_fields() {
    $product="";
    $gi=0;
    $len=0;
    $pi=0;
    $weight=0;
    $seq="";
    $comment="";
    $phase="";
    $cdsnote="";
    $genenote="";
    $table="";
    $geneflag=2;
    $function="";
    $pfam="";
    $gene="";
    return;
}


sub mydate() {
    if ($date2 eq "JAN") {$date2="01"};
    if ($date2 eq "FEB") {$date2="02"};
    if ($date2 eq "MAR") {$date2="03"};
    if ($date2 eq "APR") {$date2="04"};
    if ($date2 eq "MAY") {$date2="05"};
    if ($date2 eq "JUN") {$date2="06"};
    if ($date2 eq "JUL") {$date2="07"};
    if ($date2 eq "AUG") {$date2="08"};
    if ($date2 eq "SEP") {$date2="09"};
    if ($date2 eq "OCT") {$date2="10"};
    if ($date2 eq "NOV") {$date2="11"};
    if ($date2 eq "DEC") {$date2="12"};
    $date=$date3."-".$date2."-".$date1;
    return;
}

sub printOut() {
   if($pfam) {chop($pfam);}
   $comment.=$genenote;
   $comment.=$cdsnote;
    print INFO "$gi\t$acc\_$count3\t$locus\t$div\t$seqgi\t$date\t$taxon\t$len\t$pi\t$weight\t";
    print INFO "$organism\t$product\t$keyword\t$definition\t$gene\t$pfam\n";

    $offset1=tell(FASTA);
    print SEQINFO "$gi\t$file\t$offset1\n";
    print FASTA ">gi|$gi $acc\_$count3 \!$product\n";
    $a=length($seq)/60;
    for($i=0;$i<$a;$i++){
	print FASTA substr($seq,$i*60,60);
	print FASTA "\n";
    }
    $offset2=tell(GBK);
    print GBKSEQINFO "$gi\t$file\t$offset2\n";  
    print SEQINDEX "$gi\t$acc\_$count3\t$locus\tGPT\n";
    print GBK "LOCUS       $locus\_$count3 [$acc]\n";
    print GBK "$rawdef";
    print GBK "DATE        $fulldate\n";
    print GBK "ACCESSION   $acc\n";
    #print GBK "NID\n";
    print GBK "ORGANISM    ",substr($raworg,12);
    print GBK "$comment";
    print GBK "WEIGHT      $weight\n";
    print GBK "PI          $pi\n";
    print GBK "LENGTH      $len\n";
    print GBK "ORIGIN      Translated using phase $phase\n"; 
    &printGBKSeq();
    print GBK "//\n";
    $count2++;
    &clear_fields();
    return;
}

# -------------------------------------------------------------
# Calculate the PI and molecular weight for a peptide
#
#     ****************************************
#     *           Gary W. Smythers           *
#     * Advanced Biomedical Computing Center *
#     *                SAIC                  *
#     ****************************************
sub getpIMw() {

my %Wgt =
 (
  'A', 71.075,    'B', 114.595,   'C', 103.135,    'D', 115.085,
  'E', 129.115,   'F', 147.175,   'G', 57.055,     'H', 137.145,
  'I', 113.155,   'K', 128.175,   'L', 113.155,    'M', 131.195,
  'N', 114.105,   'P', 97.115,    'Q', 128.135,    'R', 156.185,
  'S', 87.075,    'T', 101.105,   'V', 99.135,     'W', 186.215,
  'X', 111.295,   'Y', 163.175,   'Z', 128.625
 ); 

my %Comp =
 (
  'A', 0,  'B', 0,  'C', 0,  'D', 0,  'E', 0,  'F', 0,  'G', 0,  'H', 0,
  'I', 0,  'K', 0,  'L', 0,  'M', 0,  'N', 0,  'P', 0,  'Q', 0,  'R', 0,
  'S', 0,  'T', 0,  'V', 0,  'W', 0,  'X', 0,  'Y', 0,  'Z', 0
 ); 

my %CT =
 (
  'A', 3.55,  'B', 3.55,  'C', 3.55,  'D', 4.55,  'E', 4.75,  'F', 3.55,
  'G', 3.55,  'H', 3.55,  'I', 3.55,  'K', 3.44,  'L', 3.55,  'M', 3.55,
  'N', 3.55,  'P', 3.55,  'Q', 3.55,  'R', 3.55,  'S', 3.55,  'T', 3.55,
  'V', 3.55,  'W', 3.55,  'X', 3.55,  'Y', 3.55,  'Z', 3.55
 ); 

my %NT =
 (
  'A', 7.59,  'B', 7.50,  'C', 7.50,  'D', 7.50,  'E', 7.70,  'F', 7.50,
  'G', 7.50,  'H', 7.50,  'I', 7.50,  'K', 7.50,  'L', 7.50,  'M', 7.00,
  'N', 7.50,  'P', 8.36,  'Q', 7.50,  'R', 7.50,  'S', 6.93,  'T', 6.82,
  'V', 7.44,  'W', 7.50,  'X', 7.50,  'Y', 7.50,  'Z', 7.50
 ); 

my %RT =   
 (
  'C', 9.00,  'D', 4.05,  'E', 4.45,  'H', 5.98,  'K', 10.0,  'R', 12.0,
  'Y', 10.0
 );

my $N=0;          # Length of sequence
my $aa="";        # Each amino acid in sequence
my $seqwgt=0.0;   # Sequence molecular weight
my $H2O=18.015;   # Weight to add to residues weight for N and C terminal

my $seqNT="";     # N-terminal amino acid of sequence
my $seqCT="";     # C-terminal amino acid of sequence

my $pHmin=0;
my $pHmax=14;
my $pHmid;
my $MaxLoop=2000;
my $per=0.0001;   # Precision


my $charge;
#            Contribution from:
my $cter;    # C-terminal
my $nter;    # N-terminal
my $carg;    # Arginine
my $casp;    # Aspartic acid
my $ccys;    # Cysteine
my $cglu;    # Glutamic acid
my $chis;    # Histidine
my $clys;    # Lysine
my $ctyr;    # Tyrosine

my $i;            # Loop counter

$N=length($seq);
for ($i=0;$i<$N;$i++)
{
 $aa=substr($seq,$i,1);
 $seqwgt=$seqwgt + $Wgt{$aa};
 $Comp{$aa}++;
}

$seqwgt= $seqwgt + $H2O;
$seqNT=substr($seq,0,1);
$seqCT=substr($seq,$N-1,1);

for ($i=0, $charge=1.0; ($i < $MaxLoop) and (($pHmax - $pHmin) > $per); $i++)
{
  $pHmid = $pHmin + ($pHmax - $pHmin) /2;
  $cter = 10**(-$CT{$seqCT}) / (10**(-$CT{$seqCT}) + 10**(-$pHmid));
  $nter = 10**(-$pHmid) / (10**(-$NT{$seqNT}) + 10**(-$pHmid));
  $carg = $Comp{'R'} * 10**(-$pHmid) / (10**(-$RT{'R'}) + 10**(-$pHmid));
  $chis = $Comp{'H'} * 10**(-$pHmid) / (10**(-$RT{'H'}) + 10**(-$pHmid));
  $clys = $Comp{'K'} * 10**(-$pHmid) / (10**(-$RT{'K'}) + 10**(-$pHmid));
  $casp = $Comp{'D'} * 10**(-$RT{'D'}) / (10**(-$RT{'D'}) + 10**(-$pHmid));
  $cglu = $Comp{'E'} * 10**(-$RT{'E'}) / (10**(-$RT{'E'}) + 10**(-$pHmid));
  $ccys = $Comp{'C'} * 10**(-$RT{'C'}) / (10**(-$RT{'C'}) + 10**(-$pHmid));
  $ctyr = $Comp{'Y'} * 10**(-$RT{'Y'}) / (10**(-$RT{'Y'}) + 10**(-$pHmid));
  $charge = $carg + $clys + $chis + $nter - 
                         ($casp + $cglu + $ctyr + $ccys + $cter);
  if ($charge > 0.0)
    { $pHmin = $pHmid; }
  else
    { $pHmax = $pHmid; }
}
return (sprintf("%.2f\t%.2f",$pHmid,$seqwgt));
}

sub printGBKSeq() {
  $lines=length($seq)/60;
  for($i=0;$i<$lines;$i++) {
    $out=sprintf("%9d",$i*60+1);
    for($j=0;$j<6;$j++) {
      $out.=" ";
      $out.=substr($seq,$i*60+$j*10,10);
    }
    print GBK "$out\n";
  }
  return;
}
