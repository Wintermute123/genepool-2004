#!/usr/bin/perl

use POSIX;

#Ok, lets grab the filename from the command line, open it and get ready to process it.

$filename = $ARGV[0];
$tissue_type="";
$organism="";
$description="";
$seq="";


if (!open(INFILE,"<$filename"))
  {
    print "Could not open $filename";
    exit(1);
  }
$first_entry=1;
while ( $line = <INFILE>)
  {
    if ($line =~ /^ENTRY/)
      {process_entry();}

  }

close(INFILE);

sub process_entry()
  {
    $tissue_type="";
    $organism="";
    $description="";
    $seq="";
    $stopme = 0;
    @tmparray = split(" ",$line);
    $pir_id = $tmparray[1];
#    print "pir_id: $pir_id\n";

    do
      {
        $line = <INFILE>;
        if ($line =~ /^ACCESSIONS/)
          {
            @tmparray = split(" ",$line);
            $tmparray[1] =~ s/\;//g;
            $pir_id = $tmparray[1];


          }



        if ($line =~ /^TITLE/)
          {
#            print $line;
            chomp($line);
            @tmparray = split("TITLE           ",$line);
            $title = $tmparray[1];
            $title =~ s/ /_/g;
            $title =~ s/\(//g;
            $title =~ s/\)//g;
            $title =~ s/\,//g;
            $title =~ s/\[//g;
            $title =~ s/\]//g;
#            print "Title: $title\n";
          } #end title line
        if ($line =~ /^ORGANISM/)
          {
#            print $line;
            chomp($line);
            @tmparray = split("#formal_name ",$line);
#            print "tmparray[0]: $tmparray[1]\n";
            @tmp2array = split(" #common_name",$tmparray[1]);
            $organism = $tmp2array[0];
            $organism =~ s/\(//g;
            $organism =~ s/\)//g;
            $organism =~ s/\,//g;
            $organism =~ s/ /_/g;
            $organism =~ s/\[//g;
            $organism =~ s/\]//g;
            $organism =~ s/\.//g;
#            print "Organism: $organism\n";
            if ($organism =~ /^Rattus_norvegicus/)
              { $organism = "rat"; }
            if ($organism =~ /^Mus_musculus/)
              { $organism = "mouse"; }
            if ($organism =~ /^Homo_sapiens/)
              { $organism = "human"; }

          }
        if ($line =~ /^SEQUENCE/)
          {
            #advance filepos oneline to skip numeric placeholders
            $line=<INFILE>;
            do {  if ($line =~ /\/\/\//)
                    {$stopme = 1}
                 if ($stopme != 1)
                   {
                    $line = <INFILE>;
                    chomp($line);
                    $line =~ s/ //g;
                    $line =~ s/[0-9]//g;
                    $line =~ s/\(//g;
                    $line =~ s/\)//g;
                    $line =~ s/\.//g;
                    $seq.=$line;
                   }
               } while ($stopme != 1);


          }

      } while ($line !~ /\/\/\//);
    print ">$organism|$pir_id";
    print "_pir|pir|no_tissue_information|$title\n";
    @seq_array=split("",$seq);
    $seq_size=@seq_array;
    $count=0;
    for ($a=0; $a <= $seq_size; $a++)
      {
        if ($seq_array[$a] ne "/")
          {
            print $seq_array[$a];
            $count++;
          }
        if ($count == 59)
          {  print "\n"; $count=0; }


      } #end for loop
      print "\n";

  }