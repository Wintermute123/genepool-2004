#!/usr/bin/perl
#------------------------------------------
#Script Name: /mary/src/splitgb.pl
#example to call script splitgb.pl -i [input file name] -o [output file name]
#splitgb.pl -i /data/sequences/contigs/ftp.ncbi.nlm.nih.gov/genomes/H_sapiens/CHR_Y/hs_chrY.gbk
# -o /data/GenomixDB/human/chromosomes/CHR_Y
#  
#Date: 11/2/2001
#Programmer: Mary E. Perkins
#Description: This program will extract human contigs from a Fasta file(note: only a fasta
# file because I grab the 4th postion from the Fasta header and create the file name with it.
#--------------------------------------------
$ARGC = @ARGV;
if ($#ARGV < 3)
{
    print "Error/Usage you forgot to put a command line argument. \n";
    print "Example: perl splitgb.pl -i inputfile -o outputfile. \n"; 
    exit 1; 
}

#define variable here from command line.
for ($a=0; $a<=$ARGC; $a++)
  {
    if ($ARGV[$a] =~ /-i/)
      { $input = $ARGV[$a+1]; }
    if ($ARGV[$a] =~ /-o/)
      { $outputfolder =$ARGV[$a+1]; }
  }

                  
$inputname = "< ";
$inputname.= $input;

if (open(INFILE,"$inputname"))     #open input file
{
print "infile opened $inputname\n";
}
else
{
print "Error opening $inputname\n";
}

#@lines = <INFILE>;

#@holdlines = <INFILE>;
@arrayhold=();

while($pass=<INFILE>)
{

    if (substr($pass,0,5) eq "LOCUS")   #find LOCUS this is the beginning of the file
    {

    }

 	

    if (substr($pass,0,7) eq "VERSION")
    {
           @array=split(/\s+/,$pass);     #split the line by | then put into an array so you can get the 4th postion as the filename
           $filename ="> ";
           $filename .= "$outputfolder/";
           $filename.="$array[1]";
           $filename.=".gbk";              #contig name
    }	
	if ($pass =~ /\/\//)
	{
		#close the OUTFILE because '//' was found
   		if (substr($pass,0,2) eq "//")
		{
			if (open(OUTFILE,"$filename"))    #open output file
	      		{ print "Outfile opened $filename\n"; 
        		}
        		else
        		{     print "Error outfile not opened $filename\n";
			}
			push(@arrayhold,$pass); 
        	        print OUTFILE @arrayhold;
			
			@arrayhold=(); 		  
			close(OUTFILE);
		}
		else
		{
			print "We have a problem '//' was found embedded in file\n";
			exit 1;
		}
	}
      if (substr($pass,0,2) ne "//") 
	{
	 push(@arrayhold,$pass); 
	}
 }
