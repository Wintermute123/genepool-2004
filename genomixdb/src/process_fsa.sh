#!/usr/bin/csh

foreach k (/data/swissprot/swissprot_fsa/*.seq.fsa)
  echo $k
  mv $k `basename $k .fsa`.before
  cat `basename $k .fsa`.before | col -b > `basename $k .before`.after
  perl /data/genomixdb/src/convertcase/convertcase.pl `basename $k .fsa`.fsa.after
  mv `basename $k .seq.fsa`.seq.fsa.after.UC $k
  /home/john/EXP6/bin/gxpindex $k
  /home/john/EXP6/blast/formatdb -t swissprot -p T -i $k
  rm -f -r *.before
  rm -f -r *.after
  rm -f -r *.UC
  
end
foreach k (/data/pir/pir_fsa/*.seq.fsa)
  mv $k `basename $k .fsa`.before
  cat `basename $k .fsa`.before | col -b > `basename $k .before`.after
  perl /data/genomixdb/src/convertcase/convertcase.pl `basename $k .fsa`.fsa.after
  mv `basename $k .seq.fsa`.seq.fsa.after.UC $k
  /home/john/EXP6/bin/gxpindex $k
  /home/john/EXP6/blast/formatdb -t pir -p T -i $k
  rm -f -r *.before
  rm -f -r *.after
  rm -f -r *.UC
end
foreach k (/data/swissprot/swissprot_fsa/*.seq.fsa)
  mv $k `basename $k .fsa`.before
  cat `basename $k .fsa`.before | col -b > `basename $k .before`.after
  perl /data/genomixdb/src/convertcase/convertcase.pl `basename $k .fsa`.fsa.after
  mv `basename $k .seq.fsa`.seq.fsa.after.UC $k
  /home/john/EXP6/bin/gxpindex $k
  /home/john/EXP6/blast/formatdb -t swissprot -p T -i $k
  rm -f -r *.before
  rm -f -r *.after
  rm -f -r *.UC
end
foreach k (/data/pat/pat_fsa/pat0*.seq.fsa)
  mv $k `basename $k .fsa`.before
  cat `basename $k .fsa`.before | col -b > `basename $k .before`.after
  perl /data/genomixdb/src/convertcase/convertcase.pl `basename $k .fsa`.fsa.after
  mv `basename $k .seq.fsa`.seq.fsa.after.UC $k
  /home/john/EXP6/bin/gxpindex $k
  /home/john/EXP6/blast/formatdb -t pat -p F -i $k
  rm -f -r *.before
  rm -f -r *.after
  rm -f -r *.UC
end

foreach k (/data/pat/pat_fsa/patent*.seq.fsa)
  mv $k `basename $k .fsa`.before
  cat `basename $k .fsa`.before | col -b > `basename $k .before`.after
  perl /data/genomixdb/src/convertcase/convertcase.pl `basename $k .fsa`.fsa.after
  mv `basename $k .seq.fsa`.seq.fsa.after.UC $k
  /home/john/EXP6/bin/gxpindex $k
  /home/john/EXP6/blast/formatdb -t patent_prt -p T -i $k
  rm -f -r *.before
  rm -f -r *.after
  rm -f -r *.UC
end

foreach k (/data/ncbi/genpept/genpept_fsa/*.seq.fsa)
  mv $k `basename $k .fsa`.before
  cat `basename $k .fsa`.before | col -b > `basename $k .before`.after
  perl /data/genomixdb/src/convertcase/convertcase.pl `basename $k .fsa`.fsa.after
  mv `basename $k .seq.fsa`.seq.fsa.after.UC $k
  /home/john/EXP6/bin/gxpindex $k
  /home/john/EXP6/blast/formatdb -t genpept -p T -i $k
  rm -f -r *.before
  rm -f -r *.after
  rm -f -r *.UC
end

foreach k (/data/ncbi/genbank/genbank_fsa/*.seq.fsa)
  mv $k `basename $k .fsa`.before
  cat `basename $k .fsa`.before | col -b > `basename $k .before`.after
  perl /data/genomixdb/src/convertcase/convertcase.pl `basename $k .fsa`.fsa.after
  mv `basename $k .seq.fsa`.seq.fsa.after.UC $k
  /home/john/EXP6/bin/gxpindex $k
  /home/john/EXP6/blast/formatdb -t genbank -p F -i $k
  rm -f -r *.before
  rm -f -r *.after
  rm -f -r *.UC
end

foreach k (/data/ncbi/refseq/refseq_fsa/*.seq.fsa)
  mv $k `basename $k .fsa`.before
  cat `basename $k .fsa`.before | col -b > `basename $k .before`.after
  perl /data/genomixdb/src/convertcase/convertcase.pl `basename $k .fsa`.fsa.after
  mv `basename $k .seq.fsa`.seq.fsa.after.UC $k
  /home/john/EXP6/bin/gxpindex $k
  /home/john/EXP6/blast/formatdb -t refseq -p F -i $k
  rm -f -r *.before
  rm -f -r *.after
  rm -f -r *.UC
end





