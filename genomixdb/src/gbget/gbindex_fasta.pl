#!/usr/bin/perl

###################################
# gdxINDEX
#
# Script to index a database for
# use with GrailEXP.
# Database must contain the accession
# number in the second field, delimited
# by |, e.g. <blah>|<accession no>|<blah>.....
###################################
$genomixdb=$ENV{GENOMIXDB};
if(!$genomixdb) {die "GENOMIXDB environment variable not set\n"; }
if (open(CFGFILE,"$genomixdb/cfg/genomixdb.cfg"))
  {
    while ($line=<CFGFILE>)
      {
 	if ($line =~ /genbank_fsa/)
           {
             @tmparray = split("=",$line);
             $genbank_fsa = $tmparray[1];
             $genbank_fsa =~ s/\s+//;
	     chomp($genbank_fsa);
             chomp($genbank_fsa);
             print "genbank_fsa: $genbank_fsa*stop\n";
           }
        if ($line =~ /genbank_gdx/)
           {
             @tmparray = split("=",$line);
             $genbank_gdx = $tmparray[1];
             chomp($genbank_gdx);
             chomp($genbank_gdx);
             #print "Genbank_gdx: $genbank_gdx\n";
           }
      }
  }
close(CFGFILE);


$usage = "\nUsage: $0 <database>\n";
$pad = "################";

#$indexdir="/data/sequences/genbank/gdx/indexes/";
if(@ARGV != 1) { die $usage; }
$tmpdb = shift;
chomp($tmpdb);
#print "tmpdb: $tmpdb\n";
$db = "$genbank_fsa";
$db .= "/$tmpdb";
$gdx = "$genbank_gdx/$tmpdb";
$gdx .="_fsa.gdx";
#print "db: $db\ngdx: $gdx\n";

open GDXF, ">$gdx" or die "\n[$0]: Couldn't open $gdx for writing.\n$usage\n\n";

open DBF, $db or die "\n[$0]: Couldn't open database $db for reading!\n$usage";
$ptr = tell DBF;
while($line = <DBF>) {
  if($line =~ /^>/) {
    
    @info = split /\|/, $line;
    #$numinfo=@info;
    #for ($b=0; $b <= $numinfo; $b++)
    #  { print "$b: $info[$b]\n"; }
    $tmpacc = $info[1];
    #print "key: $tmpacc\n";
    $acc = $tmpacc.(substr($pad, 0, 16-length($tmpacc)));
    push @keys_uns, $acc;
    $offst = sprintf("%012s", $ptr);
    $offsets{$acc} = $offst;
  }
  $ptr = tell DBF;
}
close DBF;

@keys = sort { $a cmp $b } @keys_uns;

for($i = 0; $i < @keys; $i++) { print GDXF "$keys[$i]$offsets{$keys[$i]}"; }

close GDXF;

exit 0;
