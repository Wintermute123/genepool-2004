#!/usr/bin/perl

###################################
# GXPFETCH
#
# Script to retrieve a sequence
# from a database.
###################################

$| = 1;
$usage = "\nUsage: $0 <database> <accn>\n\n";
$pad = "################";
$masterfound = 0;
#check for environment variable
$genomixdb=$ENV{GENOMIXDB};
if(!$genomixdb) {die "GENOMIXDB environment variable not set\n"; }


#read configuration file
#add other dir location to file here ie, swissprot, embl.
#print "opening cfg file\n";
if (open(CFGFILE,"$genomixdb/cfg/genomixdb.cfg"))
  {
    #print "Configuration file open\n";
    while ($line=<CFGFILE>)
      {

	if ($line =~ /genbank_seq/)
	   {
	     @tmparray = split("=",$line);
	     $genbank_seq = $tmparray[1];
	     chomp($genbank_seq);
	     #print "Genbank_seq: $genbank_seq\n";
	   }
	if ($line =~ /genbank_gdx/)
	   {
	     @tmparray = split("=",$line);
	     $genbank_gdx = $tmparray[1];
	     chomp($genbank_gdx);
	     #print "genbank_gdx: $genbank_gdx\n";
	   }
	if ($line =~ /genbank_fsa/)
	   {
	     @tmparray = split("=",$line);
	     $genbank_fsa = $tmparray[1];
	     chomp($genbank_fsa);
	     #print "genebank_fsa: $genbank_fsa\n";
	   }
      }
  }
close(CFGFILE);

if(@ARGV != 1) { die $usage; }
#$db = shift;
#$gxp = "$db-locus.gxp";
#print "gxp: $gxp\n";
$acc_raw = shift;

#open sequence directory read all files into array.
opendir(LISTDIR,$genbank_seq);
@dirlist=readdir(LISTDIR);
closedir(LISTDIR);
$numfiles=@dirlist;
#print "number files: $numfiles\n";

for ($ab=2; $ab<= $numfiles; $ab++)
  {
    #print "$ab: dirlist[$ab]\n";
    if ($dirlist[$ab] =~ /.seq$/)
	  {
#		print "Found Seq file $dirlist[$ab]\n";
	        $db = "$genbank_seq/$dirlist[$ab]";
		$gxp = "$genbank_gdx/$dirlist[$ab]";
		$gxp .= "_locus.gdx";
		if(length($acc_raw) > 16) { die "\n[$0]: Accession numbers must be 16 characters or less.  Offending acc num: $acc_raw	Exiting...\n\n"; }
		$acc = $acc_raw.(substr($pad, 0, 16-length($acc_raw)));

		open DBF, $db or die "\n[$0]: Couldn't open database $db for reading!\n$usage";
		open GXPF, $gxp or die "\n[$0]: Couldn't open $gxp for reading!\n$usage";

		$esize = 28;
		$fsize = -s $gxp;
		$nument = $fsize/$esize;
		$low = 0;
		$high = $fsize;
		while($high - $low > 64)  {
		  $mid = $low + int(($high - $low)/($esize*2)) * $esize;
		# print STDERR "Binary:	 $low $high... midpoint $mid\n";
		  seek GXPF, $mid, 0;
		  read GXPF, $key, 16;
		  $rv = ($acc cmp $key);
		# print STDERR "Comparing $acc to db $key... val $rv\n";
		  if($rv == 0) {
		    $low = $mid;
		    $high = $mid+$esize;
		  }
		  elsif($rv < 0) { $high = $mid; }
		  else { $low = $mid; }
		}
		$found = 0;
		seek GXPF, $low, 0;
		for($i = $low; $i < $high; $i++)  {
		  read GXPF, $key, 16;
		  read GXPF, $offset, 12;
		  if($acc eq $key) {
		    $found = 1;
		    last;
		  }
		}
		close GXPF;

		#if not found exit with error code 1;
		if($found == 0) { }
                else {
		seek DBF, $offset, 0;
		$line = <DBF>;
		if($line !~ /^LOCUS/) { die "[$0]: Invalid offset returned by index file!\n"; }
		print $line;
		$masterfound=1;
		while($line = <DBF>) {
		   print $line;
		  last if $line =~ /^\/\//;
		
		}
		close DBF;}
	}
  }
#test to see if sequence found anywhere.  Exit 1 if found
#otherwise exit 0 for not found
if ($masterfound == 1)
 {
   #print "Found seq\n$masterfound\n";
   exit 1;
 }
else
 {
   #print "Seq not found\n";
   exit 0;
 }

