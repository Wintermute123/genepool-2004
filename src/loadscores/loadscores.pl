#!/usr/bin/perl
use POSIX;
use DBI();
#usage loadscroes.pl -i score.dat -t dbname -c chr_id


for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    {
	$infile = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-t/)
    {
	$database =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id = $ARGV[$a+1];
    }

    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)  
    {
	$seq_id = $ARGV[$a+1];
    }
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }
}

    $genedb = DBI ->connect("DBI:mysql:database=$database;host=$db_host",$db_username,$db_password,{'RaiseError' => 1});  


open(INFILE,"<$infile");
while ($line = <INFILE>)
{
    @tmparray = split(" ",$line);
    $gene_id = $tmparray[0];
    $score = $tmparray[1];

    $sql = "update chr_".$chr_id."_summary set gpscore = '".$score."' where gene_id = '".$gene_id."'";

    print "$sql\n";
    $genedb -> do($sql);
}
