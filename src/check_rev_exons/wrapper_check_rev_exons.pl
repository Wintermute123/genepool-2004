#!/usr/bin/perl

for ($a=1; $a<= 25; $a++)
{
    $chr_id = $a;
    if ($a == 23)
    { $chr_id = "X";}
    if ($a == 24)
    { $chr_id = "Y";}
    if ($a == 25)
    { $chr_id = "Un"; }
    opendir(MYDIR,"/genepool/inbound/symlinks/$a");
    @dirlist = readdir(MYDIR);
    closedir(MYDIR);
    
    for ($b=2; $b <= $#dirlist; $b++)
    {
	if ($dirlist[$b] =~ /^GXDB/ && $dirlist[$b] =~ /.exons$/)
	{
	    $syscmd = "/genepool/src/check_rev_exons/check_rev_exons.pl /genepool/inbound/symlinks/$a/$dirlist[$b]";
	    print "$syscmd\n";

	    system($syscmd);
	}
    }
}
