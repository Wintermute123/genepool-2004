#!/usr/bin/perl

$infile = $ARGV[0];

@right=();
@left=();
@number=();
@active=();
open(INFILE,"<$infile");
while ($line = <INFILE>)
{
#    print "$line";

    chomp($line);
    
    @tmparray = split(" ",$line);
    push(@number,$tmparray[0]);
    push(@left,$tmparray[1]);
    push(@right,$tmparray[2]);
    push(@active,$tmparray[3]);

}

close(INFILE);
open(OUTFILE,">$infile");
if ($left[1] > $right[1])
{
    @rev_number =  @number;
    @rev_left =  reverse @left;
    @rev_right = reverse @right;
    @rev_active = reverse @active;
}
else
{
    @rev_number = @number;
    @rev_right = @right;
    @rev_left = @left;
    @rev_active = @active;
}

for ($a=0; $a <= $#number; $a++)
{
    print OUTFILE "$rev_number[$a] $rev_left[$a] $rev_right[$a] $rev_active[$a]\n";
}

