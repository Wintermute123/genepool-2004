#!/usr/bin/perl
use POSIX;
use DBI();

#usage: ./protmaker.pl DBNAME CHR_ID

#$build = $ARGV[0];
#$chr_id = $ARGV[1];

#print $build." ".$chr_id;


for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-t/)
    {
	$database =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
    if ($ARGV[$a] =~ /-g/)
    { $gene = $ARGV[$a+1];}
}

if ($DEBUG) { print "infile: $infile dbase: $database chr_id: $chr_id\n"; }






$genomix_gp = "/genepool";

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }
}





$protstring="";
  #null the arrays;
    @coding_left =();
    @coding_right=();
    @exon_left=();
    @exon_right = ();
    @tmparray = ();
    @snp_coord = ();
    @snp_active =();
    @snp_original =();
    @snp_new = ();
    @protein_coord =();
    @protein_original = ();
    @protein_left =();
    @protein_right = ();
    @snp_protein_coord =();
    @protein_stack=();



#################Main Routines Start here##############

#tell perl to use MYSQL

$genedb = DBI ->connect("DBI:mysql:database=$database;host=$db_hostname",$db_username,$db_password,{'RaiseError' => 1});
#$genedb = DBI ->connect("DBI:mysql:database=$build;host=$hostname",$username,$password);

#query the database for the selected chromosome and grab the GXDB, seq_id, gene_number, variant_number
#$sql = "select gene_id, seq_id, gene_number, variant_number from chr_".$chr_id."_summary where 1 order by gene_number, variant_number";
$sql = "select gene_id, seq_id, gene_number, variant_number from chr_".$chr_id."_summary where gene_id = '$gene' order by gene_id";
#print $sql."\n";
$sth=$genedb->prepare($sql);
$sth->execute();

#we got the query, lets load the arguements and process the genes
while (@ary=$sth->fetchrow_array())
{
    $gene_id = $ary[0];
    $seq_id = $ary[1];
    $gene_number = $ary[2];
    $variant_number = $ary[3];
    $protstring="";
    #null the arrays;
    @coding_left =();
    @coding_right=();
    @exon_left=();
    @exon_right = ();
    @tmparray = ();
    @snp_coord = ();
    @snp_active =();
    @snp_original =();
    @snp_new = ();
    @protein_coord =();
    @protein_original = ();
    @protein_new=();
    @protein_left =();
    @protein_right = ();
    @snp_protein_coord =();
    @protein_stack=();
    
    #   print $gene_id."\n";
    #  print $seq_id." ".$gene_number." ".$variant_number."\n";
    
    process_gene();
} #end while mysql result



sub process_gene()
{
#this loads all information about a particular gene.  Then it calls export_mrna
    
#    local $gene_id ;
#    local $seq_id ;
#    local $gene_number ;
#    local $variant_number;
#    local $chr_id ;
    
    
    $filename ="$gprootdir/inbound/symlinks/".$chr_id."/".$seq_id.".prot";
    if (!open(PROTFILE,"<$filename"))
    {
	print "Error opening $filename\n";
#	exit;
    }
    $protstring="";
    
    #read the mrnafile and load the correct sequence
    while ($line = <PROTFILE>)
    {
	if ($line =~ /$gene_id/)
	{
	    #print "Found $gene_id\n";
	    $line = <PROTFILE>;
	    while ($line !~ /^>/ && ! eof PROTFILE)
	    {
		chomp($line);
		$protstring .= $line;
		$line = <PROTFILE>;
	    } #end while line ! fasta header
	} #end if gene_id
    } #end $line = PROTFILE


    #print "PROTSTRING size: ".length($protstring)."\n";
    
    close(PROTFILE);
    
    #now we need to open the NT_XXXXX.out.sup file
    #and load coding, exons and snips
    
    $supfilename = "$gprootdir/inbound/symlinks/".$chr_id."/".$seq_id.".out.sup";
#   print $supfilename."\n";
    if (!open(SUPFILE,"<$supfilename"))
    {
	print "Couldn't open $supfilename\n";
	exit;
    }
    


    #   print "Entering supfile\n";
    #read in the sup file and load the arrays
    while ($line = <SUPFILE>)
    {
	#print "inside supfile\n";
	$compstring = "Gene ".$gene_number." Variant ".$variant_number;
	#print $compstring."\n";
	
	if ($line =~ /$compstring/)
	{
	    #   print "$line\n";
	    
	    $line = <SUPFILE>;
	    #    while ($line !~ /Gene/) #next gene statement is the trigger
	    do
	    {
		if ($line =~ /Gene/)
		{
		    last;
		}
		if ($line =~ /coding/ )
		{
		    @tmparray = split(" ",$line);
		    push(@coding_left,$tmparray[5]);
		    push(@coding_right,$tmparray[6]);
		    # print "CODING_right: ".$tmparray[6]."\n";
		} #end if coding

		if ($line =~ /^exon/)
		{
		    @tmparray = split(" ",$line);
		    push(@exon_left,$tmparray[5]);
		    push(@exon_right,$tmparray[6]);
		    push(@protein_left, $tmparray[7]);
		    push(@protein_right, $tmparray[8]);
		    
		} #end if exon
		
		if ($line =~ /^snip/)
		{  # print "Found SNIP @ coord: ";
		    
		    @tmparray = split(" ",$line);
		    #print "$tmparray[4]\n";
		    push(@snp_protein_coord,$tmparray[4]);
		    push(@snp_active,$tmparray[5]);
		    push(@snp_original,$tmparray[6]);
		    push(@snp_new,$tmparray[7]);
		    $tmparray[8] =~ s/\-/blank/g;
		    $tmparray[9] =~ s/\-/blank/g;
		    push(@protein_original, $tmparray[8]);
		    push(@protein_new, $tmparray[9]);
		} # end if snip
		
		
	    } while ($line = <SUPFILE>); #end while ! new gene

	}  #end if line is correct gene variant number
    }#end while supfile
	
	#$coding_size = @coding_left;
    for ($a =0; $a <= $coding_size-1; $a++)
    {
		print "Coding_left: ".$coding_left[$a]."\n";
		print "Coding_right: ".$coding_right[$a]."\n";
    } #end for a to coding size
    $exon_size = @exon_left;
   # for ($a =0; $a <= $exon_size-1; $a++)
   # {
#		print "Exon_left ".$exon_left[$a]."\n";#
		#print "exon_right ".$exon_right[$a]."\n";
#    } #end for a to exon size
	$snp_size = @snp_coord;
    #print "snip_size: ".$snp_size."\n";
 #for ($zz=0; $zz <= $snp_size; $zz++)
 # { #print "here\n";
#	#print "snp_coord".$snp_coord[$zz]."\n";
#	print "snp_active ".$snp_active[$zz]."\n";
#	print "snp_original ".$snp_original[$zz]."\n";
#	print "snp_new ".$snp_new[$zz]."\n";
 #   }


	#ok now all the arrays are loaded.  Lets export this information
	export_protein();

} #end SUB process_gene





#################Subs start here#######################

sub export_protein()
{
    $exon_type =1;
    $span_on = 0;
#this creates the html code necessary for the mrna section
    #bust the mrnastring up into an mrnaarray
    $protstring2 = " ".$protstring;
    @prot_array = split("",$protstring2);

    #now we are ready to traverse and output the mrna line.
    #stats are 60 chars per line with coding boundries, exon
    #boundries, and active/silent snips.

    #We need to initalize a few more vars first
    $protstring_counter = 0;
    $outputstring_counter = 0;
    $coding_on = 0;
    $exon_on = 0;
    @snp_to_print = ();


  #  print  ' <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><HTML><HEAD><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link rel=stylesheet type="text/css" href="../../../../../code/global/genome.css"></HEAD><BODY background="#FF9966">';
    print  '<tt><font  color="#000000">';

    $prot_size = @protein_left;


     $snip_size = @snp_protein_coord;

    $cursor_pos = 1;
    $exon_type = 1;

    stack_proteins();

    #output the stack to the  file



} #end sub export_mrna


sub stack_proteins()

{
    $line_counter =0;
    $char_count=1;
    $snip_string="";
    $snip_size = @snp_protein_coord;
    #for ($ba=0; $ba <= $snip_size-1; $ba++)
    #{
    #    if ($snp_protein_coord[$ba] > -1)
#	{
#            print  "SP():Snip_coord: $snp_protein_coord[$ba]<br>\n";
#            print  "SP():Protein_origanal: $protein_original[$ba]<br>\n";
#            print  "SP():Protein_new: $protein_new[$ba]<BR>\n";
#	}
#    }



    $protein_string2=" ".$protstring;
    @protein_array = split("",$protein_string2);

    @span_css =();
    $count = @protein_array;
   # print  "Protein Size: $count<BR>";
    $span_css[1] = '<span class="exonedge2">';
    $span_css[2] = '<span class="exonedge3">';
    $span_css[3] = '<span class="exonedge4">';
    $snp_active_css = '<font color = "red">';
    $snp_inactive_css = '<font color = "#FF00FF">';
    $font_css = '<font color="black">';

    $current_span = 1;
    $next_span = 2;

    $snip_string = "";



    #print "entering eu\n";
    $span_on = 0;
   # push(@protein_stack,'<pre><BR>');
   # push(@protein_stack,'         1         2         3         4         5         6<br>');
   # push(@protein_stack,'123456789012345678901234567890123456789012345678901234567890<br></pre>');



    push(@protein_stack,$span_css[$current_span]);
    push(@protein_stack,$protein_array[1]);




    for ($eu=2; $eu <= $count; $eu++)
    {  # print "$eu $count\n";
	$forced_exon=0;
        #if ($eu == 1 && $protein_left[0] == -1)
	#{
        #    push(@protein_stack,"Undefined Coding Region <BR>");
        #    $forced_exon = 1;
        #    $turn_span_on = 1;
	#}

        #check for and process the exon
        $exon_left_result = is_exon_left(\@protein_left,$eu);
        $exon_right_result = is_exon_right(\@protein_right,$eu);
        $exon_shared = is_exon_shared(\@protein_left,\@protein_right,$eu);
        #print "exon_left_result $exon_left_result exon_right $exon_right_result exon_shard $exon_shared\n";
        #$snip_result = is_snip($eu);
	#print "SNIP_RESULT $snip_result\n";
######################################
	$snip_result=0;
	for ($jfk=0; $jfk<=$#snp_protein_coord; $jfk++)
	{
	    if ($eu == $snp_protein_coord[$jfk])
	    {
		if ($snp_active[$jfk] == 1)
		{
		    $snip_result = 1;
		    $snip_num = $jfk;
		    $snp_prot_new = $protein_new[$jfk];
		}
		else
		{
		    $snip_result = 0;
		}
	    }
	}













##############################################################################################################################
	if ($exon_left_result == 1 && $exon_shared ==0)
	{
            push(@protein_stack,"</span>");
            push(@protein_stack,$span_css[$next_span]);
            push(@protein_stack,$protein_array[$eu]);
            $tmp_span = $current_span;
            $current_span = $next_span;
            $next_span = $tmp_span;

          }
        elsif ($exon_shared == 1)
          {
            push(@protein_stack,"</span></span></span>");
            push(@protein_stack,$span_css[3]);
            push(@protein_stack,$protein_array[$eu]);
            push(@protein_stack,"</span></span></span>");
            $forced_exon == 0;

            push(@protein_stack,$span_css[$next_span]);
            $tmp_span = $current_span;
            $current_span = $next_span;
            $next_span = $tmp_span;
          }
        elsif ($snip_result == 1 )
          {
	     # push(@protein_stack,'<br>snip_result:');
	     # push(@protein_stack,$snip_result);
	     # push(@protein_stack,'<BR>');
             push(@protein_stack,'<font class = "active">');
             push(@protein_stack,$protein_array[$eu]);
             push(@protein_stack,'</font>');
             $snip_string .= "(";
             if ($protein_original[$snip_result] =~ /blank/)
              {
                $prot_original = $protein_array[$eu];
              }
            else
              {
                $prot_original = $protein_original[$snip_num];
              }
            if ($protein_new[$snip_result] =~ /blank/)
              {
                #$prot_new = $protein_array[$eu];
		  $prot_new = $snp_prot_new;
              }
            else
              {
                #$prot_new = $protein_new[$snip_num];
		  $prot_new = $snp_prot_new;
              }
            $snip_string .= $prot_original.",".$prot_new.") ";
          }

        else
          {
            push(@protein_stack,$protein_array[$eu]);
          }




##############################################################################################################################
	$char_count++;
	if ($char_count == 60)
	{
	    $line_counter += 60;
	    push(@protein_stack,"</SPAN></span></span></span>&nbsp;&nbsp;&nbsp;$snip_string");
	    push(@protein_stack,"<BR>$span_css[$current_span]");
	    $char_count=0;
	    $snip_string="";
	}







    } #end for loop
    $rfsstring = "</span></span></span></span>";
    for (my $a = $char_count; $a <= 60; $a++)
    {
        $rfsstring .= "&nbsp;";
    }
    $rfsstring.="&nbsp;&nbsp;&nbsp;$snip_string";
    push(@protein_stack,$rfsstring);
    $protein_stack_size = @protein_stack;
    #print "Protein stack size: $protein_stack_size\n";
    $char_count = 0;
    for ($bu=0; $bu <= $protein_stack_size; $bu++)
    {
        print  $protein_stack[$bu];
    }
    #print $protein_stack[$bu];



}



sub is_coding()
{
    local @coding_left = @{$_[0]};
    local @coding_right = @{$_[1]};
    local $pos_to_compare = $_[2];
    if ($pos_to_compare == $coding_left[0])
    { return(1); # Exon On
  }
    elsif ($pos_to_compare == $coding_right[0])
    { return(2); #exon off
  }

    return(0); #not an exon
} #end sub is_exon

sub is_snip()
{
    local $pos_to_comare = $_[0];
    $snip_size = @snp_protein_coord;
   # print "is_snip snipsize: $snip_size\n";
   # for ($ba=0; $ba <= $snip_size; $ba++)
    #  {
     #   print  "is_snip_Snip_coord: $snp_protein_coord[$ba]\n";
     # }



    $num_snips = @snp_protein_coord;
    #print "123_num_snips: ".$num_snips."\n";
    for ($ab = 0; $ab <= $num_snips; $ab++)
    {  # print STDERR "pos_to_cmp: ".$pos_to_compare." Compare: ".$snp_protein_coord[$ab]."\n";
        if ($pos_to_compare == $snp_protein_coord[$ab])
	{# print  "returning snip pos: ".$pos_to_compare."<br>\n";
          #print  "is_snip: snip_result :".$ab."<br>\n";
	  #print "SNIP_FOUN\n";
            return($ab);
	}
    }
    return(-1);

}

sub is_exon_right()
{
    @protein_right = @{$_[0]};
    $gm = $_[1];

    $protein_size = @protein_right;
    for ($je=0; $je <= $protein_size; $je++)
    {
	if ($gm == $protein_right[$je])
        { return(1); }
    }
    return(0);

} #end sub
sub is_exon_left()
{
    @protein_left = @{$_[0]};
    $gm = $_[1];

    $protein_size = @protein_left;
    for ($je=0; $je <= $protein_size; $je++)
    {
	if ($gm == $protein_left[$je])
        { return(1); }
    }
    return(0);

} #end sub




sub is_exon_shared()
{
    @protein_left = @{$_[0]};
    @protein_right =@{$_[1]};
    $gm = $_[2];

    $protein_size = @protein_left;

    for ($je=0; $je <= $protein_size; $je++)
    {
	if ($protein_right[$je] == $protein_left[$je+1] && $protein_right[$je] == $gm)
        { return(1); }
    }
    return(0);

} #end sub



