#!/usr/bin/perl

#this program maps the exons onto the genomix sequence

#usage: seqmaker.pl -i NT*.*.out.sup -c chr_id 



for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    {
	$infile =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

if ($DEBUG) { print "infile: $infile  chr_id: $chr_id\n"; }






$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }
}







#global variables

$gene_number = 0;
$gene_id = 0;
@exon_number = ();
@exon_left=();
@exon_right = ();
$seq_id="";
@tmp_left=();
@tmp_right =();
$reverse = 0;
@acceptor =();
@donor = ();



$infile =~ s/forward_//g;
$infile =~ s/reverse_//g;
$infile =~ s/small_//g;

$tmpfile = $infile;
$tmpfile =~ s/\//SPLIT/g;
@tmparray = split("SPLIT",$tmpfile);
for ($zztop=0; $zztop <= $#tmparray; $zztop++)
{
    if ($tmparray[$zztop] =~ /NT_/)
    {
	$seq_id = $tmparray[$zztop];
	$seq_id =~ s/\.out\.sup//g;
    }


}

#grab the seq_id from the command line
#@tmparray = split(".out",$infile);
#$seq_id = $tmparray[0];

#print "<TT>";
#print "seq_id: $seq_id<BR>";

if (!open(INFILE,"<$infile"))
{
    die "Could not open infile $infile";
}

while ($line = <INFILE>)
{
    if ($line =~ /^Gene/)
    {
	#zero out the arrays
	@exon_number=();
	@exon_left=();
	@exon_right=();
	@tmparray = split(" ",$line);
	$gene_number = $tmparray[1];
	$gene_id = $tmparray[4];
	$reverse=0;
	#print "</font><BR>gene_number: $gene_number  $gene_id<BR>";

	#load the master exon_table
	load_master_exons();
	print_master_exons();
	print_genomic_sequence();
	
    } #end if gene;
} #end while $infile



sub load_master_exons()
{
    $masterfile = "$gprootdir/inbound/symlinks/$chr_id/$seq_id"."_gene_".$gene_number.".master_exons";
    print "master exon file: $masterfile<BR>";
    if (!open(EXONFILE,"<$masterfile"))
    { die "could not open $masterfile<BR>"; }
    push(@exon_left," ");
    push(@exon_right," ");
    push(@exon_number," ");
    while ($exonline = <EXONFILE>)
    {
	#split the line
	@tmparray = split(" ",$exonline);
	#load coords into exon_arrays
	push(@exon_number,$tmparray[0]);
	push(@exon_left,$tmparray[1]);
	push(@exon_right,$tmparray[2]);
	
	

    } #end while exonline
   ## for ($abc =0; $abc <= $#exon_number; $abc++)
#{
#    print "\n\nExon number: $exon_number[$abc]\n";
#}

    close(EXONFILE);
    if ($exon_left[1] > $exon_right[1])
    {
        @exon_tmp = @exon_left;
        @exon_left = @exon_right;
        @exon_right = @exon_tmp;
	#@tmp_number=();
	#for ($rs=1; $rs <= $#exon_number; $rs++)
	#{
	#    $tmp_number[$rs] = $exon_number[$rs];
	#    print  "tmp_num $tmp_number[$rs]<BR>\n";
	#}
        #@tmp_number = reverse @tmp_number;
	#@exon_number =();
	#push(@exon_number," ");
	#for ($rs = 1; $rs <= $#tmp_number; $rs++)
	#{
	#    $exon_number[$rs] = $tmp_number[$rs];
	#    print  "TMP->exon $exon_number[$rs]<BR>\n";
	#}


        $reverse = 1;
    }

} #end sub load_master_exons()

sub print_master_exons()
{
    $arraysize = @exon_left-1;
    for ($a = 0; $a <= $arraysize; $a++)
    {
	print "num:  $exon_number[$a] left: $exon_left[$a] right: $exon_right[$a]<BR>";

    }


} #end sub print_master_exons()

sub print_genomic_sequence()
{
    $outfile = "$gprootdir/inbound/symlinks/$chr_id/images/".$seq_id."/genomic/".$gene_id.".genomic.html";
    $syscmd = "mkdir $gprootdir/inbound/symlinks/$chr_id/images/".$seq_id."/genomic/";
    system($syscmd);

    open(OUTFILE,">$outfile");
    print OUTFILE "<tt>";
    #$seqfile = "$seq_id".".fsa";
    $sequence = " ";
     $seqfile ="$gprootdir/gpdata/gbkdir/symlinks/$chr_id/$seq_id.mfa.fsa";
#    $seqfile ="$gprootdir/inbound/symlinks/$chr_id/$seq_id.mfa.fsa";
    if (!open(SEQFILE,"<$seqfile"))
    { die "Could not open $seqfile";  }
    else
    { print "File $seqfile opened<BR>"; }
    while ($seqline = <SEQFILE>)
    {
	if ($seqline =~ /^>/)
        { #do nothing, we are skipping the fasta header
        }
	else
        {
	    chomp($sequence);
	    $sequence .= $seqline;
        }
    } #end while seqline
    close(SEQFILE);
    print "\n\n".length($sequence)." sequence Length\n";

    #reset the tmp_exons to null
    @tmp_left =();
    @tmp_right =();
    #read the next line of the GP file
    $line = <INFILE>;
    push(@tmp_left," ");
    push(@tmp_right," ");
    while ($line !~ /endvariant/)
    {
	if ($line =~ /^exon/)
	{
	    @tmparray = split(" ",$line);
	    push(@tmp_left,$tmparray[1]);
	    push(@tmp_right,$tmparray[2]);
	}
        $line = <INFILE>;

    }  #found endvariant
    print "Tmp Exons Loaded<BR>";
    if ($tmp_left[1] > $tmp_right[1])
    {
#        @tmp_tmp = @tmp_left;
 #       @tmp_left = @tmp_right;
  #      @tmp_right = @tmp_tmp;
    }

#    if ($reverse == 0)
#    {
	load_acceptor_donor();

  #  for ($abc =0; $abc <= $#exon_left; $abc++)
  #  {
#	print OUTFILE "$exon_number[$abc]  $exon_left[$abc] $exon_right[$abc]<BR>";
#    }



    @seq_array = split("",$sequence);
    $num_exons = @exon_right-1;
    $beg_pos = $exon_left[1];
    $end_pos = $exon_right[$num_exons];
    $cc = 0; #character count
    print "Sequence $beg_pos .. $end_pos<BR>";
    $exonstr="";
    if ($reverse == 0)
    {
	for ($count = $beg_pos; $count <= $end_pos; $count++)
	{
	    $isleft = is_exon_left($count);
	    $isright = is_exon_right($count);
	    $isacceptor = if_acceptor($count);
	    #if ($count == 7855)
	    #{
	    #    print "7855 Found.  isacceptor: $isacceptor\n<BR>";
	    #}
	    
	    if ($isacceptor == 1)
	    {
		print OUTFILE '</font><font color= "red">';
	    }
	    if ($isleft == 1)
	    { 
		print OUTFILE '</font><font color="blue">';
		$exon_on = 1;
		$exon_begin = $count;
		$exon_end = find_end($count);
		$exonnum = get_num($count,$exon_end);
		$exonstr .= "Exon ";
		$tp = $exonnum;
		$exonstr .= $tp." ";
		#$exonstr .=" $exon_left[$exonnum] $exon_right[$exonnum]";
		#$exonstr .= "exon $exonnum ";
	    }
	    
	    #print "$count ".$seq_array[$count]."<BR>";
	    
	    if ($exon_on == 1)
	    {
		if ($reverse == 0)
		{
		    print OUTFILE uc($seq_array[$count]);
		}
		else
		{
		    if (uc($seq_array[$count]) eq "A")
		    {
			print OUTFILE "T";
		    }
		    elsif (uc($seq_array[$count]) eq "C")
		    {
			print OUTFILE "G";
		    }
		    elsif (uc($seq_array[$count]) eq "G")
		    {
			print OUTFILE "C";
		    }
		    elsif (uc($seq_array[$count]) eq "T")
		    {
			print OUTFILE "A";
		    }
		    else
		    {
			print OUTFILE uc($seq_array[$count]);
		    }
		}
	    }
	    else
	    {
		if ($reverse == 0)
		{
		    print OUTFILE lc($seq_array[$count]);
		}
		else
		{
		    if (uc($seq_array[$count]) eq "A")
		    {
			print OUTFILE "t";
		    }
		    elsif (uc($seq_array[$count]) eq "C")
		    {
			print OUTFILE "g";
		    }
		    elsif (uc($seq_array[$count]) eq "G")
		    {
			print OUTFILE "c";
		    }
		    elsif (uc($seq_array[$count]) eq "T")
		    {
			print OUTFILE "a";
		    }
		    else
		    {
			print OUTFILE lc($seq_array[$count]);
		    }
		}
	    }
	    
	    $cc++;
	    
	    if ($isright == 1)
	    {  print OUTFILE "</font><font color=red>";
	       $exon_on = 0;
	   }
	    $isdonor = if_donor($count);
	    if ($isdonor == 1)
	    {
		print OUTFILE "</font><font color=black>";
	    }
	    if ($cc == 59)
	    {
		print OUTFILE "    $exonstr\n<BR>";
		$cc = 0;
		$exonstr ="";
	    }
	} #end For
    } #end if (!reverse)
    else  #if reverse == 1
    {
#	for ($bb =0; $bb <= $#tmp_left; $bb++)
#	{
#	    print OUTFILE "$tmp_left[$bb], $tmp_right[$bb]<BR>";
#	}
#	for ($bb = 0; $bb <= $#donor; $bb++)
#	{
#	    print OUTFILE "donor: $donor[$bb] accept: $acceptor[$bb]<BR>";
#	}



	
	for ($count = $end_pos; $count >= $beg_pos; $count--)
	#for ($count = $beg_pos; $count <= $end_pos; $count++)
	{
	    $isleft = is_exon_left($count);
	    $isright = is_exon_right($count);
	    #$isleft = is_exon_right($count);
	    #$isright = is_exon_left($count);
	    $isacceptor = if_acceptor($count);
	    #if ($count == 7855)
	    #{
	    #    print "7855 Found.  isacceptor: $isacceptor\n<BR>";
	    #}
	    
	    #if ($isacceptor == 1)
	    $isdonor = if_donor($count);
	    if ($isdonor==1 )
	    {
		print OUTFILE '</font><font color= "red">';
	    }
	    if ($isleft == 1)
	    { 
		print OUTFILE '</font><font color="blue">';
		$exon_on = 1;
		$exon_begin = $count;
		$exon_end = find_end($count);
		$exonnum = get_num($count,$exon_end);
		$exonstr .= "Exon ";
		$tp = $exonnum;
		$exonstr .= $tp." ";
		#$exonstr .=" $exon_left[$exonnum] $exon_right[$exonnum]";
		#$exonstr .= "exon $exonnum ";
	    }
	    
	    #print "$count ".$seq_array[$count]."<BR>";
#	    print OUTFILE "\n$count ";
	    if ($exon_on == 1)
	    {
		if ($reverse == 0)
		{
		    print OUTFILE uc($seq_array[$count]);
		}
		else
		{
		    if (uc($seq_array[$count]) eq "A")
		    {
			print OUTFILE "T";
		    }
		    elsif (uc($seq_array[$count]) eq "C")
		    {
			print OUTFILE "G";
		    }
		    elsif (uc($seq_array[$count]) eq "G")
		    {
			print OUTFILE "C";
		    }
		    elsif (uc($seq_array[$count]) eq "T")
		    {
			print OUTFILE "A";
		    }
		    else
		    {
			print OUTFILE uc($seq_array[$count]);
		    }
		}
	    }
	    else
	    {
		if ($reverse == 0)
		{
		    print OUTFILE lc($seq_array[$count]);
		}
		else
		{
		    if (uc($seq_array[$count]) eq "A")
		    {
			print OUTFILE "t";
		    }
		    elsif (uc($seq_array[$count]) eq "C")
		    {
			print OUTFILE "g";
		    }
		    elsif (uc($seq_array[$count]) eq "G")
		    {
			print OUTFILE "c";
		    }
		    elsif (uc($seq_array[$count]) eq "T")
		    {
			print OUTFILE "a";
		    }
		    else
		    {
			print OUTFILE lc($seq_array[$count]);
		    }
		}
	    }
	    
	    $cc++;
	    
	    if ($isright == 1)
	    {  print OUTFILE "</font><font color=red>";
	       $exon_on = 0;
	   }
	    $isdonor = if_donor($count);
	    #if ($isdonor == 1)
            #if (($count+2) == $
	    if ($isacceptor == 1)
	    {
		print OUTFILE "</font><font color=black>";
	    }
	    if ($cc == 59)
	    {
		print OUTFILE "    $exonstr\n<BR>";
		$cc = 0;
		$exonstr ="";
	    }
	} #end For
    } #end if (!reverse)
    
    
    
    

    
    
    
    
    
    if ($cc < 60)
    {
	for (my $b=$cc; $b <=60; $b++)
	{
	    print OUTFILE " ";
	}
	print OUTFILE "$exonstr\n<BR>";
    }
} #end sub print_genomic_sequence


sub is_exon_left()
{
    $pos = $_[0];
    $numexons = @exon_left;
    for ($zz=0; $zz <= $numexons -1; $zz++)
    {
	if ($pos == $tmp_left[$zz])
        {  return 1; }
    }
    return 0;
} #end sub exon left

sub is_exon_right()
{
    $pos = $_[0];
    $numexons = @exon_right;
    for ($zz=0; $zz <= $numexons -1; $zz++)
    {
	if ($pos == $tmp_right[$zz])
        {  return 1; }
    }
    return 0;
} #end sub exon right

sub get_num()
{
    #print "Getnum($_[0],$_[1])\n";
    my $begpos = $_[0];
    my $endpos = $_[1];
    my $numexons = @exon_left;
    if ($reverse == 0)
   {
	for (my $zz=0; $zz <= $numexons -1; $zz++)
	{
	    if ($begpos == $exon_left[$zz] && $endpos == $exon_right[$zz])
	    {
		#print "<BR><BR>begin: $begpos end: $endpos <BR>";
		#print "returning $exon_number[$zz]<BR><BR><BR>";
		return $exon_number[$zz]; }
	}
   }
   else
   {   #for reverse
for (my $zz=0; $zz <= $numexons -1; $zz++)
	{
	    if ($begpos == $exon_right[$zz] && $endpos == $exon_left[$zz])
	    {
#		print OUTFILE "<BR><BR>begin: $begpos end: $endpos <BR>";
	#	print OUTFILE "returning $exon_number[$zz]<BR><BR><BR>";
		return $exon_number[$zz]; }
	}
	
	
    } 
    return 0;
} #end sub exon right


sub get_num2()
{
#    print "get_num($_[0])\n";
    my $pos = $_[0];
    my $numexons = @tmp_left;
    for (my $zz = 0; $zz <= $numexons -1; $zz++)
    {
	if ($pos == $tmp_left[$zz])
	{
	    my $endpos = $tmp_right[$zz];
	    my $begpos = $tmp_left[$zz];
	    return $zz;
	}
    }
    $numexons = @exon_left;
    for (my $zz =0; $zz <= $numexons; $zz++)
    {
#	if ($begpos == $exon_left[$zz] && $endpos == $exon_right[$zz])
	if ($begpos == $exon_left[$zz])
	{
	    $ex_num = $exon_number[$zz];
	    return $ex_num;
	}

    }
    return 0;
} #end getnum2

sub find_end()
{
    my $pos = $_[0];
    my $numexons = @tmp_right;
    for (my $zz = 0; $zz <= $numexons -1; $zz++)
    {
	if ($pos == $tmp_left[$zz])
	{
	    my $endpos = $tmp_right[$zz];
	    my $begpos = $tmp_left[$zz];
	    return $endpos;
	}
    }
}

sub load_acceptor_donor()
{
    @acceptor = ();
    @donor =();
    if ($reverse != 1)
    {
	push(@acceptor," ");
	push(@donor," ");
	
	my $numexons = @tmp_left;
	push(@acceptor,0);
	$tmp = $tmp_right[1]+2;
	push(@donor,$tmp);
	for (my $a=2; $a <= $numexons-1; $a++)
        {
	    $acceptor_coord = $tmp_left[$a] - 3;
	    $donor_coord = $tmp_right[$a] + 2;
	    push(@acceptor,$acceptor_coord);
	    push(@donor,$donor_coord);
        }
    }
    else
    {
	push(@acceptor," ");
	push(@donor," ");
	
	my $numexons = @tmp_left;
	for (my $a=1; $a <= $numexons; $a++)
        {
	    $acceptor_coord = $tmp_right[$a] - 2;
	    $donor_coord = $tmp_left[$a] + 3;
	    push(@acceptor,$acceptor_coord);
	    push(@donor,$donor_coord);
        }
    }
    my $tmp = @donor -1;
#    for (my $a=1; $a <= $tmp; $a++)
#    {
#	print "acceptor $a: $acceptor[$a]\n<BR>";
#	print "donor $a: $donor[$a]<br>\n";
#    }
    
}


sub if_acceptor()
{
    my $pos_to_check = $_[0];
    for (my $a=1; $a <= $numexons-1; $a++)
    {
        if ($pos_to_check == $acceptor[$a])
	{
            return 1;
	}
    }
    return 0;
}

sub if_donor()
{
    my $pos_to_check = $_[0];
    for (my $a=1; $a <= $numexons-1; $a++)
    {
        if ($pos_to_check == $donor[$a])
	{
            return 1;
	}
    }
    return 0;
}






