#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_CHR 60000000

void load_exons(void);
void print_exons(void);
void load_sequence(void);
void print_genomic_sequence(void);
long int find_boundary(long int last_boundary);

FILE *IN;
FILE *SEQFILE;
int a;
char infile[255];
char seqfilename[255];
char outfile[259];
char chr_id[2];
char line[80], *token;
int DEBUG=0;
int numexons=0;
long int boundary=0;
int exon_on = 0;
long int char_count=0;
long int last_boundary=0;
char *sequence;
long int count=0;
int beginb=0, endb=0;


struct exon_struct
{
  int  num;
  long int begin;
  long int end;
  char active[3];
};

struct exon_struct exon[100];

main(int argc, char *argv[])
{
  int tmpexonnum;

  /* Process the command line arguments */
  for (a=0; a <= argc-1; a++)
    {
      if (DEBUG) printf("Argv[%d]: %s\n",a,argv[a]);
      if (strcmp(argv[a],"-i")==0)
	strcpy(infile,argv[a+1]);
      if (strcmp(argv[a],"-c")==0)
	strcpy(chr_id,argv[a+1]);
      if (strcmp(argv[a],"-D")==0)
	DEBUG=1;
      if (strcmp(argv[a],"-s")==0)
	strcpy(seqfilename,argv[a+1]);

  
    }

  if (DEBUG) printf("infile %s\n",infile);
  if (DEBUG) printf("chr_id %s\n",chr_id);
  if (DEBUG) printf("seqfilename %s\n",seqfilename);
  if ((IN = fopen(infile,"r")) == NULL )
    {
      printf("Error %s not opened\n",infile);
      exit(1);
    }
  else
    if (DEBUG) printf("%s opened\n",infile);

  if ((sequence = (char *)malloc(MAX_CHR)) == NULL)
    {
      printf("Malloc Failed\n");
      exit(1);
    }



  load_exons();
  print_exons();

  load_sequence();
  print_genomic_sequence();
}

void print_genomic_sequence(void)
{
  long int a;
  int line_count=0;
  long int first_boundary;
  long int last_boundary;

  last_boundary = exon[numexons].end;
  printf("Last boundary %d\n",last_boundary);
  boundary = find_boundary(0);
  first_boundary = boundary;
  exon_on=0;
  for(a=first_boundary; a<= count; a++)
    {
      line_count++;
      if (a == boundary && beginb == 1)
	{
	  printf("\n\nBoundary %d\n\n",boundary);
	  last_boundary = boundary;
	  printf("%c",sequence[a]);
	  boundary = find_boundary(last_boundary);
	}
      else if   (a == boundary && endb == 1)
	{
	  last_boundary = boundary;
	  printf("%c",sequence[a]);
	  boundary = find_boundary(last_boundary);
	  printf("\n\nBoundary %d\n\n",boundary);
	}
      else
	printf("%c",sequence[a]);
      if (line_count==60)
	{
	  printf("\n");
	  line_count=1;


	}
      if (count == last_boundary)
	break;
    }


}







void load_sequence(void)
{
  char fsastring[1024];
  char ch;

  long int a;
   
  
  
  if ((SEQFILE = fopen(seqfilename,"r")) == NULL )
    {
      printf("Error %s not opened\n",seqfilename);
      exit(1);
    }
  else
    if (DEBUG) printf("%s opened\n",seqfilename);
  
  // load in the fasta string
  fgets(fsastring,1024,SEQFILE); 
  if (DEBUG) printf("fsastring %s\n",fsastring);
  while (ch = fgetc(SEQFILE))
    {
      if (feof(SEQFILE) != 0)
	break;
      
      /* printf("test: %c\n",ch);*/
      
      if (ch != '\n')
	{	
	  
	  count++;
	  sequence[count] = ch;
	  /*	   printf("NOTANEWLINE:%ld %c\n",count,ch); */
	}
    }
  if (DEBUG) printf("sequence length: %d\n",count);
  if (DEBUG) 
    {
      for (a=0; a <= count; a++)
	{
	  printf("%c",sequence[a]);
	}
    }
}













void load_exons(void)
{
  int i;

 

  for(i=1; i<= 1000; i++)
    {
      numexons++;
     fscanf(IN,"%d %d %d %s",&exon[numexons].num,&exon[numexons].begin,&exon[numexons].end,exon[numexons].active);
      if (feof(IN)!= 0)
	{
	  numexons--;
	  break;
	}
      printf("%d %d %d %s\n",exon[numexons].num,exon[numexons].begin,exon[numexons].end,exon[numexons].active);
	
      
    }
}
       
void print_exons(void)
{
  int a;
  printf("\n\nExon Table for %d exons\n",numexons);
  for (a=1; a<=numexons; a++)
    printf("%d %d %d %s\n",exon[a].num,exon[a].begin,exon[a].end,exon[a].active);


}


long int find_boundary(long int last_bound)
{
  /* Scan the exon array and locate the next boundary */
  int a;
  for (a=1; a<= numexons; a++)
    {
      if (exon[a].begin > last_bound)
	{
	  beginb=1;
	  endb=0;
	  return(exon[a].begin);
	}

      if (exon[a].end > last_bound)
	{
	  beginb=0;
	  endb = 1;
	  return(exon[a].end);
	}
    }
  return(0);
}
