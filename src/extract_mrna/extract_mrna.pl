#!/usr/bin/perl

#usage: extract_mrna.pl NT*.mrna GXDBnum

$seq_id = $ARGV[0];
$gene_id = $ARGV[1];

if (!open(INFILE,"<$seq_id"))
{
    print "Could not open $seq_id\n";
    exit(1);
}

while ($line = <INFILE>)
{
  if ($line =~ /^>/)
  {
      $line =~ s/^>//g;
      
      
      @tmparray = split(" ",$line);
     # print "tmparray[0] $tmparray[0]\n";
      if ( $tmparray[0] eq $gene_id)
      {
	  print ">$line";
	  while ($line = <INFILE>)
	  {
	      if ($line =~ /^>/)
	      {
		  close(INFILE);
		  exit(0);
	      }
	      else
	      {
		  print $line;
	      }
	  }
      }
  }
}
