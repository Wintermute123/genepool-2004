#!/usr/bin/perl
use DBI();
#This file adds GXDB information to the sup files

#usage: update_sup.pl sup_file_name chr_id  build

$filename = $ARGV[0];
$outfile = $ARGV[0].".gp";
$chr_id = $ARGV[1];
$seq_id = $filename;
$seq_id =~ s/.out.sup//;
$build = $ARGV[2];



if (!open(INFILE,"<$filename"))
{ print "Could not open $filename\n"; exit; }
open(OUTFILE,">$outfile");
$first = 1;
$rs_genenumber =0;
while ($line = <INFILE>)
{
    
    if ($line =~ /^Gene/)
    {
        chomp($line);
        @tmparray = split(" ",$line);
        $genenumber = $tmparray[1];
        print "genenumber : $genenumber\n";
        $variantnumber = $tmparray[3];
        if ($rs_genenumber == 0 )
	{
            print "setting prev_genenum\n";
            $rs_genenumber = $genenumber;
	    
	}
        print "prev_genenum: $rs_genenumber\n";
        if ($first != 1)
        {
	    print OUTFILE "endvariant\n";
        }
	
        if ($rs_genenumber != $genenumber )
	{ print "prev: $rs_genenumber cur: $genenumber\n";
	  print OUTFILE "endgene\n";
	  $first = 0;
	  $rs_genenumber = $genenumber;
      }
	
        $first=0;
	
	open(PROTFILE,"<$seq_id.prot");
	while ($protline = <PROTFILE>)
	{
	    #find the gene and var num from the prot file
	    if ($protline =~ /^>/)
	    {
		$protline =~ s/^>//;
		@tmpprot1 = split(" ",$protline);
		@tmpprot2 = split("=",$tmpprot1[3]);
		$tmpgene = $tmpprot2[1];
		@tmpprot3 = split("=",$tmpprot1[4]);
		$tmpvar = $tmpprot3[1];
		 
	    }
	    #now we need to compare
	   # print "tmpgene $tmpgene tmpvar $tmpvar\n";
	   # print "tmpgene $tmpprot1[0]\n";
	    if ($tmpgene == $genenumber && $tmpvar == $variantnumber)
	    {
		print "I have a match\n";
		$gene_id = $tmpprot1[0];
		last;
	    }
	}
	
        print OUTFILE "$line $gene_id\n";
    } #end Gene
    else
    {
        print OUTFILE $line;
    }
    
    
    
    
} #end while infile

print OUTFILE "<endvariant>\n<endgene>\n";
