#!/usr/bin/perl

#This program extracts SNP variations from the *.GBK files
#and outputs them to STDOUT.

#Process the command line arguements

$inputfile=$ARGV[0];
if (!open(INFILE,$inputfile))
  {
	print "Cannot open $inputfile\n";
	exit 0;
  }


#set a flag if variation found and a blank variation line;
$variationline ="";
$variationfound =0;

#set pad variable to make data human readable
$pad="                         ";
while ($line=<INFILE>)
  {
    if ($line=~/variation/)
	  { #check to see if a variationline is loaded
	    if ($variationline ne "")
		  {
			#variationline loaded, print then NULL
			print "$variationline\n";
			$variationline="";
		  }
		#split the string by spaces
		@tmparray = split(" ",$line);
		#grab the location number and pad the location variable
		$location = $tmparray[1].(substr($pad, 0+length($tmparray[1]), 25-length($tmparray[1])));
		#add to $variationline
		$variationline = $location;
	  }
	if ($line =~ /\/allele=/ )
	  { #split the input line the quote marks
	    @tmp2array = split("\"",$line);
		#add the allele to padded variation line
		$variationline .= "  $tmp2array[1]  ";
	  } #
	if ($line =~ /db_xref/ && $line =~ /dbSNP/ )
	  { 
	    #split line at quote marks
		@tmp3array = split("\"",$line);
		#add the dbSNP number to the varation line
		$variationline .= "  $tmp3array[1]  ";
		#print "$tmp3array[1]\n";
	  }
	  
		
	  
  }
print $variationline;

printf("\n");
