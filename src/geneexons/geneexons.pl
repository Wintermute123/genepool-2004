#!/usr/bin/perl

#this program maps the exons onto the genomix sequence

#usage: seqmaker.pl -i NT*.*.out.sup -c chr_id -s seq_id



for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    {
	$infile =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-s/)
    {
	$seq_id = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

if ($DEBUG) { print "infile: $infile dbase: $database chr_id: $chr_id\n"; }






$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }
}
close(CFGFILE);


if (!open(INFILE,"<$infile"))
{
    print "Could not open infile: $infile\n";
    exit;
}

while ($line = <INFILE>)
{
    chomp($line);
    if ($line =~ /^Gene/)
    {
	@master_num=();
	@master_left=();
	@master_right=();
	@master_active=();
	@tmp_right=();
	@tmp_left=();
	@tmparray = split(" ",$line);
	$gene_number = $tmparray[1];
	$variant_number =$tmparray[3];
	$gene_id = $tmparray[4];
	$masterfile = $gprootdir."/inbound/symlinks/".$chr_id."/".$seq_id."_gene_".$gene_number.".master_exons";
	if ($DEBUG) { print "masterfile: $masterfile\n"; }
	if (!open(MASTERFILE,"<$masterfile"))
	{
	    print "Could not open masterfile: $masterfile\n";
	    exit;
	}
	while ($masterline = <MASTERFILE>)
	{
	    chomp($masterline);
	    if ($DEBUG) { print "$masterline\n"; }
	    @tmparray = split(" ",$masterline);
	    push(@master_num,$tmparray[0]);
	    push(@master_left,$tmparray[1]);
	    push(@master_right,$tmparray[2]);
	    push(@master_active,"No");
	}
	$line = <INFILE>;
	while ($line !~ /^endvariant/ && !eof(INFILE))
	{
	    if ($line =~ /^exon/)
	    {
		chomp($line);
		@tmparray = split(" ",$line);
		if ($DEBUG) { print "Tmarray: $tmparray[1], $tmparray[2]\n"; }
		check_exon($tmparray[1],$tmparray[2]);
		if ($DEBUG) { print_tmp(); }
		
		
		
	    }#end if exon
		
		
		$line = <INFILE>;
	} #end while line !~ endvariant
	$outfile = "$gprootdir/inbound/symlinks/$chr_id/$gene_id.exons";
	if ($DEBUG) { print "outfile: $outfile\n";}
	open(OUTFILE,">$outfile");
	for ($rs = 0; $rs <= $#master_num; $rs++)
	{
	    print OUTFILE "$master_num[$rs] $master_left[$rs] $master_right[$rs] $master_active[$rs]\n";
	}
	close(OUTFILE);

	
	
	
    } #end if gene
    
} #end while infile



sub check_exon()
{
    $left = $_[0];
    $right = $_[1];

    $mastersize = @master_left;
    for (my $z=0; $z <= $mastersize; $z++)
    {
	if ($left == $master_left[$z] && $right == $master_right[$z])
	{
	    $master_active[$z] = "Yes";
	}

	if ($right == $master_left[$z] && $left == $master_right[$z])
	{
	    $master_active[$z] = "Yes";
	}
    } #end for 1 to mastersize
}


sub print_tmp()
{
    $mastersize = @master_left;
    for (my $z=0; $z <= $mastersize; $z++)
    {
	print "Exon: $master_num[$z],$master_left[$z],$master_right[$z],$master_active[$z]\n";
    }


}
