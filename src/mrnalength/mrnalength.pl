#!/usr/bin/perl
use DBI();
for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    {
	$infile = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-t/)
    {
	$build = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }     
}

$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
     if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
	if ($DEBUG) { print "gpbin: $gpbin \n";}
    }
 if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }

}#end of process CFG



$genedb = DBI -> connect("DBI:mysql:database=$build;host=$db_hostname",$db_username,$db_password,{'RaiseError' => 1});







open(INFILE,"<$infile");

while ($line = <INFILE>)
{
    if ($line =~ /^>/)
    {
	chomp($line);
	@tmparray = split(" ",$line);
	
	$tmparray[0] =~ s/^>//g;
	
	@tmp2array = split("=",$tmparray[0]);
	$gene_id = $tmp2array[0];
	print "$gene_id  ";
	
	@tmp3array = split("=",$tmparray[8]);
	$prot_length = $tmp3array[1];
	print "Length: $prot_length\n";
	$sql = "update chr_".$chr_id."_summary set mrna_length = '$prot_length' where gene_id = '$gene_id'";
	$sth = $genedb -> prepare($sql);
	$sth -> execute();
	
    }
    
    
}
