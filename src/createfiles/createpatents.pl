#!/usr/bin/perl

#grab mysql stuff...
use DBI();


#this program reads the NT*.out.sup file and populates the database 
#with the values contained therein.

#usage: createfiles -t <database>

for ($a = 0; $a <= $#ARGV; $a++)
{
   
    if ($ARGV[$a] =~ /-t/)
    {
	$database =  $ARGV[$a+1];
    }
   
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

if ($DEBUG) { print "infile: $infile dbase: $database chr_id: $chr_id\n"; }






$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }
}


for ($a=17; $a <= 21; $a++)
{
    $folder = "$gprootdir/inbound/symlinks/$a";
    if ($DEBUG) { print "Folder: $folder\n"; }
    
    #read the contents of the directory and grap the out.sup files
    if (!opendir(MYDIR,$folder))
    {
	print "could not read directory $folder\n";
	#exit;
    }
    @dirlist=readdir(MYDIR);
    $numfiles = @dirlist;
    if ($DEBUG) { print "numfiles: $numfiles\n"; }
    closedir(MYDIR);
    
    
    
    for ($b=2; $b <= $numfiles; $b++)
    {
	if ($dirlist[$b] =~ /^NT/ && $dirlist[$b] =~ /.prot$/)
	{
	    $syscmd = "perl $gpbin/patprot -i $folder/$dirlist[$b] -c $a";
	    if ($DEBUG) { print "$syscmd\n"; }
	     system($syscmd);
	}
    }
    
    for ($b=2; $b <= $numfiles; $b++)
    {
	if ($dirlist[$b] =~ /^NT/ && $dirlist[$b] =~ /.mrna$/)
	{
	    $syscmd = "perl $gpbin/patmrna -i $folder/$dirlist[$b] -c $a";
	    if ($DEBUG) { print "$syscmd\n"; }
	     system($syscmd);
	}
	
    }    
    
    
    
}
#end 1 to 22 loop

