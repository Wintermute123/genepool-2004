#!/usr/bin/perl

for ($a=21; $a<= 25; $a++)
{
    $chr_id = $a;
    if ($a == 23)
    { $chr_id = "X";}
    if ($a == 24)
    { $chr_id = "Y";}
    if ($a == 25)
    { $chr_id = "Un"; }
    opendir(MYDIR,"/genepool/inbound/symlinks/$a");
    @dirlist = readdir(MYDIR);
    closedir(MYDIR);
    
    for ($b=2; $b <= $#dirlist; $b++)
    {
	if ($dirlist[$b] =~ /^NT/ && $dirlist[$b] =~ /.prot$/)
	{
	    $syscmd = "/genepool/src/protlength/protlength.pl -i /genepool/inbound/symlinks/$a/$dirlist[$b] -t $ARGV[0] -c $chr_id ";
	    print "$syscmd\n";

	    system($syscmd);
	}
    }
}
