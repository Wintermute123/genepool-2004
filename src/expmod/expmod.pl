#!/usr/bin/perl
use DBI();
use POSIX;
#this script processes an exp out file and
#querys the database for a gxdb number and 
#appends it to the end of the summary line.


$infile = $ARGV[0];
$chr_id = $ARGV[1];
$build = $ARGV[2];
$outfile = $infile;
$outfile =~ s/.out/.e2gff/g;
$seq_id = $infile;
$seq_id =~ s/.out//;


$db_host = "localhost";
$db_username = "root";
$db_password = "marlboro";

#Connect to the mysql database
$genedb = DBI ->connect("DBI:mysql:database=$build;host=$db_host",$db_username,$db_password,{'RaiseError' => 1});


open (INFILE,"<$infile") || die "Could not open $infile\n";
open(OUTFILE,">$outfile");

while ($line = <INFILE>)
{
    if ($line =~ summary)
    {
	chomp($line);
	@tmparray = split(" ",$line);
	$gene_number = $tmparray[0];
	$variant_number = $tmparray[1];
	print "Gene: $gene_number Variant: $variant_number\n";
	$sql = "select gene_id from chr_".$chr_id."_summary where gene_number = '".$gene_number."' and variant_number = '".$variant_number."' and seq_id = '".$seq_id."'";
	#print $sql."\n";
	$sth = $genedb-> prepare($sql);
	$sth->execute();
	while (@ary = $sth->fetchrow_array())
	{
	    $gene_id = $ary[0];
	    print OUTFILE "$line $gene_id\n";
	}
	   
	    
    }
    else
    {
	print OUTFILE $line;
    }
}
