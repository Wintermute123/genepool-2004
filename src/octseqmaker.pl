#!/usr/bin/perl
use POSIX;

# Global variables
@seq = ();
$donor_begin_tag = "<FONT COLOR = \"red\">";
$donor_end_tag = "</FONT>";
$acceptor_begin_tag = "<FONT COLOR = \"red\">";
$acceptor_end_tag = "</FONT>";
$exon_begin_tag = "<FONT COLOR = \"blue\">";
$exon_end_tag = "</FONT>";
$exon_label_begin_tag = "<FONT COLOR = \"blue\">";
$exon_label_end_tag = "</FONT>";
$gene_id = "";
$infile = "";
$chr_id = "";

# Process command-line arguments
for (my $i = 0; $i <= $#ARGV; $i++)
{
    if ($ARGV[$i] =~ /-i/)
    {
	$infile =  $ARGV[$i+1];
    }
    if ($ARGV[$i] =~ /-v/)
    {
	$gene_id =  $ARGV[$i+1];
    }
    if ($ARGV[$i] =~ /-c/)
    {
	$chr_id =  $ARGV[$i+1];
    }
    if ($ARGV[$i] =~ /-D/)
    {
	$DEBUG = 1;
    }
    if ($ARGV[$i] =~ /-b/)
    {
	$build = $ARGV[$i+1];
    }

}

if ($infile eq "")
{
  print STDERR "Error - You must specify an input file with the -i argument\n";
  exit;
}

if ($gene_id eq "")
{
  print STDERR "Error - You must specify a GXDB id number with the -v argument\n";
  exit;
}

if ($chr_id eq "")
{
  print STDERR "Error - You must specify a chromosome with the -c argument\n";
  exit;
}

if ($DEBUG) { print STDERR "infile: $infile  chr_id: $chr_id\n"; }

# Read in genepool configuration information
$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
#    print "GENOMIX_GP environment variable not set\n";
#    exit(1);
    $genomix_gp = "/genepool";
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

open(CFGFILE,"<$cfgfile") or die "Unable to open configuration file $cfgfile\n$!\n";

while (my $line = <CFGFILE>)
{
    chomp($line);
    my @tmparray;

    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }
}
close CFGFILE;

# These three functions do the job of producing the genomic sequence
load_sequence();
compute_regions();
output_genomic_sequence();

# This subroutine computes the global region variables for the variant.
sub compute_regions()
{
    my $exonfile = "$gprootdir/gpdata/builds/$build/symlinks/$chr_id/$gene_id.exons";
    if ($DEBUG) { print STDERR "Exon file: $exonfile\n"; }
    
    open(EXONFILE,"<$exonfile") or die "Unable to open exon table file $exonfile\n$!\n";

    # Initialize global region variables
    $start_of_regions = -1;
    $num_regions = 0;
    @region_lengths = ();
    @region_cases = ();  # boolean array:  0 - lowercase, 1 - uppercase
    %region_labels = ();
    %region_begin_tags = ();
    %region_end_tags = ();
    %region_label_begin_tags = ();
    %region_label_end_tags = ();

    # Now, read in all exons and compute region information
    my $last_exon_edge = -1;  # Store the last exon edge on each iteration so we can compute
                              # the intron length
    while (my $exonline = <EXONFILE>)
    {
	chomp($exonline);
	my @tmparray = split(/\s+/,$exonline);
        
        # Ignore inactive exons
        if ($tmparray[3] eq "No") {next;}

        # For all but the first exon, store the donor, intron, and acceptor between this
        # exon and the last one
        if ($last_exon_edge != -1)
        {
          # Store donor site region
          push(@region_lengths, 2);
          $region_begin_tags{$num_regions} = $donor_begin_tag;
          $region_end_tags{$num_regions} = $donor_end_tag;
          push @region_cases, 0;
          $num_regions++;

          # Store intron region
          # -5 because intron region does NOT contain acceptor and donor
          if ($REVERSE_FLAG == 0)
          {
            push(@region_lengths, $tmparray[1] - $last_exon_edge - 1 - 5);
          }
          else
          {
            push(@region_lengths, $last_exon_edge - $tmparray[1] - 1 - 5);
          }
          push @region_cases, 0;
          $num_regions++;

          # Store acceptor site region
          push(@region_lengths, 3);
          $region_begin_tags{$num_regions} = $acceptor_begin_tag;
          $region_end_tags{$num_regions} = $acceptor_end_tag;
          push @region_cases, 0;
          $num_regions++;
        }

        # If this is the first ACTIVE exon - set the start of all regions and determine the
        # gene's strand
        else
        {
          $start_of_regions = $tmparray[1] - 1;
          if ($tmparray[1] < $tmparray[2]) {$REVERSE_FLAG = 0;}
          else {$REVERSE_FLAG = 1;}
        }

        $last_exon_edge = $tmparray[2];

        # Store exon region
        if ($REVERSE_FLAG == 0)
        {
          push(@region_lengths, $tmparray[2] - $tmparray[1] + 1);
        }
        else
        {
          push(@region_lengths, $tmparray[1] - $tmparray[2] + 1);
        }
        $region_labels{$num_regions} = "Exon $tmparray[0]";
        $region_begin_tags{$num_regions} = $exon_begin_tag;
        $region_end_tags{$num_regions} = $exon_end_tag;
        $region_label_begin_tags{$num_regions} = $exon_label_begin_tag;
        $region_label_end_tags{$num_regions} = $exon_label_end_tag;
        push @region_cases, 1;
        $num_regions++;

    }
    close EXONFILE;
}

# This subroutine simply reads in the FASTA sequence
sub load_sequence()
{
    my @tmparray=();
    @tmparray = split(/\//, $infile);
    for (my $a=0; $a <= $#tmparray; $a++)
    {
	if ($tmparray[$a] =~ /^NT_/)
	{
	    $seqfile = $tmparray[$a];
	}
    }

    $fsafile = "$gbkdir/symlinks/$chr_id/$seqfile";
    $fsafile =~ s/.out.sup/.mfa.fsa/;
    if ($DEBUG) { print STDERR "fsafile: $fsafile\n"; }

    if (!open(FSAFILE,"<$fsafile"))
    {
	print STDERR "Could not open fsa file: $fsafile\n";
	exit(1);
    }
    
    while ($fsaline = <FSAFILE>)
    {
	if ($fsaline !~ /^>/)
	{
	    chomp($fsaline);
	    $sequence .= uc($fsaline);
	}
    }
    @seq = split("",$sequence);
}

# This subroutine prints the actual HTML page.  It assumes the global region variables
# have been defined
sub output_genomic_sequence()
{
  my $label_string = "";
  print_html_top();
  my $bp_count = 0;
  my $seq_ptr = $start_of_regions;
  for (my $i = 0; $i < $num_regions; $i++)
  {
    if (exists($region_begin_tags{$i})) {print $region_begin_tags{$i};}

    if (exists($region_labels{$i}))
    {
      if (exists($region_label_begin_tags{$i})) {$label_string .= $region_label_begin_tags{$i};}
      $label_string .= "&nbsp;&nbsp;&nbsp;&nbsp;";
      $label_string .= $region_labels{$i};
      if (exists($region_label_end_tags{$i})) {$label_string .= $region_label_end_tags{$i};}
    }

    # Core loop to print out bps
    for (my $j = 0; $j < $region_lengths[$i]; $j++)
    {
      my $bp_to_print = $seq[$seq_ptr];
      if ($REVERSE_FLAG == 0) {$seq_ptr++;}
      else
      {
        $seq_ptr--;
        if ($bp_to_print eq "A") {$bp_to_print = "T";}
        elsif ($bp_to_print eq "C") {$bp_to_print = "G";}
        elsif ($bp_to_print eq "G") {$bp_to_print = "C";}
        elsif ($bp_to_print eq "T") {$bp_to_print = "A";}
      }

      if ($region_cases[$i] == 0) {$bp_to_print = lc($bp_to_print);}
      else {$bp_to_print = uc($bp_to_print);}
      print $bp_to_print;

      if ($bp_count % 60 == 59)
      {
        if ($label_string ne "") {print $label_string; $label_string = "";}
        print "\n<BR>\n";
      }
      $bp_count++;
    }

    if (exists($region_end_tags{$i})) {print $region_end_tags{$i};}
  }

  # Don't forget labels on the last line!
  if ($label_string ne "")
  {
    while ($bp_count % 60 != 0) {print "&nbsp;"; $bp_count++;}
    print $label_string;
    $label_string = "";
  }
  print_html_bottom();
}

# Standard HTML page top for all HTML pages
sub print_html_top()
{
  print "<HTML>\n\n<HEAD>\n<TITLE>Genomic Sequence for $gene_id</TITLE>\n</HEAD>\n\n<BODY><TT>\n";
}

# Standard HTML page bottom for all HTML pages
sub print_html_bottom()
{
  print "</BODY>\n\n</HTML>\n";
}
