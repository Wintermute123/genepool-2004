#!/usr/bin/perl

use POSIX;

if (scalar(@ARGV) != 3)
{
  print "Usage:  sup.pl <EXP file> <SNP file> <output file>\n";
  exit;
}

$outfile = $ARGV[2];
open(OUTFILE,"> $outfile") or die "Unable to open output file $outfile:  $!\n";

@snploc = ();
@snporig = ();
@snpnew = ();
$numsnps = 0;

if (open(SNP, "< $ARGV[1]"))
{
    print "opened snp\n";
# Read in and store all single SNPs for the contig.
    while ($snpline = <SNP>)
    {
	if ($snpline =~ /^\d+\s+(A|G|T|C|a|g|t|c)\s+(A|G|T|C|a|g|t|c)\s+dbSNP/)
	{
	    print "Found snp\n";
	    @parts = split(/\s+/, $snpline);
	    push @snploc, $parts[0];
	    push @snporig, tolower($parts[1]);
	    push @snpnew, tolower($parts[2]);
	    $numsnps++;
	}
    }
    close SNP;
}

open(EXP, "< $ARGV[0]") or die "Unable to open exp file $ARGV[0]:  $!\n";
print "opened gbk.exp file\n";

# Move the file pointer to the beginning of the genes.
while (($expline = <EXP>) && ($expline !~ /^begin genes/)) {}

# Now, process each gene.
$FIRST_FLAG = 1;
while ($expline = <EXP>)
{
  if (($expline =~ /^\s+\d+\s+\d+\s+summary/) || ($expline =~ /^end genes/))
  {
      print "Found a summary line\n";
    $tmpline = $expline;
    chomp($tmpline);
    @tmparray = split(" ",$tmpline);
    $accession_version = $tmparray[11];

    # Process the gene just read in.
    if ($FIRST_FLAG == 0)
    {
      # If on the reverse strand, exon boundaries were pushed onto the array backwards!
      if ($strand eq "r")
      {
        @exleft = reverse @exleft;
        @exright = reverse @exright;
        @exphase = reverse @exphase;
        @exlen = reverse @exlen;
        @exctgleft = reverse @exctgleft;
        @exctgright = reverse @exctgright;
      }

      # Frame shifts produce x's in the mrna as "ghost" bps that do not actually
      # exist in the contig and complicate coordinate computations.  Thus, to do
      # the calculations correctly, we need a count of the number of x's between
      # the exons.  Note that our mrnalen variable counts the x's.
      my $exonsum = 0;
      for ($i = 0; $i < $numexons-1; $i++)
      {
        $exonsum += $exlen[$i];
        if ($i > 0) {$exonsum += $exnumxs[$i-1];}
        $exnumxs[$i] = 0;
        if ($mrnalen > 0)
        {
          for ($j = $exonsum + 1; $mrna[$j] eq "x"; $j++) {$exnumxs[$i]++;}
        }
      }
            
      # Next, compute coding in mrna coordinates - handling undetermined bounds separately
      if ($codleft == -1)
      {
        if ($exphase[0] == 0) {$codleft = 1;}
        elsif ($exphase[0] == 1) {$codleft = 3;}
        else {$codleft = 2;}
        $mrnacodleft = $codleft;
      }
      else {$mrnacodleft = gene2mrna($codleft);}
      
      if ($codright == -1)
      {
        $codright = $exright[$numexons-1];
        $mrnacodright = gene2mrna($codright);
      }
      else {$mrnacodright = gene2mrna($codright);}

      # A debugging test
      if ($mrnalen > 0) {test_data_consistency();}

      # Print information about the gene before processing SNPs
      print OUTFILE "Gene $genenum Variant $varnum  $accession_version\n";
      if ($strand eq "f")
      {
        print OUTFILE "coding:  $codctgleft $codctgright $codleft $codright $mrnacodleft $mrnacodright\n";
      }
      else
      {
        print OUTFILE "coding:  $codctgright $codctgleft $codleft $codright $mrnacodleft $mrnacodright\n";
      }
      # Print exon information
      for ($i=0; $i < $numexons; $i++)
      {
        my $exmrnaleft = gene2mrna($exleft[$i]);
        my $exmrnaright = gene2mrna($exright[$i]);
        my $exproteinleft = -1;
        my $exproteinright = -1;
        if (($exmrnaleft >=  $mrnacodleft) && ($exmrnaleft <= $mrnacodright))
        {
          $exproteinleft = floor(($exmrnaleft - $mrnacodleft)/3) + 1;
        }
        if (($exmrnaright >=  $mrnacodleft) && ($exmrnaright <= $mrnacodright))
        {
          $exproteinright = floor(($exmrnaright - $mrnacodleft)/3) + 1;
        }
        if ($strand eq "f")
        {
          print OUTFILE "exon:  $exctgleft[$i] $exctgright[$i] $exleft[$i] $exright[$i] $exmrnaleft $exmrnaright $exproteinleft $exproteinright\n";
        }
        else
        {
          print OUTFILE "exon:  $exctgright[$i] $exctgleft[$i] $exleft[$i] $exright[$i] $exmrnaleft $exmrnaright $exproteinleft $exproteinright\n";
        }
      }

      # Print evidence information
      for ($i=0; $i < $numevids; $i++)
      {
        if ($evidtype[$i] == 0) {print OUTFILE "evid:  ";}
        else {print OUTFILE "pevid:  ";}

        if ($strand eq "f")
        {
          print OUTFILE "$evidctgleft[$i] $evidctgright[$i] $evidleft[$i] $evidright[$i] $eviddb[$i] $evidorg[$i] $evidacc[$i] $evidtissue[$i]\n";
        }
        else
        {
          print OUTFILE "$evidctgright[$i] $evidctgleft[$i] $evidleft[$i] $evidright[$i] $eviddb[$i] $evidorg[$i] $evidacc[$i] $evidtissue[$i]\n";
        }
      }
        
      # Walk through SNPs that lie on this gene, convert coordinates, and
      # print them.
      for ($snpnum = 0; $snpnum < $numsnps; $snpnum++)
      {
        # If the mrna information is not available, skip the reading in of SNPs.
        if ($mrnalen <= 0) {last;}

        if (($snploc[$snpnum] < $geneleft) || ($snploc[$snpnum] > $generight)) {next;}
        my $ctgco = $snploc[$snpnum];
        if ($strand eq "f") {$geneco = $ctgco  - $offset;}
        else {$geneco = $offset - $ctgco;}
        my $mrnaco = gene2mrna($geneco);

        # If on the reverse strand we must use the complement of the SNP.  So store the
        # SNP in different variables and flip if necessary.
        $mysnporig = $snporig[$snpnum];
        $mysnpnew = $snpnew[$snpnum];
        if ($strand eq "r")
        {
          $mysnporig = flip($mysnporig);
          $mysnpnew = flip($mysnpnew);
        }

        # Error checking - make sure that the original SNP bp and that on the contig match.
        if (($mrnaco != -1) && ($mysnporig ne $mrna[$mrnaco]))
        {
          # if they do not, genbank may have them backwards - so go ahead and swap them
          my $tmpsnp = $snporig[$snpnum];
          $snporig[$snpnum] = $snpnew[$snpnum];
          $snpnew[$snpnum] = $tmpsnp;
          $tmpsnp = $mysnporig;
          $mysnporig = $mysnpnew;
          $mysnpnew = $tmpsnp;

          # Now, if they still aren't correct, print an error
          if ($mysnporig ne $mrna[$mrnaco])
          {
            print STDERR "Error - bps for SNP and bp in mrna do not match at contig coordinate $ctgco $mysnporig $mysnpnew $mrna[$mrnaco].\n";
            # exit;
          }
        }
 
        # Now, analyze the effect of the SNP on the protein
        my $proteinco = -1;
        my $active = 0;
        my $proteinorig = "-";
        my $proteinnew = "-";
        my $triple;
        if (($mrnaco >= $mrnacodleft) && ($mrnaco <= $mrnacodright))
        {
          $proteinco = floor(($mrnaco - $mrnacodleft)/3) + 1;
        }
        if (($mrnaco >= $mrnacodleft) && ($mrnaco <= $mrnacodright) &&
                                                                   ($proteinco <= $proteinlength))
        {
          # Put together the alternate codon for the protein and see if it causes a
          # different translation
          if (($mrnaco - $mrnacodleft) % 3 == 0)
          {
            $triple = $mysnpnew.$mrna[$mrnaco+1].$mrna[$mrnaco+2];
          }
          elsif (($mrnaco - $mrnacodleft) % 3 == 1)
          {
            $triple = $mrna[$mrnaco-1].$mysnpnew.$mrna[$mrnaco+1];
          }
          else
          {
            $triple = $mrna[$mrnaco-2].$mrna[$mrnaco-1].$mysnpnew;
          }
          my $protrans = trans($triple);
          if ($protrans ne $protein[$proteinco])
          {
            $active = 1;
            $proteinorig = $protein[$proteinco];
            $proteinnew = $protrans;
          }
        }
          
        print OUTFILE "snip:  $ctgco $geneco $mrnaco $proteinco $active $mysnporig $mysnpnew $proteinorig $proteinnew\n";
      }
    }
    else {$FIRST_FLAG = 0;}

    # Exit point for when we hit the end of the genes.
    if ($expline =~ /^end genes/) {exit;}

    # Now, initialize variables for the next gene.
    @exleft = ();
    @exright = ();
    @exctgleft = ();
    @exctgright = ();
    @exphase = ();
    @exlen = ();
    @evidtype = ();
    @evidleft = ();
    @evidright = ();
    @evidctgleft = ();
    @evidctgright = ();
    @eviddb = ();
    @evidorg = ();
    @evidacc = ();
    @evidtissue = ();
    $mrnalen = 0;
    $numexons = 0;
    $numevids = 0;
    @parts = split(/\s+/, $expline);
    $genenum = $parts[1];
    $varnum = $parts[2];
    $strand = $parts[4];
    
    # NOTE:  geneleft and generight are in CONTIG coordinates
    $geneleft = $parts[6];
    $generight = $parts[7];
    $genelength = $generight - $geneleft + 1;
    #  Offset for converting from contig to gene coordinates.  For reverse strand we
    # subtract coordinate from offset.  For forward strand subtract offset from
    # coordinate.
    if ($strand eq "f") {$offset = $geneleft - 1;}
    else {$offset = $genelength + $geneleft;}

    # Read in coding coordinates
    $codctgleft = $parts[8];
    $codctgright = $parts[9];
    if ($strand eq "f")
    {
      $codleft = $parts[8] - $offset;
      $codright = $parts[9] - $offset;
    }
    else
    {
      # Don't forget to flip left and right!
      $codleft = $offset - $parts[9];
      $codright = $offset - $parts[8];
    }

    # We do not want to translate -1 if coding is undetermined
    if ($parts[8] == -1)
    {
      if ($strand eq "f") {$codleft = -1;}
      else {$codright = -1;}
    }
    if ($parts[9] == -1)
    {
      if ($strand eq "f") {$codright = -1;}
      else {$codleft = -1;}
    }
  }

  # Read in exon coordinates
  if ($expline =~ /^\s+\d+\s+\d+\s+exon/)
  {
    @parts = split(/\s+/, $expline);
    push @exctgleft, $parts[4];
    push @exctgright, $parts[5];
    if ($strand eq "f")
    {
      push @exleft, ($parts[4] - $offset);
      push @exright, ($parts[5] - $offset);
    }
    else
    {
      # Don't forget to flip left and right!
      push @exleft, ($offset - $parts[5]);
      push @exright, ($offset - $parts[4]);
    }
    push @exphase, $parts[6];
    push @exlen, ($parts[5] - $parts[4] + 1);
    $numexons++;
  }

  # Read in evidence data
  if ($expline =~ /^\s+\d+\s+\d+\s+(p|)evidence/)
  {
    chomp $expline;
    @parts = split(/\s+/, $expline);
    if ($parts[3] =~ /pevidence/) {push @evidtype, 1;}
    else {push @evidtype, 0;}
    push @eviddb, $parts[4];
    push @evidorg, $parts[5];
    push @evidacc, $parts[6];
    push @evidctgleft, $parts[8];
    push @evidctgright, $parts[9];
    if ($strand eq "f")
    {
      push @evidleft, ($parts[8] - $offset);
      push @evidright, ($parts[9] - $offset);
    }
    else
    {
      # Don't forget to flip left and right!
      push @evidleft, ($offset - $parts[9]);
      push @evidright, ($offset - $parts[8]);
    }
    push @evidtissue, $parts[14];
    $numevids++;
  }

  # Read in mrna sequence
  if ($expline =~ /^\s+\d+\s+\d+\s+mrna/)
  {
    chomp $expline;
    $mrnastring = $expline;
    $mrnastring =~ s/^\s+\d+\s+\d+\s+mrna\s+(.*)$/$1/;
    $mrnastring = " $mrnastring";
    $mrnastring = tolower($mrnastring);
    @mrna = split(//, $mrnastring);
    $mrnalen = scalar(@mrna) - 1;
  }

  # Read in protein sequence
  if ($expline =~ /^\s+\d+\s+\d+\s+protein/)
  {
    chomp $expline;
    $proteinstring = $expline;
    $proteinstring =~ s/^\s+\d+\s+\d+\s+protein\s+(.*)$/$1/;
    $proteinstring = " $proteinstring";
    $proteinstring = toupper($proteinstring);
    # Strip out X's at end
    $proteinstring =~ s/^(.*?)X+$/$1/;
    @protein = split(//, $proteinstring);
    $proteinlength = scalar(@protein) - 1;
  }
}

close EXP;

# An optional subroutine - which is why this code was set aside as a separate routine - that will
# retranslate the protein using our exon boundaries, gene2mrna function, etc. and check if the
# translation matches with that read from the EXP out file.
sub test_data_consistency()
{
  my $testprotein = " ";
  my $oldproteinexcoord = -1;
  for (my $i=0; $i<$numexons; $i++)
  {
    for (my $j=0; $j<$exlen[$i]; $j++)
    {
      my $excoord = $exleft[$i] + $j;
      if (($excoord < $codleft) || ($excoord > $codright)) {next;}
      my $mrnaexcoord = gene2mrna($excoord);
      # Convert to protein coordinates and only translate when we have a new protein
      # This method gets us around frame shift difficulties
      my $proteinexcoord = floor(($mrnaexcoord - $mrnacodleft)/3) + 1;
      if ($proteinexcoord == $oldproteinexcoord) {next;}
      if ($proteinexcoord > $proteinlength) {last;}

      if (($mrnaexcoord - $mrnacodleft) % 3 == 0)
      {
        $testcodon = $mrna[$mrnaexcoord].$mrna[$mrnaexcoord + 1].$mrna[$mrnaexcoord + 2];
      }
      elsif (($mrnaexcoord - $mrnacodleft) % 3 == 1)
      {
        $testcodon = $mrna[$mrnaexcoord - 1].$mrna[$mrnaexcoord].$mrna[$mrnaexcoord + 1];
      }
      else
      {
        $testcodon = $mrna[$mrnaexcoord - 2].$mrna[$mrnaexcoord - 1].$mrna[$mrnaexcoord];
      }
      $testprotein .= trans($testcodon);
      $oldproteinexcoord = $proteinexcoord;
    }
  }

  if ($testprotein ne $proteinstring)
  {
    print STDERR "Error - inconsistency in EXP protein translation and my translation:\nEXP translation\n$proteinstring\nmy translation\n$testprotein\n";
    # exit;
  }
}

# Return the complement of a given bp or an n for bad input
sub flip()
{
  my $bp = tolower($_[0]);
  
  if ($bp eq "a") {return "t";}
  if ($bp eq "c") {return "g";}
  if ($bp eq "g") {return "c";}
  if ($bp eq "t") {return "a";}
  return "n";
}

# This subroutine converts a given gene coordinate to its mrna coordinate.  Please note
# that all the exon arrays from the main program must be initialized correctly before
# this subroutine will work.  Note that this is NOT an independent function.  Function returns
# -1 if the coordinate does not lie on an exon or if it is simply out-of-range of the gene.
sub gene2mrna()
{
  $pos = $_[0];
  my $exonsum = 0;
  my $xsum = 0;
  for (my $i = 0; $i < $numexons; $i++)
  {
    if ($pos > $exright[$i])
    {
      $exonsum += $exlen[$i];
      $xsum += $exnumxs[$i];
    }
    else
    {
      if ($pos < $exleft[$i]) {return -1;}
      return ($exonsum + $xsum + ($pos - $exleft[$i] + 1));
    }
  }
  return -1;
}

# This function translates a given codon (three-letter string) to its corresponding protein.
# Stops (tag, tga, and taa) are translated as "*" and an "X" is returned on any other condition.
# These are the same conventions as that of the EXP code.
sub trans()
{
  my $codon = tolower($_[0]);
  my @codonarray = split(//, $codon);

  if ($codon eq "ttt") {return "F";}
  if ($codon eq "ttc") {return "F";}
  if ($codon eq "tta") {return "L";}
  if ($codon eq "ttg") {return "L";}
  if (($codonarray[0] eq "c") && ($codonarray[1] eq "t")) {return "L";}
  if ($codon eq "att") {return "I";}
  if ($codon eq "atc") {return "I";}
  if ($codon eq "ata") {return "I";}
  if ($codon eq "atg") {return "M";}
  if (($codonarray[0] eq "g") && ($codonarray[1] eq "t")) {return "V";}
  if (($codonarray[0] eq "t") && ($codonarray[1] eq "c")) {return "S";}
  if (($codonarray[0] eq "c") && ($codonarray[1] eq "c")) {return "P";}
  if (($codonarray[0] eq "a") && ($codonarray[1] eq "c")) {return "T";}
  if (($codonarray[0] eq "g") && ($codonarray[1] eq "c")) {return "A";}
  if ($codon eq "tat") {return "Y";}
  if ($codon eq "tac") {return "Y";}
  if ($codon eq "cat") {return "H";}
  if ($codon eq "cac") {return "H";}
  if ($codon eq "caa") {return "Q";}
  if ($codon eq "cag") {return "Q";}
  if ($codon eq "aat") {return "N";}
  if ($codon eq "aac") {return "N";}
  if ($codon eq "aaa") {return "K";}
  if ($codon eq "aag") {return "K";}
  if ($codon eq "gat") {return "D";}
  if ($codon eq "gac") {return "D";}
  if ($codon eq "gaa") {return "E";}
  if ($codon eq "gag") {return "E";}
  if ($codon eq "tgt") {return "C";}
  if ($codon eq "tgc") {return "C";}
  if ($codon eq "tgg") {return "W";}
  if (($codonarray[0] eq "c") && ($codonarray[1] eq "g")) {return "R";}
  if ($codon eq "agt") {return "S";}
  if ($codon eq "agc") {return "S";}
  if ($codon eq "aga") {return "R";}
  if ($codon eq "agg") {return "R";}
  if (($codonarray[0] eq "g") && ($codonarray[1] eq "g")) {return "G";}
  if ($codon eq "tag") {return "*";}
  if ($codon eq "tga") {return "*";}
  if ($codon eq "taa") {return "*";}
  return "X";
}
