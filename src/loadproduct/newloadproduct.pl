#!/usr/bin/perl
#------------------------------------
#Script:	loadproduct.pl -i [/subdirectory/] -t [database]
#Example:	perl loadproduct.pl -i /data/GenePOOL/development/CHR_17/ -t development
#Date:		1/28/2002 (command driven)
#Programmer:	Ray Surface and Mary Perkins
#Description:	This script will load the product data into genepool. It grabs product information from blast 
#		outfiles and updates the prodcut field on the corresponding
#---------------------------------------------------------------------
use DBI;

#setup the mysql variable information
#$database="development";
$hostname = "localhost";
$user="root";
$pass="marlboro";


sub open_databases()
{
    #add all open DBI things here
    $genedb = DBI->connect("DBI:mysql:database=$database;host=$hostname",$user,$pass,{RaiseError => 1});
    if ($debug)
    {
	$logtime = get_time();
	#print DEBUG "$logtime: MySQL opened $database, $hostname\n";
    }
}

$ARGC = @ARGV;

#check for appropriate number of options
if ($ARGC < 3 )
{
    print_usage();
    exit 1;
}

#define variable here from command line.
for ($a=0; $a<=$ARGC; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    { 
	$folder = $ARGV[$a+1];  #location of CHR_* directories
    }
    if ($ARGV[$a] =~ /-t/)
    {
	$database = $ARGV[$a+1];  #which database to use
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id = $ARGV[$a+1]; 
    }
}


#grab product information from blast outfiles and update the prodcut field on the corresponding GXDB file.

#$folder=$ARGV[0];
open_databases();
opendir(MYDIR,$folder);
@dirlist = readdir(MYDIR);
$numfiles = @dirlist;
closedir(MYDIR);

for ($a = 1; $a <= $numfiles; $a++)
{
    if ($dirlist[$a] =~ /^out_/)
    {
	if  (open(INFILE,"$folder$dirlist[$a]"))
	{
	    print "input is opened $folder$dirlist[$a]\n";
	}
	else
	{
	    print "Error opening $folder$dirlist[$a]\n";
	}
	
        @filearray = split("out_",$dirlist[$a]);
        @gxdbarray = split("-seq",$filearray[1]);
        #for ($b = 0; $b <= 5; $b++)
	# { print "$b: $gxdbarray[$b]"; }
        $gxdbnum = $gxdbarray[0];
        print "gxdbnum = $gxdbnum\n";
        $count = 1;
        while ($line = <INFILE>)
	{  $header = "";
	   if ($line =~ /No hits found/)
	   {
	       $sql = "update chr_".$chr_id."_summary set product = 'No Hits Found' where gene_id = '".$gxdbnum."'";
	       print "$sql\n";
	       $sth=$genedb->prepare($sql);
	       $sth -> execute();  
	       last;
	   }


	    if ($line =~ /^>/)
	    {
		print "$line";
		chomp ($line);
		while ($line !~ /Length/)
		{
		    $header .= $line;
		    $line = <INFILE>;
		    chomp($line);
		}
		$header =~ s/          //g;
		$header =~ s/DATE/\|/g;
		#print "header: $header\n";
		@tmparray = split (/\|/,$header);
		$product = @tmparray[4];
		$product =~ s/[\!\@\#\$\%\&\(\)\~\*]//;
		$product =~ s/\'//g;
		#print "product: $product \n";
		if ($count == 1)
		{
		    $sql = "update chr_".$chr_id."_summary set product = '".$product."' where gene_id = '".$gxdbnum."'";
		    print "$sql\n";
		    $sth=$genedb->prepare($sql);
		    $sth -> execute();
		    $count++;
		
		}
		if ($count <= 15)
		{
		    $sql = "insert into master_products (gene_id,product,chr_id) values ('$gxdbnum','$product','$chr_id')";
		   
		    print "$count: $sql\n";
 		    $sth=$genedb->prepare($sql);
		    $sth -> execute();
		    $count++;
		
		}
		if ($count >= 16)
		{
		    print "Count excceeded! Exiting\n";
		    last;
		}

		
	    }#end if header


	}
           
    }
    close(INFILE);
}

###############################################
#Subroutines start here
###############################################

sub print_usage()
{
    print "========================================================\n";
    print "loadproduct.pl  (c)Copyright 2002, by Genomix Corporation\n";
    print "========================================================\n";
    print "Usage: $0  -i  base subdirectory to start from \n";
    print "Usage: $0  -t  database to use\n";
    
    print <<  "end_tag";
  Options:
    
    -i              base subdirectory where the .out files are you want to process. 
	
	-t              database to use.
example:
        perlloader.pl -i /in/path/ -t development   (note: Don't forget to type the last '/' on the path)
end_tag
}

