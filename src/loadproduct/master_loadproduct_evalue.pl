#!/usr/bin/perl
#------------------------------------
#Script:	loadproduct.pl -i [/subdirectory/] -t [database]
#Example:	perl loadproduct.pl -i /data/GenePOOL/development/CHR_17/ -t development -c chr_id
#Date:		1/28/2002 (command driven)
#Programmer:	Ray Surface and Mary Perkins
#Description:	This script will load the product data into genepool. It grabs product information from blast 
#		outfiles and updates the prodcut field on the corresponding
#---------------------------------------------------------------------
use DBI;

#setup the mysql variable information
#$database="development";
$hostname = "localhost";
$user="root";
$pass="marlboro";


sub open_databases()
{
    #add all open DBI things here
    $genedb = DBI->connect("DBI:mysql:database=$database;host=$hostname",$user,$pass,{RaiseError => 1});
    if ($debug)
    {
	$logtime = get_time();
	#print DEBUG "$logtime: MySQL opened $database, $hostname\n";
    }
}

$ARGC = @ARGV;

#check for appropriate number of options
if ($ARGC < 3 )
{
    print_usage();
    exit 1;
}

#define variable here from command line.
for ($a=0; $a<=$ARGC; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    { 
	$folder = $ARGV[$a+1];  #location of CHR_* directories
    }
    if ($ARGV[$a] =~ /-t/)
    {
	$database = $ARGV[$a+1];  #which database to use
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id = $ARGV[$a+1]; 
    }
}


#grab product information from blast outfiles and update the prodcut field on the corresponding GXDB file.

#$folder=$ARGV[0];
open_databases();
opendir(MYDIR,$folder);
@dirlist = readdir(MYDIR);
$numfiles = @dirlist;
closedir(MYDIR);

for ($a = 1; $a <= $numfiles; $a++)
{
    if ($dirlist[$a] =~ /^out_/)
    {
	if  (open(INFILE,"$folder/$dirlist[$a]"))
	{
	    print "input is opened $folder$dirlist[$a]\n";
	}
	else
	{
	    print "Error opening $folder$dirlist[$a]\n";
	}
	
        @filearray = split("out_",$dirlist[$a]);
        @gxdbarray = split("-seq",$filearray[1]);
        #for ($b = 0; $b <= 5; $b++)
	# { print "$b: $gxdbarray[$b]"; }
        $gxdbnum = $gxdbarray[0];
#        print "gxdbnum = $gxdbnum\n";
        $first = 1;
        while ($line = <INFILE>)
	{
        
            if ($line =~ /^>gi/ && $first >1)
	    {
		$line =~ s/\s+//;
		$line =~ s/[\*\!\@\#\$\~\%\&\(\)]//;
		$line =~ s/\'//;
		chomp($line);
		$linestr=$line;
		do {
		    $line = <INFILE>;
		    #$line =~ s/\s+//g;
		    $line =~ s/[\!\@\#\$\%\&\(\)\~\*]//;
		    $line =~ s/\'//g;
		    chomp($line);
		    
		    if ($line =~ /Length/)
		    {}
		    else
		    {
			$linestr .= $line;
		    }
		} until ($line =~ /Length/);
		#check the eval.  Must be greater than 1e-05 or 0
		$line=<INFILE>;
		$line=<INFILE>;
		#print "\n\n1:$line";
		#strip the spaces
		$line=~s/\s+//g;
		print "\n\n1: $line\n";
		chomp($line);
		$updatedb = 0;
		$line=~ s/\=/SPLIT/g;
		@etmparray=split("SPLIT",$line);
		#for ($ghi=0; $ghi <= $#etmparray; $ghi++)
		#{
		#    print "GHI: $ghi: $etmparray[$ghi]\n";
		#}
		print "a:etmparray[2]: $etmparray[2]\n";
		if ($etmparray[2] == 0)
		{
		    $updatedb =0;
		    print "e-value: $etmparray[2] passed\n";
		}

		elsif ($etmparray[2] =~ /e/)
		{   print "testing numeric value\n";
		    @raytmp = split("-",$etmparray[2]);
		    for ($rs=0; $rs <= $#raytmp; $rs++)
		    {
			print "$rs: $raytmp[$rs]\n";
		    }
		    if ($raytmp[1] >= 5)
		    {
			print "passed numeric test $etmparray[2]\n";
			$updatedb = 1;
		    }
		}
		
		else
		{
		    $updatedb =0;
		    print "A: E-value $etmparray[2] failed\n";
		}
		
		
 #               print "LINESTR: $linestr\n";
		@product_array = split( "[)]" ,$linestr);
		$product = $product_array[1];
                print "before = $product\n";
		
		$product =~ s/[\(\)\&\*\!\@\#\$\%]//;
		
		$product =~ s/\'//g;
		print "Product: $product\n";
		if ($updatedb == 1)
		{

		    $sql = "insert into master_products (gene_id, product, chr_id) values ('$gxdbnum','$product','$chr_id')";
		    #       print $sql;
		    $sth=$genedb->prepare($sql);
		    $sth -> execute();
		    $first++;
		} 
		
		
		
	    } #end not the first product
	    if ($line =~ /^>/ && $first == 1)
	    {
		#$line =~ s/\s+//g;
		$line =~ s/[\*\!\@\#\$\~\%\&\(\)]//;
		$line =~ s/\'//g;
		chomp($line);
		$linestr=$line;
		do {
		    $line = <INFILE>;
		    $line =~ s/\s+//;
		    $line =~ s/[\!\@\#\$\%\&\(\)\~\*]//;
		    $line =~ s/\'//;
		    chomp($line);
		    
		    if ($line =~ /Length/)
		    {}
		    else
		    {
			$linestr .= $line;
		    }
		} until ($line =~ /Length/);
$line=<INFILE>;
		$line=<INFILE>;
		print "2: $line";
		#strip the spaces
		$line=~s/\s+//g;
		chomp($line);
		$updatedb = 0;
		@etmparray=split("=",$line);
		#for ($ghi=0; $ghi <= $#etmparray; $ghi++)
		#{
		#    print "2GHI: $ghi: $etmparray[$ghi]\n";
		#}
		if ($etmparray[1]  == 0)
		{
		    $updatedb=1;
		    print "E-value $etmparray[1] pass\n";
		}
		elsif ($etmparray[1] =~ /e/)
		{
		    $e2tmparray = split("-",$etmparray[1]);
		    if ($e2tmparray[1] >= 5)
		    {
			$updatedb=1;print "E-value $etmparray[1] pass\n";
		    }
		}
		elsif ($etmparray[1] =~ /^e/)
		{
		    $updatedb = 1;print "E-value $etmparray[1] pass\n";
		}
		else
		{
		    print "E-value $etmparray[1] failed\n";
		}




		@product_array = split( "[)]" ,$linestr);
		$product = $product_array[1];
	#	print "before = $product\n";
		
		$product =~ s/[\(\)\&\*\!\@\#\$\%]//;
		
		$product =~ s/\'//g;
	#	print "Product: $product\n";
                $sql = "update chr_".$chr_id."_summary set product ='$product' where gene_id = '$gxdbnum'";
                print $sql;
		$sth=$genedb->prepare($sql);
#		$sth -> execute();
                if ($updatedb == 1)
		{
		$sql = "insert into master_products (gene_id, product, chr_id) values ('$gxdbnum','$product','$chr_id')";
         #       print $sql;
		$sth=$genedb->prepare($sql);
		$sth -> execute();
		
	    } $first++;
	    }
	}
	
    }
    close(INFILE);
}

###############################################
#Subroutines start here
###############################################

sub print_usage()
{
    print "========================================================\n";
    print "loadproduct.pl  (c)Copyright 2002, by Genomix Corporation\n";
    print "========================================================\n";
    print "Usage: $0  -i  base subdirectory to start from \n";
    print "Usage: $0  -t  database to use\n";
    print "Usage: $0  -c chromosome id number\n";
    print <<  "end_tag";
  Options:
    
    -i              base subdirectory where the .out files are you want to process. 
	
	-t              database to use.
example:
        perlloader.pl -i /in/path/ -t development   (note: Don't forget to type the last '/' on the path)
end_tag
}

