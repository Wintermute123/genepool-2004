#!/usr/bin/perl

print "Content-type: text/html\n\n";
print "<HTML><HEAD></HEAD><body>";
print "<PRE>";
$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
   $genomix_gp = "../../"
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }

}
#grab the variables from the content_length string
#passed from the web browser

if ($ENV{'REQUEST_METHOD'} eq 'POST')
{
    #grab the input
    read(STDIN,$buffer,$ENV{'CONTENT_LENGTH'});
    #split the name-value pairs
    @pairs = split(/&/,$buffer);
}

for ($a=0; $a<=$#pairs; $a++)
{
    print "$pairs[$a]\n";
    if ($pairs[$a] =~ /sequence/)
    {
	@tmparray = split("=",$pairs[$a]);
	$sequence= $tmparray[1];
    }
    if ($pairs[$a] =~ /program/)
    {
	@tmparray = split("=",$pairs[$a]);
	$program= $tmparray[1];
    }
    if ($pairs[$a] =~ /blast_db/)
    {
	@tmparray = split("=",$pairs[$a]);
	$blast_db= $tmparray[1];
    }
    if ($pairs[$a] =~ /matrix/)
    {
	@tmparray = split("=",$pairs[$a]);
	$matrix= $tmparray[1];
    }
    if ($pairs[$a] =~ /evalue/)
    {
	@tmparray = split("=",$pairs[$a]);
	$evalue= $tmparray[1];
    }
    if ($pairs[$a] =~ /wsize/)
    {
	@tmparray = split("=",$pairs[$a]);
	$wsize = $tmparray[1];
    }
}

$pid =$$;

$filename = "blast.fsa.$pid";
if (!open(OUTFILE,">$filename"))
{
    print "couldn't open tmp file $filename";
    exit;
}
print OUTFILE $sequence."\n";
print "seq: $sequence";
close(OUTFILE);




