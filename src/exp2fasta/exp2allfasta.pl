#!/usr/bin/perl
#  Script name: exp2allfasta.pl -c [chromosome number] -i [file name] -o [output path]
#               exp2allfasta.pl -c 3 -i /in/path/NT_0011878.7_exp.out -o /out/outputpath/CHR_01OUT
#  Date:        12/18/2001
#  programmers: Mary E. Perkins 
#  Description: This program will create 2 FASTA files from EXP 5 output.  One file named CHR_?.prot and 
#		CHR_?.mrna All chromosome 1 EXP files will be made into a FASTA file.
#		Example:  NT_0011878.7_exp.out will be combined into one FASTA file each out file will 
#		have a FASTA header with sequences. You would get CHR_1.prot.fsa and CHR_1.mrna.fsa. The 
#		next available GxDB number will be used and the same GXDB number will be used in the prot file
#		and mrna file.
 #--------------------------------------------------------------------------------

$genomix_gp=$ENV{GENOMIX_GP};  
$chromo = "0";
$newgxdb = "no";		#variable for using gxdb number 
$ARGC = @ARGV;
 
#check for appropriate number of options
if ($ARGC < 3 )
{
    print_usage();
    exit 0;
}

$newgxdb= "yes"; 
#define variable here from command line.
for ($a=0; $a<=$ARGC; $a++)
{
    if ($ARGV[$a] =~ /-i/)
      { $input = $ARGV[$a+1];
print "Input: $input\n"; }
    if ($ARGV[$a] =~ /-c/)
    {	$chromo = $ARGV[$a+1]; }	  	
    #  if ($ARGV[$a] =~ /-u/)
    #    { $newgxdb= "yes"; }
    if ($ARGV[$a] =~ /-o/)
    { $output= $ARGV[$a+1]; }	#get the output path 	
}

#find out what paths to use for the gxdb number and the outputfolder by looking into the genomix_gp configuation file
if(!$genomix_gp) {die "genomix_gp environment variable not set\n"; }
if (open(CFGFILE,"$genomix_gp/code/global/genepool.cfg"))
{
    while ($line=<CFGFILE>)
    {
        if ($line =~ /^gpgxdbnumfile/)
	{
	    @tmparray = split("=",$line);
	    $gxdbbase = $tmparray[1];
	    chomp($gxdbbase);
        print "gxdbbase:  $gxdbbase\n";

	}
    }
}
close(CFGFILE);           

$basedir = $ARGV[2];    #the base subdirectory from the -i option

if ($newgxdb eq "yes")		#get the next available number from the genomix config file
{	
    if (open(GDBNUM, "< $gxdbbase"))            #open GDB number file for input
    {
	#    print "input GDBNUM opened \n";
    }
    else
    {
	print "Error opening GDBNUM $gxdbbase file\n";
    }
    #get the next GXDB number
    @gdbnumber=<GDBNUM>;
    $nucleicnum= $gdbnumber[0];           #GXDB number for the nucleic file
    $proteinnum= $gdbnumber[0];            #GXDB number for the protein file
}
if ($newgxdb eq "no")	#start the GXDB number with 0 it will incremented by 1
{
    $nucleicnum= 0;           #GXDB number for the nucleic file
    $proteinnum= 0;            #GXDB number for the protein file  
}

#@inputarray = split(/\//,$input);
#$inputlen = @inputarray;     
#@arraycontig = split(/\./,$inputarray[$inputlen-1]); 		     
#@arraynum = split(/\_/,$arraycontig[1]);   	#


#$newname = $arraycontig[0];
#$newname .= ".";
#$newname.=  $arraynum[0];       #contig number

$newname = $input;
print "newname: $newname\n";
$newname =~ s/\.out$//g;
print "$newname newname\n";
$arraycontig[0]=$newname;
$arraynum[0]=$newname;







if (open(INFILE,"< $input"))     #open input file
{
    #   print "input file opened $input\n";
}
else
{
    print "Error opening $input\n";
    exit 1;
}

$protfile =$output;
$protfile.="/";	
$protfile.="$newname";
$protfile.=".prot";
if (open(OUTPROT,"> $protfile"))
{ 
    print "output file $protfile opened\n";
}
else
{
    print "Error opening $protfile\n";
    exit 1;
}	

$mrnafile =$output;
$mrnafile.="/";
$mrnafile.="$newname";
$mrnafile.=".mrna";
if (open(OUTMRNA,"> $mrnafile"))
{
    print "output file $mrnafile opened\n";
}
else
{
    print "Error opening $mrnafile\n";
    exit 1;
}                 


#do while there is input
while ($pass=<INFILE>)
{
    if ($pass =~ /begin genes/)   #find the words 'begin genes'
    {
	while ($pass=<INFILE>)
	{
	    #look for the summary information then find strand, location, codon begin and end & length of MNRA
	    if ($pass=~/summary/)
	    {
		@arraysummary=split(/\ /,$pass);
		$strand = $arraysummary[4];		#Strand
		$location = $arraysummary[6];
		$location .="...";
		$location.= $arraysummary[7];       #gene begin and end			    
		if ($arraysummary[8] eq "-1")
		{$coding = "undetermined";
	     }
		else
		{$coding = $arraysummary[8];	#codon begin	
	     }
		$coding .= "...";
		if ($arraysummary[9] eq "-1")
		{$coding .= "undetermined"; 	
	     }
		else
		{$coding .= $arraysummary[9];	#codon end added to the codon beginning		
	     }
		
		#if ($strand eq "f")
		#{
		 #   $strand ="forward";
	#	}
#		if ($strand eq "r")
#		{
#		    $strand = "reverse";
#		}
		$length =($arraysummary[7] -$arraysummary[6])+1;
	    }
	    
	    #look for the mrna information than place it into a fasta file format
	    if ($pass=~/mrna/)
	    {
		$nucleicnum++;                    #increment the GBD number by one
		&stuff("$nucleicnum", "8");       #call the stuff subroutine stuff zeros in front of the number
		@array=split(/\ /,$pass);         #split the line by spaces
		
		@arrayhold=split(//,$array[4]);   #split into characters
		$arraylen=@arrayhold;             #Find out the length of the array
		$length = $arraylen-1;		  #get the character count of the sequence - 1	
		$stop=0;                          #holder variable
		if ($chromo eq "0")	#chromosome number was not given
		{
		    print OUTMRNA ">GXDB$num contig=$newname predictor=grailexp_3.5 gene=$array[1] variant=$array[2] strand=$strand location=$location coding=$coding length=$length nt\n"; 
		}
		else			#chromosome number was given
		{	
		    print OUTMRNA ">GXDB$num chr=$chromo seq=$newname gene=$array[1] var=$array[2] strand=$strand loc=$location cod=$coding len=$length\n";
		}

		#print 60 characters per line print the sequence
		for ($a = 0; $a<=$arraylen; ++$a)
		{
		    chomp($arrayhold[$a]);          #remove the trailing newline
		    @arraytest=split(/\ /,$arrayhold[$a]);  #split by spaces

		    if ($arraytest[0] ne "")                 #don't print blank characters
		    {
			$big = uc($arrayhold[$a]);	  #print in upper case only
			print OUTMRNA "$big";
			$stop++;
		    }
		    if ($stop eq "60") #Only put 60 characters on a line
		    {
			print OUTMRNA "\n";
			$stop=0;
		    }
		}
		
		#only print the \n if stop is greater than 0
		if ($stop > "0")                                  
		{
		    print OUTMRNA "\n";
		}
	    }
	    
	    #look for the translation information than place it into a fasta file format
	    if ($pass=~/protein/)
	    {
		$proteinnum++;                   #increment the count be one
		&stuff("$proteinnum","8");        #call the stuff subroutine stuff zeros in front of the number
		#print "PASS: $pass\n";
		@array=split(" ",$pass);         #split the line by spaces
		#for ($rszzz =0; $rszzz <= 5; $rszzz++)
		#{  
		#    print "PASS: array[$rszzz]: $array[$rszzz]\n";
		#}
		@arrayhold=split(//,$array[3]);   #split the sequence into
		#characters
		#------------------------
		#get rid of the '*' character do not print it
		
		$holder=pop(@arrayhold);  #holder for '*'
		$holder=pop(@arrayhold);
		if ($holder eq '*')
		{
		    #do nothing
		}
		elsif ($holder ne '*')
		{  push(@arrayhold,$holder);
	       }                
		
		$arraylen=@arrayhold;     #find out the length of the array
		$lenhold=$arraylen; 
		if ($chromo eq "0")	#chromosome number was not given
		{
		    print OUTPROT ">GXDB$num contig=$newname predictor=exp5 gene=$array[0] variant=$array[1] strand=$strand location=$location coding=$coding length=$lenhold aa\n";  
		}
		else			#chromosome number was given
		{
                print OUTPROT ">GXDB$num chr=$chromo seq=$newname gene=$array[0] var=$array[1] strand=$strand loc=$location cod=$coding len=$lenhold\n";
		}
		#------------------	
		$stop=0;                          #holder variable
		#print 60 characters per line print the sequence
		
		for ($a = 0; $a<=$arraylen; ++$a)
		{
		    chomp($arrayhold[$a]);          #remove the trailing newline
		    @arraytest=split(/\ /,$arrayhold[$a]);  #split by spaces
		    
		    if ($arraytest[0] ne "")                 #don't print blank characters
		    {   
			$big = uc($arrayhold[$a]);	 #print in upper case only
			print OUTPROT "$big";  
			$stop++;
		    }
		    
		    if ($stop eq "60")
		    {
			print OUTPROT "\n";
			$stop=0;
		    }
		}
		if ($stop > "0")
		{
		    print OUTPROT "\n";
		    
		}
	    }
	}
    }
             }

if ($newgxdb eq "yes") 
{
    close(GDBNUM);
    if (open(GDBNUM, "> $gxdbbase"))            #open GDB number file for output
    {
#          print "input GDBNUM opened \n";
    }
    else
    {
	print "Error opening GDBNUM file\n";
	exit 1;
    }
    print GDBNUM  "$proteinnum";
}


###############################
#Subroutines start here       #
###############################


sub print_usage()
{
    print "========================================================\n";
    print "exp2fasta  (c)Copyright 2001, by Genomix Corporation\n";
    print "========================================================\n";
    print "Usage: $0 -i inputfile - c chromosome number -o output path \n";
    print <<  "end_tag";
  Options:
    
    -i		file name (EXP3 or EXP5 output file)
	
	-c		chromosome number
	    
	    -o		output path
example:
		exp2fasta -c 3 -i /in/inpath/NT_0011878.7_exp.out -o /in/path/outputpath
end_tag
		    }


#beginning of stuff subroutine
sub stuff
{
#------------------------------------------------------------------------------
#This subroutine will return a value with 8 zeros stuffed in front of the argument.
#example:  If 1 is sent into the subroutine then 000000001 is returned.  If 10 is sent into the subroutine
#then 000000010 is returned; If 100 is sent in then 000000100 is returned
#-------------------------------------------------------------------------------
    
    $newnum="$_[0]";       #1st argument
    $cnt = "$_[1]";        #2nd argument
    
    #place the zeros in front of the number
 if ($newnum <10)
 {      $test1="";
	for ($count=1; $count <=$cnt; $count++)
	{
	    $test1.="0";
	}
        $test1.=$newnum;
        $num=$test1;
	
    }
    if ($newnum >=10 &&  $newnum<100)
    {        $test2="";
	     $cnt=$cnt-1;
	     for ($count2=1; $count2 <=$cnt; $count2++)
	     {
		 $test2.="0";
	     }
	     
	     $test2.=$newnum;
	     $num=$test2;
	 }
    if ($newnum >= 100 && $newnum < 1000)
    {        $test3="";
	     $cnt=$cnt-2;
	     for ($count3=1; $count3 <=$cnt; $count3++)
	     {
		 $test3.="0";
	     }
	     $test3.=$newnum;
	     $num=$test3;
	     
	 }
    if ($newnum >= 1000 && $newnum < 10000)
    {       $test4="";
	    $cnt=$cnt-3;
	    for ($count4=1; $count4 <=$cnt; $count4++)
	    {
		$test4.="0";
	    }
	    $test4.=$newnum;
	    $num=$test4;
	    
	}
    if ($newnum >= 10000 && $newnum < 100000)
    {
	$test5="";
	$cnt=$cnt-4;
        for ($count5=1; $count5 <=$cnt; $count5++)
	{
	    $test5.="0";
        }
	$test5.=$newnum;
	$num=$test5;
    }
    if ($newnum >= 100000 &&  $newnum< 1000000)
    {
	$test6="";
	$cnt=$cnt-5;
        for ($count6=1; $count6 <=$cnt; $count6++)
	{
	    $test6.="0";
        }
	$test6.=$newnum;
	$num=$test6;
    }
    if ($newnum >= 1000000 && $newnum < 10000000)
    {
        $test7="";
	$cnt=$cnt-6;
        for ($count7=1; $count7 <=$cnt; $count7++)
	{
	    $test7.="0";
        }
	$test7.=$newnum;
       $num=$test7;
    }
    if ($newnum >= 10000000 && $newnum < 100000000)
    {
	$test8="";
	$cnt=$cnt-7;
        for ($count8=1; $count8 <=$cnt; $count8++)
	{
	    $test8.="0";
        }
	$test8.=$newnum;
       $num=$test8;
    }
    if ($newnum >= 100000000 && $newnum < 1000000000)
    {
	$num="$newnum";
    }
    return($num);
}
