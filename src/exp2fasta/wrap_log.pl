#!/usr/bin/perl
# Script name: wrap_log.pl -i [basesubdirectory]
# example:     perl wrap_log.pl -i /data/GenomixDB/build_003/contigs/CHR_01 
# Date:        11/19/2001
# Programmer:  Mary E. Perkins
#-----------------------------------------
 
$genomixdb=$ENV{GENOMIXDB};
$ARGC = @ARGV;
 
#check for appropriate number of options
if ($ARGC < 2 )
  {
   print_usage();
    exit 1;
  }

#create a new error.list open file and close file
if (open(ERRORLOG,"> error.list"))     #open output file
   {
 #      print "error.list file opened \n";
   }
else
   {
      print "Error opening error.list\n";
      exit 0;
   }      

close(ERRORLOG);


$type = "";
#define variable here from command line.
for ($a=0; $a<=$ARGC; $a++)
  {
    if ($ARGV[$a] =~ /-i/)
      { $basedir = $ARGV[$a+1];
	$type = "ONE"; }
    if ($ARGV[$a] =~ /-r/) 
      { $basedir = $ARGV[$a+1];
        $type = "TWO"; } 
    if ($ARGV[$a] =~ /-l/)
      { $filelist = $ARGV[$a+1];
        $type = "THREE"; }  

  }

if ($type eq "TWO") 		#-r flag was chosen
{
  for ($chromocount=25;$chromocount >= 1; $chromocount--)
  {
	if ($chromocount <= 9)
        {
           	$chromo = "0$chromocount";
        }   
	elsif ($chromocount <= 22)
	{
        	$chromo = $chromocount;
	}
        if ($chromocount ==23)
        { $chromo = "Un";}
        if ($chromocount == 24)
        { $chromo = "X";}
        if ($chromocount == 25)
        { $chromo = "Y";}
 
        $folder = $basedir;
        $folder .="/";
	
        $folder .="CHR_$chromo";
        print "Processing  CHR_$chromo\n";
        #open the subdirectory and find out how many files are there
        #place the files into an array named @dirlist
        opendir(MYDIR,$folder);
        @dirlist=readdir(MYDIR);
        closedir(MYDIR);
        $numfiles = @dirlist;   #number of files in the subdirectory 
	for ($count =2; $count < $numfiles; $count++)
        {
		#get the folder and file name for input
		$filename=$folder;
		$filename.="/";
		$filename.=$dirlist[$count];  
                @arrayfilename=split(/\./,$dirlist[$count]);          
 
	        $holder=$arrayfilename[2];
		if ($holder eq "out")
        	{
		       $rc = 0xffff & system("exp2fastalog.pl -c $chromo -i $filename");
		       if ($rc == 0)
		        {print "exp2fastalog.pl failed\n";}
		        else
		        {print "exp2fastalog.pl passed\n";}
        	}	
	}     #for $count loop                  
  }
}



if ($type eq "ONE") 		#-i flag was used only do one Chomosome
{                                          
@arraychromo=split(/\//,$basedir);
$testlen=@arraychromo;
$cnt=$testlen-1;
$chromo=$arraychromo[$cnt];
@arraychromo=split(/\_/,$chromo);
$chromo=$arraychromo[1];          #get the chromosome number
 
#get the file folder
$folder = $basedir;
 
#open the subdirectory and find out how many files are there
#place the files into an array named @dirlist
opendir(MYDIR,$folder);
@dirlist=readdir(MYDIR);
closedir(MYDIR);
$numfiles = @dirlist;   #number of files in the subdirectory  
#loop thru all files in the directory
for ($count =2; $count < $numfiles; $count++)
{
    #get the folder and file name for input
    $filename=$folder;
    $filename.="/";
    $filename.=$dirlist[$count];
    @arrayfilename=split(/\./,$dirlist[$count]);
    $holder=$arrayfilename[2];
 if ($holder eq "out")
        {
	print "chromo/filename = $chromo $filename\n";
       $rc = 0xffff & system("exp2fastalog.pl -c $chromo -i $filename"); 
	print "after system call\n";
       if ($rc == 0)
        {print "exp2fastalog.pl failed\n";}
        else
        {print "exp2fastalog.pl passed\n";}
        }
}     #for $count loop
}

if ($type eq "THREE") 
{
   if (open(FILELIST,"< $filelist "))     #open output file
   {
      print "$filelist  opened \n";
   }
   else
   {
      print "Error opening $filelist\n";
      exit 0;
   }
 
   while ($line=<FILELIST>)
   {
	@arraychromo=split(/\//,$line);
	$testlen=@arraychromo;
	$cnt=$testlen-1;
	$chromo=$arraychromo[$cnt];

	@arraychromo=split(/\_/,$chromo);
	$chromo=$arraychromo[1];          #get the chromosome number
 
	#get the file folder
	$folder = $line;
	chomp($folder);
	#open the subdirectory and find out how many files are there
	#place the files into an array named @dirlist
	opendir(MYDIR,$folder);
	@dirlist=readdir(MYDIR);
	closedir(MYDIR);
	$numfiles = @dirlist;   #number of files in the subdirectory
	#loop thru all files in the directory
	for ($count =2; $count < $numfiles; $count++)
	{
	    #get the folder and file name for input
	    $filename=$folder;
	    $filename.="/";
	    $filename.=$dirlist[$count];
 
	    @arrayfilename=split(/\./,$dirlist[$count]);
 
	    $holder=$arrayfilename[2];                  	
	    if ($holder eq "out")
	    {
 
	       $rc = 0xffff & system("exp2fastalog.pl -i $filename -c $chromo");
       		if ($rc == 0)
        	{print "exp2fastalog.pl failed\n";}
        	else
        	{print "exp2fastalog.pl passed\n";}
            }
 
        }     #for $count loop  
   }
 
}
 
                                              
###############################
#Subroutines start here       #
###############################
 
 
sub print_usage()
  {
    print "========================================================\n";
    print "wrap_log  (c)Copyright 2001, by Genomix Corporation\n";
    print "========================================================\n";
    print "Usage: $0  -i  base subdirectory to start in \n";
        print <<  "end_tag";
Options:

Note: You can only use one option at a time.
 
-i              base subdirectory to start from (must end with a chromosome number one subdirectory only)

-r		base subdirectory to start from (one up from Chromosomes)

-l		file name (enter all subdirectory one line at a time in this file)

You will get a file named error.list.
 
example:
        wrap_log.pl -i /in/path/CHR_01 
	wrap_log.pl -r /in/path
	wrap_log.pl -l file.list
end_tag

  }
 
 
