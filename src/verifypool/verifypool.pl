#!/usr/bin/perl

$infile = $ARGV[0];
$splitfile = "split.lst";
$masterfile = "master.lst";


#seperate the filelist into split and non-split categories.
open(INFILE,"<$infile");
open(SPLITFILE,">$splitfile");
open(MASTERFILE,">$masterfile");

while ($line=<INFILE>)
{
    @tmparray = split(/\./,$line);
    if ($tmparray[2] !~ /mfa/)
    {
	print SPLITFILE $line;
    }
    else
    {
	print MASTERFILE $line;
    }
}


close(INFILE);
close(MASTERFILE);
close(SPLITFILE);

#now lets load the master file into a hash table.
open(MASTERFILE,"<master.lst");

while ($line = <MASTERFILE>)
{
    chomp($line);
    $line =~ s/.mfa.fsa//g;
    @tmparray = split(/\//,$line);
    push(@master_list,$tmparray[1]);
    
    
}
close(MASTERFILE);

#now do the same to the split list
open(SPLITFILE,"<$splitfile");

while ($line = <SPLITFILE>)
{
    chomp($line);
    $line=~ s/.mfa.fsa//g;
    @tmparray = split(/\//,$line);
    
    push(@split_list,$tmparray[1]);
    
}
close(MASTERFILE);



#now we need to compare the master list against the split list.
@final_list =();



for (my $a=0; $a <= $#master_list; $a++)
{
    $found =0;
    print STDERR "\n$a";
    for (my $b=0; $b <= $#split_list; $b++)
    {
	#print ".";
	if ($split_list[$b] =~ /$master_list[$a]/)
	{
	    push(@final_list,$split_list[$b]);
	    $found = 1;
	}
    }
    if ($found == 0)
    {
	push(@final_list,$master_list[$a]);
    }

}

for ($a =0; $a <= $#final_list; $a++)
{
    print "$final_list[$a]\n";


}
