#!/usr/bin/perl
use POSIX;

for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    {
	$infile =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

if ($DEBUG) { print "infile: $infile  chr_id: $chr_id\n"; }



$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }
}
# $genedb = DBI ->connect("DBI:mysql:database=$database;host=$hostname",$user,$pass,{'RaiseError' => 1});

####################
#DEFINE GLOBALS HERE
####################
@exon_num=();
@exon_left=();
@exon_right=();
@exon_active=();
@sequence_string =();
$reverse =0;
$sequence="";

# open the out.sup file, scan for Gene Variant and grab the gxdb number.
open(INFILE,"<$infile");
while ($line = <INFILE>)
{
    $reverse =0;
    if ($line =~ /^Gene/ && $line =~ /Variant/)
    {
	@tmparray = split(/\s+/,$line);
	$gene_id = $tmparray[4];
	if ($DEBUG) { print "Found gene_id: $gene_id\n"; }
	load_exons();
	load_sequence();
	check_reverse();
	if ($reverse == 1)
	{
	    reverse_exon_table();
	}
	

	output_genomic_sequence();
    }
		    
	

}

sub load_exons()
{
    $exonfile = "$gprootdir/inbound/symlinks/$chr_id/$gene_id.exons";
    if ($DEBUG) { print "Exon file: $exonfile\n"; }
    
    if (!open(EXONFILE,"<$exonfile"))
    {
	print "Could not open exonfile $exonfile\n";
	exit(1);
    }
    while ($exonline =<EXONFILE>)
    {
	#if ($DEBUG) { print "$exonline";}
	chomp($exonline);
	@tmparray = split(/\s+/,$exonline);
	push(@exon_num,$tmparray[0]);
	push(@exon_left,$tmparray[1]);
	push(@exon_right,$tmparray[2]);
	push(@exon_active,$tmparray[3]);
    }
    close(EXONFILE);

    if ($DEBUG)
    {
	print "======================================\n";
	
	for (my $a= 0; $a <= $#exon_num; $a++)
	{
	    print "$exon_num[$a] $exon_left[$a] $exon_right[$a] $exon_active[$a]\n";
	}
	print "======================================\n";
    }
}


sub load_sequence()
{
    #load the fsa seq into $sequence_string

    $fsafile = "$gbkdir/symlinks/$chr_id/$infile";
    $fsafile =~ s/.out.sup/.mfa.fsa/;
    if ($DEBUG) { print "fsafile: $fsafile\n"; }
    if (!open(FSAFILE,"<$fsafile"))
    {
	print "Could not open fsa file: $fsafile\n";
	exit(1);
    }
    
    while ($fsaline = <FSAFILE>)
    {
	if ($fsaline !~ /^>/)
	{
	    chomp($fsaline);
	    $sequence .= uc($fsaline);
	}
    }
    @sequence_string = split("",$sequence);
    if ($DEBUG) { print "$#sequence_string base pairs loaded\n"; }
    

}


sub check_reverse()
{
    #check to see if this gene is reverse or not.  
    #if reverse, compliment it
    if ($exon_num[0] > 1)
    {
	#sequence needs complimented
	if ($DEBUG) { print "sequence needs reverse complimented\n"; }
	my $b = 0;
	
	$tmp_sequence = $sequence_string;
	#now we compliment the string
	for ($a = 0; $a <= length($tmp_sequence); $a++)
	{
	    if (uc($tmp_sequence[$a] eq "A"))
	    {
		$tmp_sequence[$a] = "T";
	    }
	   if (uc($tmp_sequence[$a] eq "C"))
	    {
		$tmp_sequence[$a] = "G";
	    }
	   if (uc($tmp_sequence[$a] eq "G"))
	    {
		$tmp_sequence[$a] = "C";
	    }
	    if (uc($tmp_sequence[$a] eq "T"))
	    {
		$tmp_sequence[$a] = "A";
	    }
	   

	} #end compliment
	$sequence_string = $tmp_sequence;
	$tmp_sequence="";
	$reverse = 1;
    }
    else
    {
	if ($DEBUG) { print "Sequence strand is forward\n"; }
    }
	
}

sub reverse_exon_table()
{
    #this routine takes the value in the exon array
    #and subtracts+1 it from the sequence size.
    $seqlen = length($sequence_string);
    for (my $a=0; $a <= $#exon_right; $a++)
    {
	$exon_left[$a] = $seqlen - $exon_left[$a]+1;
	$exon_right[$a] = $seqlen - $exon_right[$a]+1;
    }
    if ($DEBUG)
    {
	print "Reveresed exon table\n";
	for (my $a=0; $a <= $#exon_right; $a++)
	{
	    print "$exon_num[$a] $exon_left[$a] $exon_right[$a]\n";
	}

    }

}


sub output_genomic_sequence()
{

}
