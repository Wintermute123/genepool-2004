#!/usr/bin/perl

# Perl script to input a FASTA entry nucleotide sequence and output its inverse

@seq = ();
while ($line = <STDIN>)
{
  if ($line =~ /^>/) {print $line;next;}
  if ($line =~ /^\n$/) {next;}

  chomp $line;
  @line = split(//, $line);

  push @seq, @line;
}

$ncount = 0;
for ($i = scalar(@seq) - 1; $i >=0; $i--)
{
  if (($ncount != 0) && ($ncount%60 == 0)) {print "\n";}

  if ($seq[$i] eq "a") {print "t";}
  elsif ($seq[$i] eq "A") {print "T";}
  elsif ($seq[$i] eq "c") {print "g";}
  elsif ($seq[$i] eq "C") {print "G";}
  elsif ($seq[$i] eq "g") {print "c";}
  elsif ($seq[$i] eq "G") {print "C";}
  elsif ($seq[$i] eq "t") {print "a";}
  elsif ($seq[$i] eq "T") {print "A";}
  else {print $seq[$i];}

  $ncount++;
}
print "\n";
