#!/usr/bin/perl

# Genomix Corporation
# July 24, 2002
# This program inputs a sup file, contig, and gene number and outputs an R (relevant) sequence
# for that particular gene.  The R sequence is a concatenation of subsequences from the contig
# in order, of course.  The subsequences are the exons corresponding to the gene with an extra
# number of bps, a window, around the exons.  Thus, we produce a very small genomic sequence
# that contains all of the relevant portions relating to the gene.  This sequence is then ideal
# for quickly producing genomic alignments with ours or any other alignment tool.

# Window sizes
$EXON_WINDOW = 100;
$GENE_WINDOW = 1000;

if ($GENE_WINDOW < $EXON_WINDOW)
{
  print STDERR "Error - gene window size cannot be smaller than exon size!\n";
  exit;
}

if (scalar(@ARGV) == 0)
{
  print STDERR "Usage:  rseq.pl -supfile <sup file> -seqfile <sequence file> -gene <gene number> -var <optional variant number> -rseqfile <optional R sequence output file> -exonfile <optional exon list output file>\n";
  exit;
}

# Read in command-line arguments
$sup_file = "";
$seq_file = "";
$gene_number = -1;
$var_number = -1;
$rseq_file = "";
$exon_file = "";
for (my $i = 0; $i < scalar(@ARGV) - 1; $i++)
{
  if ($ARGV[$i] eq "-supfile") {$sup_file = $ARGV[$i+1];}
  if ($ARGV[$i] eq "-seqfile") {$seq_file = $ARGV[$i+1];}
  if ($ARGV[$i] eq "-gene") {$gene_number = $ARGV[$i+1];}
  if ($ARGV[$i] eq "-var") {$var_number = $ARGV[$i+1];}
  if ($ARGV[$i] eq "-rseqfile") {$rseq_file = $ARGV[$i+1];}
  if ($ARGV[$i] eq "-exonfile") {$exon_file = $ARGV[$i+1];}
}

my $ERROR_FLAG = 0;
if ($sup_file eq "")
{
  print STDERR "Error - a sup file must be provided (-supfile argument)\n";
  $ERROR_FLAG = 1;
}

if ($seq_file eq "")
{
  print STDERR "Error - a sequence file must be provided (-seqfile argument)\n";
  $ERROR_FLAG = 1;
}

if ($gene_number == -1)
{
  print STDERR "Error - a gene number must be provided (-gene argument)\n";
  $ERROR_FLAG = 1;
}

if ($ERROR_FLAG == 1) {exit;}

# First, read in the sequence file
$seq = " ";
open(SEQ, "< $seq_file") or die "Unable to open sequence file $seq_file\n$!\n";
while ($line = <SEQ>)
{
  chomp $line;
  if ($line !~ /^>/) {$seq .= $line;}
}
close SEQ;

# Open the sup file and find the gene.
open(SUP, "< $sup_file") or die "Unable to open sup file $sup_file\n$!\n";

if ($var_number == -1)
{
  while (($line = <SUP>) && ($line !~ /^Gene $gene_number Variant/)) {}
  if ($line !~ /^Gene $gene_number Variant/)
  {
    print STDERR "Error - unable to find gene $gene_number in file $sup_file\n";
    exit;
  }
}

else
{
  while (($line = <SUP>) && ($line !~ /^Gene $gene_number Variant $var_number/)) {}
  if ($line !~ /^Gene $gene_number Variant $var_number/)
  {
    print STDERR "Error - unable to find gene $gene_number variant $var_number in file $sup_file\n";
    exit;
  }
}

# Now, read in the exons
$strand = 0;
@lefts = ();
@rights = ();
@exonlefts = ();
@exonrights = ();
while (($line = <SUP>) && ((($var_number == -1) && ($line !~ /^Gene.*Variant 1/))
                       ||  (($var_number != -1) && ($line !~ /^Gene.*Variant/  ))))
{
  if ($line =~ /^exon:/)
  {
    my @parts = split(/\s+/, $line);
    if ($parts[1] > $parts[2]) {$strand = 1;$origleft = $parts[2]; $origright = $parts[1];}
    else {$origleft = $parts[1]; $origright = $parts[2];}

    $newleft = $origleft;
    $newright = $origright;
    # Store the original exon coordinates before going further - we need them for the exon list
    
    # See if the new interval overlaps any interval we already have
    my @overlaps = ();
    for (my $i = 0; $i < scalar(@exonlefts); $i++)
    {
      if (($newleft <= $exonrights[$i]) && ($newright >= $exonlefts[$i]))
      {
        push @overlaps, $i;
        if ($exonlefts[$i] < $newleft) {$newleft = $exonlefts[$i];}
        if ($exonrights[$i] > $newright) {$newright = $exonrights[$i];}
      }
    }

    # Remove the intervals that overlap and add a new one
    my $num_spliced_so_far = 0;
    foreach my $ol (@overlaps)
    {
      splice(@exonlefts, $ol - $num_spliced_so_far, 1);
      splice(@exonrights, $ol - $num_spliced_so_far, 1);
      $num_spliced_so_far++;
    }
    push @exonlefts, $newleft;
    push @exonrights, $newright;

    # Now, convert coordinates to the intervals that we actually want and repeat the above for
    # these new coordinates.
    $newleft  = $origleft - $EXON_WINDOW; $newright = $origright + $EXON_WINDOW;

    if ($newleft < 1) {$newleft = 1;}
    if ($newright > length($seq) - 1) {$newright = length($seq) - 1;}

    # See if the new interval overlaps any interval we already have
    my @overlaps = ();
    for (my $i = 0; $i < scalar(@lefts); $i++)
    {
      if (($newleft <= $rights[$i]) && ($newright >= $lefts[$i]))
      {
        push @overlaps, $i;
        if ($lefts[$i] < $newleft) {$newleft = $lefts[$i];}
        if ($rights[$i] > $newright) {$newright = $rights[$i];}
      }
    }

    # Remove the intervals that overlap and add a new one
    my $num_spliced_so_far = 0;
    foreach my $ol (@overlaps)
    {
      splice(@lefts, $ol - $num_spliced_so_far, 1);
      splice(@rights, $ol - $num_spliced_so_far, 1);
      $num_spliced_so_far++;
    }
    push @lefts, $newleft;
    push @rights, $newright;
  }
}
close SUP;

if (scalar(@lefts) == 0)
{
  print STDERR "Error - no exons found for gene $gene_number\n";
  exit;
}

# Sort the intervals
@exonlefts = sort {$a <=> $b} @exonlefts;
@exonrights = sort {$a <=> $b} @exonrights;
@lefts = sort {$a <=> $b} @lefts;
@rights = sort {$a <=> $b} @rights;

# Finally, create the R sequence
$rseq = "";

# First, add some bps before the beginning of the gene
if ($lefts[0] - $GENE_WINDOW + $EXON_WINDOW > 1)
{
  $rseq .= substr($seq, $lefts[0] - $GENE_WINDOW + $EXON_WINDOW, $GENE_WINDOW - $EXON_WINDOW);
}
else {$rseq .= substr($seq, 1, $lefts[0] - 1);}

# Append each interval
for (my $i = 0; $i < scalar(@lefts); $i++)
{
  $rseq .= substr($seq, $lefts[$i], $rights[$i] - $lefts[$i] + 1);
}

# Lastly, append bps after the end of the gene
if ($rights[scalar(@rights)-1] + $GENE_WINDOW - $EXON_WINDOW < length($seq) - 1)
{
  $rseq .= substr($seq, $rights[scalar(@rights)-1] + 1, $GENE_WINDOW - $EXON_WINDOW);
}
else {$rseq .= substr($seq, $rights[scalar(@rights)-1] + 1);}


# We have R sequence, now compute exon list

# Start by converting original exon coordinates to their R sequence coordinates
@exonLengths = ();
for(my $i = 0; $i < scalar(@exonlefts); $i++)
{
  push @exonLengths, $exonrights[$i] - $exonlefts[$i] + 1;
}

$exonlefts[0] = $GENE_WINDOW + 1;
for(my $i = 1; $i < scalar(@exonlefts); $i++)
{
  my $intronLength  = $exonlefts[$i] - $exonrights[$i-1] - 1;

  $exonrights[$i-1] = $exonlefts[$i-1] + $exonLengths[$i-1] - 1;

  # Length of the new intron is always $EXON_WINDOW*2 or the original length if it is smaller
  if ($intronLength <= $EXON_WINDOW*2) {$exonlefts[$i] = $exonrights[$i-1] + $intronLength + 1;}
  else {$exonlefts[$i] = $exonrights[$i-1] + $EXON_WINDOW*2 + 1;}
}
$exonrights[scalar(@exonrights)-1] = $exonlefts[scalar(@exonrights)-1] +
                                                $exonLengths[scalar(@exonrights)-1] - 1;

# If the sequence is on the reverse strand - reverse and complement the sequence and exon list
# before continuing
$rseq = uc($rseq);
@rseq = split(//, $rseq);
if ($strand == 1)
{
  invert(\@rseq);

  # Now, invert the exon lists

  # First, swap the left and right arrays
  @tmpExonArray = @exonlefts;
  @exonlefts = @exonrights;
  @exonrights = @tmpExonArray;

  # Then, reverse the arrays
  @exonlefts = reverse @exonlefts;
  @exonrights = reverse @exonrights;

  # Finally, invert the coordinates themselves
  for (my $i = 0; $i < scalar(@exonlefts); $i++)
  {
    $exonlefts[$i] = scalar(@rseq) - $exonlefts[$i] + 1;
    $exonrights[$i] = scalar(@rseq) - $exonrights[$i] + 1;
  }
}

# Print R sequence in FASTA format
if ($rseq_file ne "")
{
  open(RSEQ, "> $rseq_file") or die "Unable to open R sequence output file $rseq_file\n$!\n";
  open(STDOUT, ">& RSEQ");
}

print ">R sequence for gene $gene_number\n";
print $rseq[0];
for (my $i = 1; $i < scalar(@rseq); $i++)
{
  if ($i % 60 == 0) {print "\n";}
  print $rseq[$i];
}
if ($rseq_file eq "") {print "\n";}
else {close RSEQ;}

# Finally, print the exon list
if ($exon_file ne "")
{
  open(EXON, "> $exon_file") or die "Unable to open exon list output file $exon_file\n$!\n";
  open(STDOUT, ">& EXON");
}

for (my $i = 0; $i < scalar(@exonlefts); $i++) {print "$exonlefts[$i] $exonrights[$i]\n";}
if ($exon_file ne "") {close EXON;}

# This function simply inverts a given sequence
sub invert()
{
  my $seq = $_[0];

  @{$seq} = reverse @{$seq};

  for (my $i = 0; $i < scalar(@{$seq}); $i++)
  {
    if (${$seq}[$i] eq "A") {${$seq}[$i] = "T";}
    elsif (${$seq}[$i] eq "T") {${$seq}[$i] = "A";}
    elsif (${$seq}[$i] eq "C") {${$seq}[$i] = "G";}
    elsif (${$seq}[$i] eq "G") {${$seq}[$i] = "C";}
    elsif (${$seq}[$i] eq "a") {${$seq}[$i] = "t";}
    elsif (${$seq}[$i] eq "t") {${$seq}[$i] = "a";}
    elsif (${$seq}[$i] eq "c") {${$seq}[$i] = "g";}
    elsif (${$seq}[$i] eq "g") {${$seq}[$i] = "c";}
  }
}
