#!/usr/bin/perl

# Genomix Corporation
# July 31, 2002
# This code inputs a raw genomic alignment where the first line is the genomic sequence, the
# second line is the mRNA, and the rest are ESTs, and converts it to an mRNA alignment.

# First read in all the lines
while (my $line = <STDIN>)
{
  chomp $line;
  my @parts = split(/\s+/, $line);
  push @aligns, $parts[0];
  push @accs, $parts[1];
}

# Now, compress sequences by deleting bps that lie only on the genomic sequence
for (my $i = 0; $i < length($aligns[1]); $i++)
{
  my $DASH_OKAY = 0;
  for (my $j = 1; $j < scalar(@aligns); $j++)
  {
    my $letter = substr($aligns[$j], $i, 1);
    if (($letter ne "-") && ($letter ne ".")) {$DASH_OKAY = 1; break;}
  }

  if ($DASH_OKAY == 0)
  {
    for (my $j = 1; $j < scalar(@aligns); $j++) {substr($aligns[$j], $i, 1) = "";}
    $i--;
  }
}

# Finally, print all lines except the top genomic line
for (my $i = 1; $i < scalar(@aligns); $i++) {print "$aligns[$i] $accs[$i]\n";}
