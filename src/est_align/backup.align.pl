#!/usr/bin/perl

# Genomix Corporation
# August 1, 2002
# This code wrappers the alignment tools for use with GenePOOL.
# Input:  a SUP file, contig file, gene, variant, and dblist.
# Output:  mRNA alignment against that gene and variant using the dblist
# Note:  Since the dblist is expected to be small and user-defined, the
# constraints should be set to bare minimum

$tmp_dir = "/tmp";

if (scalar(@ARGV) != 5)
{
  print STDERR "Usage:  <SUP file> <sequence> <gene number> <variant number> <database list>\n";
  exit;
}

$sup_file = $ARGV[0];
$seq_file = $ARGV[1];
$gene_num = $ARGV[2];
$var_num = $ARGV[3];
$db_file = $ARGV[4];

# Name the tmp files to be used
$rseq_file = "$tmp_dir/al.rseq.$$";
$exon_file = "$tmp_dir/al.exon.$$";
$galign_file = "$tmp_dir/al.galign.$$";
$malign_file = "$tmp_dir/al.malign.$$";

system("rseq.pl -supfile $sup_file -seqfile $seq_file -gene $gene_num -var $var_num -rseqfile $rseq_file -exonfile $exon_file");

system("galign.pl $rseq_file $db_file $exon_file > $galign_file");

system("malign.pl < $galign_file > $malign_file");

system("format.pl < $malign_file");
