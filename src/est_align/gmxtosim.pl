#!/usr/bin/perl

while ($line = <STDIN>)
{
  chomp $line;
  if ($line =~ /^>/) {$line =~ s/(,|\s)+/_/g;}
  print "$line\n";
}
