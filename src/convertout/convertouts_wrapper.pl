#!/usr/bin/perl

for ($a=17;$a <= 25; $a++)
{
    open(OUTFILE,">$a.test.csh");
    
    print OUTFILE "cd /genepool/inbound/symlinks/$a\n";
    print OUTFILE "foreach k (*.out)\n";
    print OUTFILE "perl ../../src/convertout/convertout.pl -i \$k -c ";
    print OUTFILE $a." -o ./ \n";
    print OUTFILE "end\n";
    print OUTFILE "cd /genepool/inbound\n";
    close(OUTFILE);
    print "$a running....\n";
    system("csh $a.test.csh");
    system("rm -f -r $a.test.csh");


}
