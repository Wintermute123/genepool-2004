#!/usr/bin/perl
#
# usage: convertout -i <infile> -c <chr #> -o <outdir> (-D)


for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    {
	$infile = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-o/)
    {
	$outdir = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

if ($DEBUG) { print "infile: $infile outfile: $outdir\n"; }





$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
     if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
	if ($DEBUG) { print "gpbin: $gpbin \n";}
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }

}#end of process CFG

if ($DEBUG) { print "1 $gprootdir 2 $gpdatadir 3 $gpbuilddir 4 $gpsrcdir 5 $gbkdir 6 $gpbin\n"; }

close(TMPFILE);

#ok we need to reun exp2allfasta first to create
#the MRNA, PROT files for the contigs.  exp2allfasta
#also assigns the GXDB number
#
#usage: exp2allfasta -i <contig.out> -c <chr #> -o <output folder>

$syscmd = "$gpbin/exp2allfasta -i $infile -c $chr_id -o $outdir";
if ($DEBUG) { print "$syscmd\n"; } 
$rc =0xffff & system($syscmd);
if ($rc != 0) { die "execution of $syscmd failed"; }
#exit;
#ok now we need to run expsup on it.
#
#usage:  sup.pl <EXP input file> <optional SNP input file> <output file>

$supout = $outdir."/$infile.sup";
$gbkfile = $gbkdir."/symlinks/".$chr_id."/".$infile;
$gbkfile =~ s/\.out/\.gbk.snp/g;

$syscmd = "$gpbin/expsup $infile $gbkfile $supout";  
print "CONVERTOUT $syscmd\n";
if ($DEBUG) { print "$syscmd\n"; } 
$rc =0xffff & system($syscmd);
if ($rc != 0) { die "execution of $syscmd failed"; }

#run update_sup to place GXDB nums into file
$syscmd = "$gpbin/update_sup $supout ";  
if ($DEBUG) { print "$syscmd\n"; } 
$rc =0xffff & system($syscmd);
if ($rc != 0) { die "execution of $syscmd failed"; }

$syscmd = "rm -f -r $supout";
system($syscmd);

$newfile = $supout;
$newfile =~ s/\.sup/\.sup\.gp/g;

$syscmd = "mv $newfile $supout";
system($syscmd);
print "$syscmd\n";




