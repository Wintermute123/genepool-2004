#!/usr/bin/perl

#usage: extract_prot.pl NT*.prot GXDBnum

for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-s/)
    {
	$seq_id =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-n/)
    {
	$gene_id = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

if (!open(INFILE,"<$seq_id"))
{
    print "Could not open $seq_id\n";
    exit(1);
}

while ($line = <INFILE>)
{
    if ($line =~ /^>/)
    {
	$line =~ s/^>//g;
	#print $line;
	
	@tmparray = split(" ",$line);
	#print "tmparray[0] $tmparray[0]\n";
	if ( $tmparray[0] eq $gene_id)
	{
	    print ">$line";
	    while ($line = <INFILE>)
	    {
		if ($line =~ /^>/)
		{
		    close(INFILE);
		    exit(0);
		}
		else
		{
		    print $line;
		}
	    }
	}
    }
}
