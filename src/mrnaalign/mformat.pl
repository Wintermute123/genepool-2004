#!/usr/bin/perl

use POSIX;

$MAX_LABEL_SIZE = 4;

$FIRST_FLAG = 1;
while ($line = <STDIN>)
{
  chomp $line;
  my @lineParts = split(/\s+/, $line);
  push @acc, $lineParts[1];
  if ($FIRST_FLAG == 1) {$lineParts[0] = uc($lineParts[0]);}
  else {$lineParts[0] = lc($lineParts[0]);}
  my @lineArray = split(//, $lineParts[0]);
  push @lines, \@lineArray;
  $FIRST_FLAG = 0;
}

$size = scalar(@{$lines[0]});
for ($i = 0; $i < $size; $i += 60)
{
  $mrnaBegin = $i + 1;
  $mrnaEnd = $i + 60;
  if ($mrnaEnd > $size) {$mrnaEnd = $size;}
  $acc_no = 0;
  foreach $line (@lines)
  {
    @printArray = splice(@{$line}, 0, 60);
    @labelArray = split(//, $acc[$acc_no]);
    for (my $i = 0; $i < $MAX_LABEL_SIZE; $i++)
    {
      if ($i >= scalar(@labelArray)) {print " ";}
      else {print $labelArray[$i];}
    }
    print "\t";
    print @printArray;
    if ($acc_no == 0) {print "     $mrnaEnd";}
    print "\n";
    $acc_no++;
  }
  print "\n\n";
}
