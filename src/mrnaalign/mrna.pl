#!/usr/bin/perl

# Genomix Corporation
# August 2, 2002
# This program inputs a SUP file and its corresponding sequence and produces an EXACT alignment
# of the mRNAs against one another.  That is, only the actual coordinates are used and no
# outside alignment tools like blast or sim4 are needed.
# NOTE:  This code assumes that each variant begins and ends at the same contig position, as
#        will be true if processed with all current versions of EXP 6 (EXP 6.1.0 or earlier)

if (scalar(@ARGV) != 3)
{
  print STDERR "Usage:  mrna.pl <SUP file> <sequence> <gene number>\n";
  exit;
}

$sup_file = $ARGV[0];
$seq_file = $ARGV[1];
$gene_number = $ARGV[2];

# First, read in the sequence file
$seq = " ";
open(SEQ, "< $seq_file") or die "Unable to open sequence file $seq_file\n$!\n";
while ($line = <SEQ>)
{
  chomp $line;
  if ($line !~ /^>/) {$seq .= $line;}
}
close SEQ;

# Open the sup file and find the gene.
open(SUP, "< $sup_file") or die "Unable to open sup file $sup_file\n$!\n";

while (($line = <SUP>) && ($line !~ /^Gene $gene_number Variant/)) {}
if ($line !~ /^Gene $gene_number Variant/)
{
  print STDERR "Error - unable to find gene $gene_number in file $sup_file\n";
  exit;
}

# Now, read in the exons
@exonlefts = ();
@exonrights = ();
@sup_lines = ();
$strand = 0;
chomp $line;
push @sup_lines, $line;
while (($line = <SUP>) && ($line !~ /^Gene.*Variant 1/))
{
  chomp $line;
  if ($line =~ /(^exon:|^Gene $gene_number)/) {push @sup_lines, $line;}

  if ($line =~ /^exon:/)
  {
    my @parts = split(/\s+/, $line);
    if ($parts[1] > $parts[2]) {$strand = 1;$newleft = $parts[2]; $newright = $parts[1];}
    else {$newleft = $parts[1]; $newright = $parts[2];}

    # See if this interval overlaps some we already have
    my @overlaps = ();    
    for (my $i = 0; $i < scalar(@exonlefts); $i++)
    {
      if (($newleft <= $exonrights[$i]) && ($newright >= $exonlefts[$i]))
      {
        push @overlaps, $i;
        if ($exonlefts[$i] < $newleft) {$newleft = $exonlefts[$i];}
        if ($exonrights[$i] > $newright) {$newright = $exonrights[$i];}
      }
    }

    # Remove the intervals that overlap and add a new one
    my $num_spliced_so_far = 0;
    foreach my $ol (@overlaps)
    {
      splice(@exonlefts, $ol - $num_spliced_so_far, 1);
      splice(@exonrights, $ol - $num_spliced_so_far, 1);
      $num_spliced_so_far++;
    }
    push @exonlefts, $newleft;
    push @exonrights, $newright;
  }
}

@exonlefts = sort {$a <=> $b} @exonlefts;
@exonrights = sort {$a <=> $b} @exonrights;

$FIRST_FLAG = 1;
$var_number = 0;
foreach $line (@sup_lines)
{
  if ($line =~ /^Gene/)
  {
    if ($FIRST_FLAG != 1)
    {
      if ($var_number == 0) {print "$mrna Base\n";}
      else {print "$mrna Var$var_number\n";}
    }
    $mrna = "";
    $last_coord = 0;
    $var_number++;
    $FIRST_FLAG = 0;
  }

  else
  {
    @parts = split(/\s+/, $line);
    my $numDashes = ctg2mrna($parts[1]) - $last_coord - 1;
    for (my $i = 0; $i < $numDashes; $i++) {$mrna .= "-";}
    if ($strand == 1) {$mrna .= invert(substr($seq, $parts[2], $parts[1] - $parts[2] + 1));}
    else {$mrna .= substr($seq, $parts[1], $parts[2] - $parts[1] + 1);}
    $last_coord = ctg2mrna($parts[2]);
  }
}

if ($FIRST_FLAG == 0)
{
  if ($var_number == 0) {print "$mrna Base\n";}
  else {print "$mrna Var$var_number\n";}
}

# This function translates the given contig coordinate to the GLOBAL mRNA coordinate for the
# gene.  Note that it assumes @exonlefts and @exonrights arrays have already been defined
sub ctg2mrna()
{
  my $coord = $_[0];
  my $num_bps = 0;
  if ($strand == 0)
  {
    for (my $i = 0; $i < scalar(@exonlefts); $i++)
    {
      if (($coord >= $exonlefts[$i]) && ($coord <= $exonrights[$i]))
      {
        return ($num_bps + ($coord - $exonlefts[$i] + 1));
      }
      else {$num_bps += ($exonrights[$i] - $exonlefts[$i] + 1);}
    }
  }

  else
  {
    for (my $i = scalar(@exonlefts) - 1; $i >= 0; $i--)
    {
      if (($coord >= $exonlefts[$i]) && ($coord <= $exonrights[$i]))
      {
        return ($num_bps + ($exonrights[$i] - $coord + 1));
      }
      else {$num_bps += ($exonrights[$i] - $exonlefts[$i] + 1);}
    }
  }

  print STDERR "Error - contig coordinate $coord does not lie on the mRNA in ctg2mrna()\n";
  exit;
}

# This function simply reverses and complements a given string
sub invert()
{
  my @toflip = split(//, $_[0]);

  for (my $i = 0; $i < scalar(@toflip); $i++)
  {
    if ($toflip[$i] eq "A") {$toflip[$i] = "T";}
    elsif ($toflip[$i] eq "T") {$toflip[$i] = "A";}
    elsif ($toflip[$i] eq "C") {$toflip[$i] = "G";}
    elsif ($toflip[$i] eq "G") {$toflip[$i] = "C";}
    elsif ($toflip[$i] eq "a") {$toflip[$i] = "t";}
    elsif ($toflip[$i] eq "t") {$toflip[$i] = "a";}
    elsif ($toflip[$i] eq "c") {$toflip[$i] = "g";}
    elsif ($toflip[$i] eq "g") {$toflip[$i] = "c";}
  }

  return (join("", reverse @toflip));
}
