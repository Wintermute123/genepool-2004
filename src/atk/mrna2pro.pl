#!/usr/bin/perl

use POSIX;

# Start by reading in the mRNA sequences
@mrnas = ();
@labels = ();
while ($line = <STDIN>)
{
  chomp $line;
  my @parts = split(/\s+/, $line);
  push @mrnas, $parts[0];
  push @labels, $parts[1];
}

$label_number = 0;
$numDashes = 0;
$DELAY_DASH_WRITE = 0;
$PRINTED_BP_IN_LAST_LOOP = 1;
$nobp_char = ".";
foreach my $mrna (@mrnas)
{
  $codon = "";
  while ($mrna ne "")
  {
    # Shift 3 bps from mRNA to codon "register"
    $codon .= substr($mrna, 0, 3);
    substr($mrna, 0, 3) = "";

    # Delete dashes and dots
    $codon =~ s/(-|\.)//g;

    # If we have 3 bps we can print the amino acid and any stored dashes
    if (length($codon) >=3)
    {
      $nobp_char = "-";
      my $aa = trans(substr($codon, 0, 3));
      if ($aa eq "*") {$nobp_char = ".";}
      print $aa;
      if ($DELAY_DASH_WRITE == 1)
      {
        for (my $i =0; $i < $numDashes; $i++) {print $nobp_char;}
        $numDashes = 0;
        $DELAY_DASH_WRITE = 0;
      }
      substr($codon, 0, 3) = "";
      $PRINTED_BP_IN_LAST_LOOP = 1;
    }

    else
    {
      # Delay printing of dashes in this case so we can put the amino acid to the left of the
      # dashes
      if ((length($codon) == 2) && ($PRINTED_BP_IN_LAST_LOOP == 1)) {$DELAY_DASH_WRITE = 1;}

      if ($DELAY_DASH_WRITE == 1) {$numDashes++;}
      else {print $nobp_char;}

      $PRINTED_BP_IN_LAST_LOOP = 0;
    }
  }
  for (my $i =0; $i < $numDashes; $i++) {print $nobp_char;}
  print " $labels[$label_number]\n";
  $label_number++;
}

# This function translates a given codon (three-letter string) to its corresponding protein.
# Stops (tag, tga, and taa) are translated as "*" and an "X" is returned on any other condition.
# These are the same conventions as that of the EXP code.
sub trans()
{
  my $codon = tolower($_[0]);
  my @codonarray = split(//,$codon);

  if ($codon eq "ttt") {return "F";}
  if ($codon eq "ttc") {return "F";}
  if ($codon eq "tta") {return "L";}
  if ($codon eq "ttg") {return "L";}
  if ($codon eq "ctt") {return "L";}
  if ($codon eq "ctc") {return "L";}
  if ($codon eq "cta") {return "L";}
  if ($codon eq "ctg") {return "L";}
  if (($codonarray[0] eq "c") && ($codonarray[1] eq "t")) {return "L";}
  if ($codon eq "att") {return "I";}
  if ($codon eq "atc") {return "I";}
  if ($codon eq "ata") {return "I";}
  if ($codon eq "atg") {return "M";}
  if (($codonarray[0] eq "g") && ($codonarray[1] eq "t")) {return "V";}
  if (($codonarray[0] eq "t") && ($codonarray[1] eq "c")) {return "S";}
  if (($codonarray[0] eq "c") && ($codonarray[1] eq "c")) {return "P";}
  if (($codonarray[0] eq "a") && ($codonarray[1] eq "c")) {return "T";}
  if (($codonarray[0] eq "g") && ($codonarray[1] eq "c")) {return "A";}
  if ($codon eq "tat") {return "Y";}
  if ($codon eq "tac") {return "Y";}
  if ($codon eq "cat") {return "H";}
  if ($codon eq "cac") {return "H";}
  if ($codon eq "caa") {return "Q";}
  if ($codon eq "cag") {return "Q";}
  if ($codon eq "aat") {return "N";}
  if ($codon eq "aac") {return "N";}
  if ($codon eq "aaa") {return "K";}
  if ($codon eq "aag") {return "K";}
  if ($codon eq "gat") {return "D";}
  if ($codon eq "gac") {return "D";}
  if ($codon eq "gaa") {return "E";}
  if ($codon eq "gag") {return "E";}
  if ($codon eq "tgt") {return "C";}
  if ($codon eq "tgc") {return "C";}
  if ($codon eq "tgg") {return "W";}
  if (($codonarray[0] eq "c") && ($codonarray[1] eq "g")) {return "R";}
  if ($codon eq "agt") {return "S";}
  if ($codon eq "agc") {return "S";}
  if ($codon eq "aga") {return "R";}
  if ($codon eq "agg") {return "R";}
  if (($codonarray[0] eq "g") && ($codonarray[1] eq "g")) {return "G";}
  if ($codon eq "tag") {return "*";}
  if ($codon eq "tga") {return "*";}
  if ($codon eq "taa") {return "*";}
  return "X";
}
