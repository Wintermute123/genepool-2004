#!/usr/bin/perl

# Genomix Corporation
# July 17, 2002
# Usage:  galign.pl <relevant (R) sequence> <database list> <optional exon list>
# Updated version of our previous align utility.  Now we use sim4 instead of blast and an r
# sequence instead of an mRNA sequence to allow for greater accuracy.  Another utility is needed
# to process the resulting R sequence alignments to mRNA alignments

$exonFile = "";
if ((scalar(@ARGV) != 2) && (scalar(@ARGV) != 3))
{
  print "Usage:  galign.pl <relevant (r) sequence> <database list file> <optional exon list>\n";
  exit;
}

$rseqFile = $ARGV[0];
$dbListFile = $ARGV[1];
if (scalar(@ARGV) >= 3) {$exonFile = $ARGV[2];}

$MIN_ALIGN_LENGTH = 20;

# Read in the database list
open(DBLIST, "< $dbListFile") or die "Unable to open database list file $dbListFile:  $!\n";
$numDbs = 0;
while (my $file = <DBLIST>)
{
  chomp $file;
  $dbList[$numDbs] = $file;
  $numDbs++;
}
close DBLIST;

# Read in the R sequence
$rString = "";
open(RSEQ, "< $rseqFile") or die "Unable to open R sequence file $rseqFile:  $!\n";
while (my $line = <RSEQ>)
{
  chomp $line;
  if ($line !~ /^>/) {$rString .= $line;}
}
close RSEQ;

# Read in exon list if provided
if ($exonFile ne "")
{
  open(EXON, "< $exonFile") or die "Unable to open exon list file $exonFile:  $!\n";
  while (my $line = <EXON>)
  {
    my @parts = split(/\s+/, $line);
    push @exonLefts, $parts[0];
    push @exonRights, $parts[1];
  }

  @exonLefts = sort {$a <=> $b} @exonLefts;
  @exonRights = sort {$a <=> $b} @exonRights;

  $mrna = "";
  for (my $i = 0; $i < scalar(@exonLefts); $i++)
  {
    $mrna .= substr($rString, $exonLefts[$i] - 1, $exonRights[$i] - $exonLefts[$i] + 1);
  }
}

# Now, create a temporary file of the sequence to run Blast against
$tmpSeqFile = "/tmp/al.seq.$$";
open(SEQ, "> $tmpSeqFile") or die "Unable to open temporary sequence file $tmpSeqFile for writing:  $!\n";
print SEQ ">Temporary Sequence File\n";
if ($exonFile ne "") {print SEQ $mrna;}
else {print SEQ $rString;}
close SEQ;

# Start blast runs against each database, create a temporary R database of relevant ESTs
$rdbFile = "/tmp/al.rdb.$$";
open(RDB, "> $rdbFile") or die "Unable to open R database file $rdbFile for writing:  $!\n";
close RDB;
 
foreach my $db (@dbList)
{
  open(BLAST, "blastall -p blastn -e 0.00001 -d $db -i $tmpSeqFile -v 5000 -b 5000 -F F -m 8 |") or die "Unable to execute blast process:  blastall -p blastn -e 0.00001 -d $db -i $tmpSeqFile -v 5000 -b 5000 -F F -m 8 \n$!\n";
 
  while (my $line = <BLAST>)
  {
    my @parts = split(/\s+/, $line);

    my $curr_acc = $parts[1];
    $curr_acc =~ s/^.*?\|(.*?)\|.*/$1/;
    if (($parts[6] > $parts[7]) || ($parts[8] > $parts[9]))
    {
      if ($ctgFoundHashR{$curr_acc} != 1)
      {
        $ctgFoundHashR{$curr_acc} = 1;
        my $status = system("gxpfetch $db $curr_acc | gmxtosim.pl | invert.pl >> $rdbFile");
        if ($status != 0)
        {
          print STDERR "Error while executing command:  gxpfetch $db $curr_acc | gmxtosim.pl | invert.pl >> $rdbFile\n";
        }
      }
    }
    
    else
    {
      if ($ctgFoundHashF{$curr_acc} != 1)
      {
        $ctgFoundHashF{$curr_acc} = 1;
        my $status = system("gxpfetch $db $curr_acc | gmxtosim.pl >> $rdbFile");
        if ($status != 0)
        {
          print STDERR "Error while executing command:  gxpfetch $db $curr_acc | gmxtosim.pl >> $rdbFile\n";
        }
      }
    }
  }
  close BLAST;
}

# Now, run sim4 against the R (relevant) database to get precise genomic alignments
$FIRST_FLAG = 1;
@rseq = split(//, $rString);
@initAligns = ();
# These two variables work together to indicate when we are actually processing an alignment.
# Whenever an alignment starts being read, set the flag and reset the counter to zero.  No need
# to worry about the counter when the flag is not set.
$READ_ALIGN_FLAG = 0;
$lineCounter = 0;

open(SIM4, "sim4 $rseqFile $rdbFile A=3 R=0 |") or die "Unable to execute sim4 process:  sim4 $rseqFile $rdbFile A=3 R=0\n$!\n";
while (my $line = <SIM4>)
{
  chomp $line;
  $lineCounter++;

  # Starting a new sequence - a signal to shut off reading in of other alignment
  if ($line =~ /^seq1/) {$READ_ALIGN_FLAG = 0;}

  # Grab the accession number
  elsif ($line =~ /^seq2/)
  {
    $curr_acc = $line;
    $curr_acc =~ s/^.*?\((.*?)\).*/$1/;
    $curr_acc =~ s/.*?\|(.*?)\|.*/$1/;
  }

  # Store endpoints of each alignment
  elsif ($line =~ /^\d+-\d+\s+\(\d+-\d+\)\s+\d+\%/)
  {
    $line =~ s/(-|\(|\))/ /g;
    my @parts = split(/\s+/, $line);
    push @rBegin, ($parts[0] - 1);
    push @rEnd, ($parts[1] - 1);
    push @estBegin, ($parts[2] - 1);
    push @estEnd, ($parts[3] - 1);
  }

  # Begin processing a new alignment.  Store previous alignment and reset variables
  elsif ($line =~ /^\s+0\s+\./)
  {
    if ($FIRST_FLAG == 0)
    {
      $newEst{"RSTRING"} = $curr_query;
      my %estCopy = %newEst;
      push @initAligns, \%estCopy;
    }
    else {$FIRST_FLAG = 0;}
    $newEst{"STRING"} = "";
    $newEst{"ACC"} = $curr_acc;
    $curr_query = "";

    $lineCounter = 0;
    $READ_ALIGN_FLAG = 1;
  }

  elsif (($READ_ALIGN_FLAG == 1) && ($lineCounter % 5 == 1))
  {
    my $bps = $line;
    $bps =~ s/^\s*\d+\s//;
    $bps =~ s/\s/-/g;
    $curr_query .= $bps;
  }

  elsif (($READ_ALIGN_FLAG == 1) && ($lineCounter % 5 == 3))
  {
    my $bps = $line;
    $bps =~ s/^\s*\d+\s//;
    $bps =~ s/\s/-/g;
    $newEst{"STRING"} .= $bps;
  }
}

if ($FIRST_FLAG == 0)
{
  $newEst{"RSTRING"} = $curr_query;
  my %estCopy = %newEst;
  push @initAligns, \%estCopy;
}

close SIM4;

@midAligns = ();
my $alignCount = 0;
# Before creating middle alignments, make the mRNA sequence (from the exon list) the first middle
# alignment
if (scalar(@exonLefts) != 0)
{
  $bp_count = 0;
  for (my $i = 0; $i < scalar(@exonLefts); $i++)
  {
    my %newPiece = ();
    $newPiece{"RSTRING"} = substr($rString, $exonLefts[$i]-1, $exonRights[$i] - $exonLefts[$i] + 1);
    $newPiece{"STRING"} = $newPiece{"RSTRING"};
    $newPiece{"ACC"} = "mRNA";
    $newPiece{"RBEGIN"} = $exonLefts[$i] - 1;
    $newPiece{"REND"} = $exonRights[$i] - 1;
    $newPiece{"BEGIN"} = ($bp_count + 1);
    $bp_count += ($exonRights[$i] - $exonLefts[$i] + 1);
    $newPiece{"END"} = $bp_count;

    ${$middleAligns[0]}[$i] = \%newPiece;
  }
  $alignCount++;
}

# Now, break contiguous alignments into separate pieces and store in @middleAligns
my $totalPieceCount = 0;
foreach my $est (@initAligns)
{
  my $pos = 0;
  my @pos_array = ();
  my $pieceCount = 0;

  # Record the positions where breaks occur using the R sequence
  push @pos_array, -9;
  while (($pos = index(${$est}{"RSTRING"}, "...", $pos+1)) != -1) {push @pos_array, $pos-3;}
  push @pos_array, length(${$est}{"RSTRING"});

  # Now break into contiguous pieces
  for (my $i = 0; $i < scalar(@pos_array) - 1; $i++)
  {
    my %newPiece = ();
    $newPiece{"RSTRING"} = substr(${$est}{"RSTRING"}, 0, $pos_array[$i+1] - $pos_array[$i] - 9);
    $newPiece{"STRING"} = substr(${$est}{"STRING"}, 0, $pos_array[$i+1] - $pos_array[$i] - 9);

    substr(${$est}{"RSTRING"}, 0, $pos_array[$i+1] - $pos_array[$i]) = "";
    substr(${$est}{"STRING"}, 0, $pos_array[$i+1] - $pos_array[$i]) = "";

    $newPiece{"ACC"} = ${$est}{"ACC"};
    $newPiece{"RBEGIN"} = $rBegin[$totalPieceCount];
    $newPiece{"REND"} = $rEnd[$totalPieceCount];
    $newPiece{"BEGIN"} = $estBegin[$totalPieceCount];
    $newPiece{"END"} = $estEnd[$totalPieceCount];

    # @middleAligns is an array of arrays of hashes
    # array 1 - contiguous alignments
    # array 2 - pieces of the alignments
    # hash - information about the pieces
    ${$middleAligns[$alignCount]}[$pieceCount] = \%newPiece;

    $pieceCount++;
    $totalPieceCount++;
  }

  $alignCount++;
}

# Before continuing, filter and sort the alignments

# First, filter, pushing those alignments that make the cut to @finalAligns.  The current rules
# are that we only take the first alignment for each EST and they must span at least
# $MIN_ALIGN_LENGTH on the R sequence
my %foundHash = ();
@finalAligns = ();
for (my $i = 1; $i < scalar(@middleAligns); $i++)
{
  my $numPieces = scalar(@{$middleAligns[$i]});
  my $rend = ${${$middleAligns[$i]}[$numPieces - 1]}{"REND"};
  my $rbegin = ${${$middleAligns[$i]}[0]}{"RBEGIN"};
  if ($rend - $rbegin < $MIN_ALIGN_LENGTH) {next;}

  my $acc_no = ${${$middleAligns[$i]}[0]}{"ACC"};
  if (!(exists($foundHash{$acc_no})))
  {
    push @finalAligns, $middleAligns[$i];
    $foundHash{$acc_no} = 1;
  }
}

# Now sort it
@finalAligns = sort {align_compare()} @finalAligns;

# Finally, the mRNA, of course, should be the first alignment
unshift @finalAligns, $middleAligns[0];

# The next task is to take care of inserting dashes - begin by recording information about
# dashes
foreach my $align (@finalAligns) {foreach my $piece (@{$align}) {recordDashes($piece);}}

# Now, construct the R sequence with dashes inserted
@rDashed = ();
my $lastDashKey = -1;
foreach my $dashKey (sort {$a <=> $b} keys %dashHash)
{
  push @rDashed, splice(@rseq, 0, $dashKey - $lastDashKey);
  for (my $i = 0; $i < $dashHash{$dashKey}; $i++) {push @rDashed, "-";}
  $lastDashKey = $dashKey;
}

# Don't forget the rest of the sequence!
push @rDashed, @rseq;

# Next, construct the aligns with dashes
foreach my $align (@finalAligns)
{
  foreach my $piece (@{$align})
  {
    @{${$piece}{"SEQ"}} = ();
    my @pieceSeq = split(//, ${$piece}{"STRING"});
    my $lastDashKey = ${$piece}{"RBEGIN"} - 1;

    # Find dashed regions that overlap this piece
    foreach my $dashKey (sort {$a <=> $b} keys %dashHash)
    {
      if (($dashKey < ${$piece}{"RBEGIN"}) || ($dashKey >= ${$piece}{"REND"})) {next;}
      push @{${$piece}{"SEQ"}}, splice(@pieceSeq, 0, $dashKey - $lastDashKey);

      # Splice off the number of bps this piece has for the dashed region and put them on the
      # sequence.  Fill the remainder of the gap with dashes.
      my $numExtraBps;
      if (exists(${${$piece}{"INS"}}{$dashKey})) {$numExtraBps = ${${$piece}{"INS"}}{$dashKey};}
      else {$numExtraBps = 0;}
      if ($numExtraBps > 0) {push @{${$piece}{"SEQ"}}, splice(@pieceSeq, 0, $numExtraBps);}
      for (my $i = 0; $i < $dashHash{$dashKey} - $numExtraBps; $i++)
                                                           {push @{${$piece}{"SEQ"}}, "-";}
      $lastDashKey = $dashKey;
    }

    # Don't forget the rest of the sequence!
    push @{${$piece}{"SEQ"}}, @pieceSeq;
  }
}

# Finally, print it all out
print @rDashed;
print " Rseq\n";
foreach my $align (@finalAligns)
{
  my $lastPieceEnd = -1;
  for (my $i = 0; $i < scalar(@{$align}); $i++)
  { 
    my $numBlanks = ${${$align}[$i]}{"RBEGIN"} + numDashes(${${$align}[$i]}{"RBEGIN"}) -
                                              $lastPieceEnd - numDashes($lastPieceEnd) - 1;

    # Be careful!  Need to subtract blanks for insertions at start of this alignment or end of last
    # alignment (because we already filled in bps for them)
    my $dashString = ${${$align}[$i]}{"RSTRING"};
    $dashString =~ s/^(\-*).*/$1/;
    $numBlanks -= length($dashString);
    if ($i != 0)
    {
      my $dashString = ${${$align}[$i - 1]}{"RSTRING"};
      $dashString =~ s/^.*?(\-*$)/$1/;
      $numBlanks -= length($dashString);
    }
    if ($i == 0) {for (my $j = 0; $j < $numBlanks; $j++) {print ".";}}
    else {for (my $j = 0; $j < $numBlanks; $j++) {print "-";}}

    print @{${${$align}[$i]}{"SEQ"}};
    $lastPieceEnd = ${${$align}[$i]}{"REND"};
  }

  # Be careful here too!
  my $dashString = ${${$align}[scalar(@{$align}) - 1]}{"RSTRING"};
  $dashString =~ s/^.*?(\-*$)/$1/;
  
  for (my $j = 0; $j < scalar(@rDashed) - $lastPieceEnd - numDashes($lastPieceEnd) - length($dashString) - 1; $j++) {print ".";}

  my $acc_no = ${${$align}[0]}{"ACC"};
  print " $acc_no\n";
}

# This function inputs an alignment piece and records, both locally and globally, the extra bps
# that it contains that are not in the r sequence
sub recordDashes()
{
  my $estRef = $_[0];
  my @query = split(//, ${$estRef}{"RSTRING"});

  my $numDashes;
  my $queryDashPos;
  my $totalNumDashes = 0;
  my $queryBegin = ${$estRef}{"RBEGIN"};
  for (my $i = 0; $i < scalar(@query); $i++)
  {
    if ($query[$i] eq "-")
    {
      # Count the number of dashes and update the global dash hash to contain the maximum
      # number of dashes in the interval.  Be careful - subtract the number of dashes found
      # so far on this same line ($totalNumDashes).
      $numDashes = 1;
      $queryDashPos = $queryBegin + $i - $totalNumDashes - 1;
      for (my $j = $i+1; $query[$j] eq "-"; $j++) {$numDashes++;}
      if ($dashHash{$queryDashPos} < $numDashes)
                   {$dashHash{$queryDashPos} = $numDashes;}
      ${${$estRef}{"INS"}}{$queryDashPos} = $numDashes;
      $totalNumDashes += $numDashes;
      $i += ($numDashes - 1);
    }
  }
}

# This function counts the number of dashes before a certain point on the original query sequence.
sub numDashes()
{
  my $numDashes = 0;

  foreach my $dashKey (sort {$a <=> $b} keys %dashHash)
  {
    if ($dashKey < $_[0]) {$numDashes += $dashHash{$dashKey};}
    else {return $numDashes;}
  }
  return $numDashes;
}

# Function for sorting alignments.  Currently, sort by beginning position on R sequence first, and
# then by overall length of match
sub align_compare()
{
  if (${${$a}[0]}{"RBEGIN"} < ${${$b}[0]}{"RBEGIN"}) {return -1;}
  if (${${$b}[0]}{"RBEGIN"} < ${${$a}[0]}{"RBEGIN"}) {return 1;}

  my $a_numPieces = scalar(@{$a});
  my $b_numPieces = scalar(@{$b});
  my $a_rend = ${${$a}[$a_numPieces - 1]}{"REND"};
  my $b_rend = ${${$b}[$b_numPieces - 1]}{"REND"};
  my $a_rbegin = ${${$a}[0]}{"RBEGIN"};
  my $b_rbegin = ${${$b}[0]}{"RBEGIN"};
  if ($a_rend - $a_rbegin >= $b_rend - $b_rbegin) {return -1;}
  return 1;
}
