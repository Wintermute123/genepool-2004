#!/usr/bin/perl

for ($a=17; $a<= 25; $a++)
{
    #open up the images directory and read in the CTG directories
    $rootfolder = "/genepool/inbound/symlinks/$a/images";
    opendir(CTGDIR,$rootfolder);
    @ctgdir = readdir(CTGDIR);
    closedir(CTGDIR);
    for ($b=2; $b <= $#ctgdir; $b++)
    {
	#ok now lets open the PS directories and process
	$psdir = "$rootfolder/$ctgdir[$b]/images";
	print "psdir: $psdir\n";
	opendir(PSDIR,$psdir);
	@psfiles = readdir(PSDIR);
	closedir(PSDIR);
	
	#we have the files, now process each one of them.
	for ($c= 2; $c <= $#psfiles; $c++)
	{
	   # print "c loop: $psfiles[$c]\n";
	    if ($psfiles[$c] =~ /^var.*ps/)
	    {
		print "Var: $psfiles[$c]\n";
		$syscmd = "perl /genepool/src/imagemaker/var_makemaps.pl $psdir/$psfiles[$c] $ARGV[0] $a";
		print "$syscmd\n";
		system($syscmd);

	    }
	    if($psfiles[$c] =~ /^evid.*ps/)
	    {
		print "EVID: $psfiles[$c]\n";
		$syscmd = "perl /genepool/src/imagemaker/evid_makemaps.pl $psdir/$psfiles[$c] $ARGV[0] $a";
		print "$syscmd\n";
		system($syscmd);

	    }

	} #end for 2 to # psfiles





    } #end for 2 to num ctgdirs







} #end for 1 to 22 loop

