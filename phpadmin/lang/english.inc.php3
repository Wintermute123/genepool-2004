<?php
/* $Id: english.inc.php3,v 1.7 2001/06/04 12:48:34 lem9 Exp $ */

$strAPrimaryKey		= "A primary key has been added on ";
$strAccessDenied 	= "Access denied";
$strAction		= "Action";

$strAddNewField		= "Add new field";
$strAddSearchConditions = "Add search conditions (body of the \"where\" clause):";
$strAfter		= "After";
$strAnIndex		= "An index has been added on ";

$strAtBeginningOfTable  = "At Beginning of Table";
$strAtEndOfTable        = "At End of Table";
$strAttr                = "Attributes";
$strBack                = "Back";
$strBrowse		= "Browse";
$strCarriage		= "Carriage return: \\r";
$strChange		= "Change";
$strColumnNames		= "Column names";
$strCompleteInserts     = "Complete inserts";
$strCopyTable           = "Copy table to:";
$strCopyTableOK         = "Table \$table has been copied to \$new_name.";
$strCreate		= "Create";
$strCreateNewDatabase	= "Create new database";
$strCreateNewTable	= "Create new table on database ";

$strDatabase		= "Database ";
$strDatabases		= "databases";
$strDefault		= "Default";
$strDelete		= "Delete";
$strDeleted		= "The row has been deleted";

$strDisableMagicQuotes   = "<b>Warning:</b> You have enabled magic_quotes_gpc in your PHP configuration. This version of PhpMyAdmin cannot work properly with it. Please have a look at the configuration section of the PHP manual for information on how to disable it.";
$strDisplay             = "Display";
$strDoAQuery		= "Do a \"query by example\" (wildcard: \"%\")";
$strDoYouReally		= "Do you really want to ";
$strDocu		= "Documentation";
$strDrop		= "Drop";
$strDropDB              = "Drop database ";
$strDumpingData		= "Dumping data for table";
$strEdit		= "Edit";
$strEmpty		= "Empty";
$strEmptyResultSet	= "MySQL returned an empty result set (i.e. zero rows).";
$strEnableMagicQuotes   = "<b>Warning:</b> You haven't enabled magic_quotes_gpc in your PHP configuration. PhpMyAdmin needs this to work properly. Please have a look at the configuration section of the PHP manual for information on how to enable it.";
$strEnclosedBy		= "enclosed by";
$strEnd                 = "End";
$strError		= "Error";
$strEscapedBy		= "escaped by";
$strExtra		= "Extra";
$strField		= "Field";
$strFields		= "Fields";
$strFunction            = "Function";
$strGo			= "Go";
$strHasBeenAltered	= "has been altered.";
$strHasBeenCreated	= "has been created.";
$strHasBeenDropped	= "has been dropped.";
$strHasBeenEmptied	= "has been emptied.";
$strHome		= "Home";
$strHomepageOfficial    = "Official phpMyAdmin Homepage"; //lem9
$strHomepageSourceforge = "new (but still unofficial) phpMyAdmin Homepage"; //lem9
$strHost		= "Host";
$strIfYouWish		= "If you wish to load only some of a table's columns, specify a comma separated field list.";
$strIndex		= "Index";
$strInsert		= "Insert";
$strInsertIntoTable	= "Insert into table";
$strInsertNewRow	= "Insert new row";
$strInsertTextfiles	= "Insert textfiles into table";
$strKeyname		= "Keyname";
$strLengthSet		= "Length/Set";
$strLimitNumRows        = "records per page";
$strLineFeed		= "Linefeed: \\n";
$strLines		= "Lines";
$strLocationTextfile	= "Location of the textfile";
$strLogout              = "Log out";
$strModifications	= "Modifications have been saved";

$strMySQLReloaded	= "MySQL reloaded.";
$strMySQLSaid		= "MySQL said: ";
$strMySQLShowProcess    = "Show processes";
$strMySQLShowStatus     = "Show MySQL runtime information";
$strMySQLShowVars       = "Show MySQL system variables";
$strName		= "Name";
$strNext		= "Next";
$strNo			= "No";
$strNoTablesFound	= "No tables found in database.";
$strNull		= "Null";
$strOftenQuotation	= "Often quotation marks. OPTIONALLY means that only char and varchar fields are enclosed by the \"enclosed by\"-character.";
$strOptionalControls	= "Optional. Controls how to write or read special characters.";
$strOptionally		= "OPTIONALLY";
$strOr                  = "or";
$strPos1                = "Begin";
$strPrevious		= "Previous";
$strPrimary		= "Primary";
$strPrimaryKey		= "Primary key";
$strPrintView           = "Print view";
$strPrinterFriendly	= "Printer friendly version of above table";
$strProducedAnError	= "produced an error.";
$strProperties		= "Properties";
$strQBE                 = "Query by Example";
$strReadTheDocs		= "Read the docs";
$strRecords             = "Records";
$strReloadFailed        = "MySQL reload failed.";
$strReloadMySQL		= "Reload MySQL";
$strRenameTable         = "Rename table to";
$strRenameTableOK       = "Table \$table has been renamed to \$new_name";
$strReplace		= "Replace";
$strReplaceTable	= "Replace table data with file";
$strReset		= "Reset";
$strRowsFrom            = "rows starting from";

$strRunSQLQuery		= "Run SQL query/queries on database ";
$strRunning	        = "running on ";
$strSQLQuery		= "SQL-query";
$strSave		= "Save";
$strSelect		= "Select";
$strSelectFields	= "Select fields (at least one):";
$strSelectNumRows	= "in query";
$strSend		= "Send";
$strShow                = "Show";
$strShowingRecords	= "Showing records ";

$strStrucCSV            = "CSV data";
$strStrucData           = "Structure and data";
$strStrucDrop		= "Add 'drop table'";
$strStrucOnly           = "Structure only";
$strSubmit		= "Submit";
$strSuccess		= "Your SQL-query has been executed successfully";
$strTable		= "table ";
$strTableComments	= "Table comments";
$strTableStructure	= "Table structure for table";
$strTableType		= "Table type";
$strTerminatedBy	= "terminated by";
$strTheContent		= "The content of your file has been inserted.";
$strTheContents		= "The contents of the file replaces the contents of the selected table for rows with identical primary or unique key.";
$strTheTerminator	= "The terminator of the fields.";
$strTotal		= "total";
$strType		= "Type";
$strUnique		= "Unique";

$strValue		= "Value";
$strViewDump		= "View dump (schema) of table";
$strViewDumpDB		= "View dump (schema) of database";
$strWelcome		= "Welcome to ";
$strWrongUser 		= "Wrong username/password. Access denied.";
$strYes			= "Yes";

$strCheckTable		= "Check";
$strAnalyzeTable	= "Analyze";
$strRepairTable		= "Repair";
$strOptimizeTable	= "Optimize";

$strTextAreaLength=" Because of its length,<br> this field might not be editable ";  
$strNoUsersFound = "No user(s) found.";
$strNoRights = "You don't have enough rights to be here right now!";
$strHostEmpty = "The host name is empty!";
$strUserEmpty = "The user name is empty!";
$strPasswordEmpty = "The password is empty!";
$strDbEmpty = "The database name is empty!";
$strTableEmpty = "The table name is empty!";
$strPasswordNotSame = "The passwords aren't the same!";
$strAnyDatabase = "Any database";
$strUser = "User";
$strAnyTable = "Any table";
$strHost = "Host";
$strAnyHost = "Any host";
$strUserName = "User name";
$strAnyUser = "Any user";
$strPassword = "Password";
$strNoPassword = "No Password";
$strReType = "Re-type";
$strPrivileges = "Privileges";
$strGrantOption = "Grant Option";
$strAddUser = "Add a new User";
$strAddPriv = "Add a new Privilege";
$strCheckDbPriv = "Check Database Privileges";
$strAddPrivMessage = "You have added a new privilege.";
$strAddUserMessage = "You have added a new user.";
$strRememberReload = "Remember reload the server.";
$strRevoke = "Revoke";
$strGrants = "Grants";
$strRevokePriv = "Revoke Privileges";
$strRevokeGrant = "Revoke Grant";
$strRevokeMessage = "You have revoked the privileges for";
$strDelPassMessage = "You have deleted the password for";
$strRevokeGrantMessage = "You have revoked the Grant privilege for";
$strUpdatePrivMessage = "You have updated the privileges for";
$strUpdatePassMessage = "You have updated the password for";
$strDeleteUserMessage = "You have deleted the user";
$strDeletePassword = "Delete Password";
$strUpdatePassword = "Update Password";
$strEditPrivileges = "Edit Privileges";
$strNoPrivileges = "No Privileges";
$strAny = "Any";
$strAll = "All";
$strUpdate = "Update";
$strReload = "Reload";
$strShutdown = "Shutdown";
$strProcess = "Process";
$strFile = "File";
$strGrant = "Grant";
$strReferences = "References";
$strAlter = "Alter";
$strCheckAll = "Check All";
$strUncheckAll = "Uncheck All";
$strUsers = "Users";
$strConfirm = "Do you really want to do it?";
$strDeleteFailed = "Deleted Failed!";
$strColumn = "Column";
$strAnyColumn = "Any Column";
$strColumnEmpty = "The columns names are empty!";

$strTableMaintenance=" Table maintenance ";  
?>
